﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExpCSF.Controls;

namespace ExpCSF.Base
{
    public sealed class ActionResult<TResult>
            where TResult : class
    {
        #region · Properties ·

        public TResult Data
        {
            get;
            set;
        }

        public ActionResultType Result
        {
            get;
            set;
        }

        #endregion

        #region · Constructors ·

        public ActionResult()
        {
        }

        #endregion
    }
}
