﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace ExpCSF.Controls
{
    /// <summary>
    /// Interaction logic for AnimationView.xaml
    /// </summary>
    public partial class AnimationView : UserControl
    {
        public AnimationView()
        {
            InitializeComponent();
            execAnimation();

        }
        Random rand;
        System.Windows.Threading.DispatcherTimer myDispatcherTimer;
        int fortimerinterval;

        private void execAnimation()
        {
            rand = new Random();
            fortimerinterval = rand.Next(2000, 10000);
            fortimerinterval += Convert.ToInt32(Guid.NewGuid().ToString("N").Substring(0, 3), 16);
            myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, fortimerinterval);
            myDispatcherTimer.Tick += new EventHandler(Each_Tick);
            myDispatcherTimer.Start();

        }

        // Raised every 100 miliseconds while the DispatcherTimer is active.
        public void Each_Tick(object o, EventArgs sender)
        {
            myDispatcherTimer.Stop();

            doAnimation();
            execAnimation();

        }
        private void doAnimation()
        {
            rand = new Random();
            int selectedAnimation = rand.Next(1, 9);

            Storyboard sbdLabelRotation = (Storyboard)FindResource("animation");

            switch (selectedAnimation)
            {
                case 1:
                    sbdLabelRotation = (Storyboard)FindResource("animation1");
                    break;
                case 2:
                    sbdLabelRotation = (Storyboard)FindResource("animation2");
                    break;
                case 3:
                    sbdLabelRotation = (Storyboard)FindResource("animation3");
                    break;
                case 4:
                    sbdLabelRotation = (Storyboard)FindResource("animation4");
                    break;
                case 5:
                    sbdLabelRotation = (Storyboard)FindResource("animation5");
                    break;
                case 6:
                    sbdLabelRotation = (Storyboard)FindResource("animation6");
                    break;
                case 7:
                    sbdLabelRotation = (Storyboard)FindResource("animation7");
                    break;
                case 8:
                    sbdLabelRotation = (Storyboard)FindResource("animation8");
                    break;
                case 9:
                    sbdLabelRotation = (Storyboard)FindResource("animation9");
                    break;
            }
       sbdLabelRotation.Begin();

        }

        private bool isQuadruple;

        public bool IsQuadruple
        {
            get { return isQuadruple; }
            set
            {
                isQuadruple = value;
                if (value)
                {
                    this.image.Width = 195;
                    this.image.Height = 195;
                }
            }
        }
        private string text;

        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                this.MainText.Text = value;
            }
        }
        private string imageSource;

        public string ImageSource
        {
            get { return imageSource; }
            set
            {
                imageSource = value;
                this.image.Source = new BitmapImage(new Uri(value, UriKind.RelativeOrAbsolute)); ;  //(ImageSource)value; // value;
            }
        }
    }
}
