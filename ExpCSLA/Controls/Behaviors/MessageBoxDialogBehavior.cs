﻿using System.Linq;
using System.Windows.Interactivity;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Messaging;
using System;

namespace ExpCSF.Controls.Behaviors
{
    public class MessageBoxDialogBehavior : Behavior<FrameworkElement>
    {
        readonly IMessenger _messenger = Messenger.Default;
        public bool Identifier { get; set; }

        protected override void OnAttached()
        {
            base.OnAttached();

            _messenger.Register<MessageDailog>(this, Identifier, ShowDialog);
        }


        private void ShowDialog(MessageDailog dm)
        {
            Dispatcher.BeginInvoke(
               new Action(
                   delegate
                   {
                       var messageWindow = new MessageWindowElement(dm);
                       messageWindow.ShowWindow();

                       dm.ProcessCallback(messageWindow.DialogResult);

                   }
                   ), DispatcherPriority.Normal);

            //Dispatcher.InvokeAsync(() =>
            //{
            //    var messageWindow = new MessageWindowElement(dm);
            //    messageWindow.ShowWindow();

            //    dm.ProcessCallback(messageWindow.DialogResult);
            //}, DispatcherPriority.Normal);

        }
    }
}
