﻿using System.Windows;
using System.Windows.Interactivity;
using GalaSoft.MvvmLight.Messaging;

namespace ExpCSF.Controls.Behaviors
{

    public class VMModalDialogBehavior : Behavior<FrameworkElement>
    {
        readonly IMessenger _messenger = Messenger.Default;

        protected override void OnAttached()
        {
            base.OnAttached();

            //_messenger.Register<VMMessageDailog>(this, Identifier, ShowDialog);
            _messenger.Register<VMMessageDailog>(this, true, ShowDialog);
        }

        public bool Identifier { get; set; }

        private void ShowDialog(VMMessageDailog dm)
        {
            var messageWindow = new ModalDailogWindow(dm);
            messageWindow.ShowDialog();
            //dm.Callback(messageWindow.DialogResult);
        }
    }
}
