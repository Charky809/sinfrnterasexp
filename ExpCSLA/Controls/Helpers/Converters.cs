﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace ExpCSF.Controls
{
    public class BoolToOppositeBoolConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is bool))
                throw new InvalidOperationException("The target must be a boolean");

            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is bool))
                throw new InvalidOperationException("The target must be a boolean");

            return !(bool)value;
        }
        #endregion
    }

    [MarkupExtensionReturnType(typeof(IValueConverter))]
    public abstract class MarkupConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        protected abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);
        protected abstract object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture);

        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return Convert(value, targetType, parameter, culture);
            }
            catch
            {
                return DependencyProperty.UnsetValue;
            }
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return ConvertBack(value, targetType, parameter, culture);
            }
            catch
            {
                return DependencyProperty.UnsetValue;
            }
        }
    }

    public class MyCommandParameterConverter : MarkupExtension, IMultiValueConverter
    {
         public object Convert(object[] values, Type targetType, object parameter,
                              System.Globalization.CultureInfo culture)
        {
            return values;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
                                    System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
        
    }

    public class NullObjectToVisibiltyConverter : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value == null) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class NullObjectToBoolConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return false;

            if (value is IList)
            {
                return ((IList)value).Count != 0;
            }

            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class ToLowerConverter
        : MarkupExtension, IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        [SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                var strValue = value.ToString();


                return strValue.ToLowerInvariant();
            }
            return null;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class BooleanOrConverter : MarkupExtension, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
                              System.Globalization.CultureInfo culture)
        {
            return values.Any(value => value != DependencyProperty.UnsetValue && (bool)value);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
                                    System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class ToUpperConverter : MarkupConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = value as string;
            return val != null ? val.ToUpper() : value;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class ToTitleCaseConverter : MarkupConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = value as string;
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
            return val != null ? ti.ToTitleCase(val) : value;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class VisibilityByRepairStatusConverter : MarkupConverter
    {
        public string StatusParameter { get; set; }
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = value is RepairStatus ? (RepairStatus)value : RepairStatus.NotProcessed;

            return (status.ToString() == StatusParameter) ? Visibility.Visible : Visibility.Collapsed;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public sealed class AddPunctuationValueConverter : IValueConverter
    {
        private bool _addPadding = true;
        private char _punctuationCharacter = ':';
        private int _paddingLength = 2;
        private string _originalValue = string.Empty;

        public bool AddPadding
        {
            get { return this._addPadding; }
            set { this._addPadding = value; }
        }
        public char PunctuationCharacter
        {
            get { return this._punctuationCharacter; }
            set { this._punctuationCharacter = value; }
        }
        public int PaddingLength
        {
            get { return this._paddingLength; }
            set { this._paddingLength = value; }
        }

        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                this._originalValue = string.Empty;
                return string.Empty;
            }

            this._originalValue = value.ToString();
            string returnValue = string.Empty;
            try
            {
                //if (value is string)
                //{
                returnValue = value.ToString().Trim().TrimEnd(this._punctuationCharacter).Trim() + " " + this._punctuationCharacter.ToString();
                if (this._addPadding)
                    returnValue += new string(' ', this._paddingLength);
                //}
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return _originalValue;
        }
        #endregion
    }

    public class BoolToVisibilityValueConverter : MarkupExtension, IValueConverter
    {
        public bool IsNegateValue { get; set; }
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool && (bool)value)
                return IsNegateValue ? Visibility.Collapsed : Visibility.Visible;

            return !IsNegateValue ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
        #endregion

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class BoolToVisibilityValueConverterNegate : MarkupExtension, IValueConverter
    {
        public bool IsNegateValue { get; set; }
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool && (bool)value)
                return IsNegateValue ? Visibility.Visible : Visibility.Collapsed;

            return !IsNegateValue ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
        #endregion

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class BoolToFontWeight : MarkupExtension, IValueConverter
    {
        public bool IsNegateValue { get; set; }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value is bool && (bool)value)
                return IsNegateValue ? FontWeights.Normal : FontWeights.Bold;

            return !IsNegateValue ? FontWeights.Normal : FontWeights.Bold;


        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class EnumToResourceConverter : MarkupConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var resourceString =
                    Resources.EnumResources.ResourceManager.GetString(value.GetType().Name + "_" + value.ToString(),
                                                                      CultureInfo.CurrentCulture);
                return (!string.IsNullOrEmpty(resourceString)) ? resourceString : value;
            }
            return string.Empty;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    [ValueConversion(typeof(bool?), typeof(bool))]
    public class SuccessConverter : MarkupExtension, IValueConverter
    {
        public string TipoOpcion { get; set; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //bool param = bool.Parse(parameter.ToString());
            bool param = true;
            string opcion = TipoOpcion;
            if (value == null)
            {
                return false;
            }
            else
            {
                return !((bool)value ^ param);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //bool param = bool.Parse(parameter.ToString());
            bool param = true;
            return !((bool)value ^ param);
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    //public class EnumToResourceConverter : MarkupConverter
    //{
    //    protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        if (value != null)
    //        {
    //            var resourceString =
    //                Resources.EnumResources.ResourceManager.GetString(value.GetType().Name + "_" + value.ToString(),
    //                                                                  CultureInfo.CurrentCulture);
    //            return (!string.IsNullOrEmpty(resourceString)) ? resourceString : value;
    //        }
    //        return string.Empty;
    //    }

    //    protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        return Binding.DoNothing;
    //    }
    //}

    //public class VisibilityOnEarMoldType : MarkupConverter
    //{
    //    protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        if (value is EarMoldType)
    //        {
    //            var result = (EarMoldType)value;
    //            return result == EarMoldType.Soft ? Visibility.Visible : Visibility.Collapsed;
    //        }
    //        return Visibility.Collapsed;
    //    }

    //    protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        return Binding.DoNothing;
    //    }
    //}
}
