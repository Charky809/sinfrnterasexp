﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ExpCSF.Controls
{

    [Serializable]
    [DataContract]
    public enum AppointmentType
    {
        [EnumMember]
        Control = 0,
        [EnumMember]
        PrimerVez = 1,
        
    }

    
    [Serializable]
    public enum ActionResultType
    {
        DataFetched,
        RequestedNew,
        DataNotFound
    }

    [Serializable]
    public enum DialogButton
    {
        /// <summary>
        /// The message box displays an OK button.
        /// </summary>
        Ok = 0,
        /// <summary>
        /// The message box displays OK and Cancel buttons.
        /// </summary>
        OkCancel = 1,
        /// <summary>
        /// The message box displays Yes and No buttons.
        /// </summary>
        YesNo = 4,
    }

    [Serializable]
    public enum ViewModeType
    {
        /// <summary>
        /// Default mode
        /// </summary>
        Default,
        /// <summary>
        /// Adding new
        /// </summary>
        Add,
        /// <summary>
        /// Edit mode
        /// </summary>
        Edit,
        /// <summary>
        /// View only mode
        /// </summary>
        ViewOnly,
        /// <summary>
        /// Busy mode
        /// </summary>
        Busy
    }

    public enum Status
    {
        InActive = 0,
        Active = 1,
    }

    public enum DirtyState : short
    {
        UnChanged = 0,
        PendingAddChange = 1,
        PendingDelete = 2,
    }
    public enum AccessType : short
    {
        Default = 0,
        ReadOnly = 1,
    }


    public enum DialogType
    {
        ByPercentage,
        BySizeInPixel
    }

    public enum MaskType
    {
        Any,
        Integer,
        Decimal
    }

    public enum TimeFormat
    {
        Custom,
        ShortTime,
        LongTime
    }

    public enum CategoryApt
    {
        [EnumMember]
        Roja = 1,
        [EnumMember]
        Amarilla = 2,
        [EnumMember]
        Verde = 3
    }

    public enum MetodoRegistro
    {
        [EnumMember]
        Texto = 1,
        [EnumMember]
        Seleccion = 2,
        [EnumMember]
        Ambos = 3
       
    }

    [Serializable]
    [DataContract]
    public enum RepairStatus
    {
        [EnumMember]
        NotProcessed = 0,
        [EnumMember]
        DispatchedToCompany = 1,
        [EnumMember]
        CompanyReceived = 2,
        [EnumMember]
        CompanyDispatched = 3,
        [EnumMember]
        Received = 4,
        [EnumMember]
        Informed = 5,
        [EnumMember]
        DeliveredToCustomer = 6
    }

    [Serializable]
    public enum DialogResult
    {
        /// <summary>
        /// The message box returns no result.
        /// </summary>
        None = 0,
        /// <summary>
        /// The result value of the message box is OK.
        /// </summary>
        Ok = 1,
        /// <summary>
        /// The result value of the message box is Cancel.
        /// </summary>
        Cancel = 2,
        /// <summary>
        /// The result value of the message box is Yes.
        /// </summary>
        Yes = 6,
        /// <summary>
        /// The result value of the message box is No.
        /// </summary>
        No = 7,
    }




}
