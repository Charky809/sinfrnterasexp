﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExpCSF.Base;

namespace ExpCSF.Controls
{
    public class VMMessageDailog
    {
        public Action<dynamic> Callback { get; private set; }

        public IBaseViewModel ChildViewModel { get; set; }

        public void ProcessCallback(dynamic result)
        {
            if (this.Callback == null)
                return;
            this.Callback(result);
        }
    }
}
