﻿using System.Windows;
using ExpCSF.ViewModel;
using System;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Resources;
using WinInterop = System.Windows.Interop;
using ExpCSF.Controls;

namespace ExpCSF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            //Closing += (s, e) => ViewModelLocator.Cleanup();
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            IntPtr handle = (new WinInterop.WindowInteropHelper(this)).Handle;
            WinInterop.HwndSource.FromHwnd(handle).AddHook(Win32Interop.WindowProc);
            ResourceManager.AllResourceCultures = ResourceManager.Default;

            DataContext = new MainViewModel(Messenger.Default) { WindowState = WindowState.Maximized };
        }

       
    }
}