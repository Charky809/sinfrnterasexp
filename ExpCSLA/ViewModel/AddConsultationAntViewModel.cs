﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class AddConsultationAntViewModel : BaseViewModel<List<Form_Exp_Consulta>>
    {

        
        

        private string _searchTextCode;
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                this.RaisePropertyChanged("SearchText");
                if(_searchText.Length >=5)
                    GetSearchList(_searchText,false);
            }
        }

        public string SearchTextCode
        {
            get { return _searchTextCode; }
            set
            {
                _searchTextCode = value;
                this.RaisePropertyChanged("SearchTextCode");
                if (_searchTextCode.Length >= 2)
                    GetSearchList(_searchTextCode,true);
            }
        }

        private bool _isPopupOpen;
        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set
            {
                _isPopupOpen = value;
                this.RaisePropertyChanged("IsPopupOpen");
            }
        }

        private int _SelectedDiagnosticID;
        public int SelectedDiagnosticID
        {
            get { return _SelectedDiagnosticID; }
            set
            {
                _SelectedDiagnosticID = value;
                this.RaisePropertyChanged("SelectedDiagnosticID");
            }
        }


        private int _RadioGroupName;
        public int RadioGroupName
        {
            get { return _RadioGroupName; }
            set
            {
                _RadioGroupName = value;
                this.RaisePropertyChanged("RadioGroupName");
            }
        }

 

        

        private IDictionary<String, String> _customerSearchList;
        public IDictionary<String, String> CustomerSearchList
        {
            get { return _customerSearchList; }
            set
            {
                _customerSearchList = value;
                this.RaisePropertyChanged(" CustomerSearchList");
            }
        }

        private void GetSearchList(string strSearchText,bool isCode)
        {
             DataService _dataservice = new DataService();
            try
            {
                if (string.IsNullOrEmpty(strSearchText))
                {
                    CustomerSearchList = null;
                    IsPopupOpen = false;
                    return;
                    
                }

               
                   var task = Task.Factory.StartNew(() =>
                    {
                        CustomerSearchList = _dataservice.GetActiveDXFilter(strSearchText,isCode);
                        IsPopupOpen = true;
                        if (CustomerSearchList == null || !CustomerSearchList.Any())
                        {
                            CustomerSearchList = new Dictionary<String, String>();
                            CustomerSearchList.Add(new KeyValuePair<string, string>("", "No se encontraron registros."));

                        }

                        this.RaisePropertyChanged("CustomerSearchList");
                    });

                   task.Wait();
               

                this.SelectedDiagnosticID = 0;

            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured, ExceptionResources.ExceptionOccuredLogDetail);
            }

        }

        private Consultas _consulta;
        public Consultas ConsultaSeleccionada
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("ConsultaSeleccionada");
            }
        }

        private List<Form_Exp_Consulta> _DiagnosticosConsulta;
        public List<Form_Exp_Consulta> DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }


        private List<Diagnosticos> _ComboDiagnosticosItems;
           public List<Diagnosticos> ComboDiagnosticosItems
        {
            get { return _ComboDiagnosticosItems; }
            set
            {
                _ComboDiagnosticosItems = value;
                this.RaisePropertyChanged("ComboDiagnosticosItems");
            }
        }


           private Diagnosticos _SelectedDiagnostic;
           public Diagnosticos SelectedDiagnostic
           {
               get { return _SelectedDiagnostic; }
               set
               {
                   _SelectedDiagnostic = value;
                   this.RaisePropertyChanged("SelectedDiagnostic");
               }
           }

        
        #region Delegate
        public Action RefreshConsultationDX { get; set; }
        #endregion

             #region Properties

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #endregion

        #region Commands
        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private ICommand _cancelCommand;
        
        private ICommand _addPhoneCommand;
        private ICommand _deletePhoneCommand;


        public ICommand CancelCommand
        {
            get { return this._cancelCommand ?? (this._cancelCommand = new RelayCommand(OnCancelCustomer)); }
        }

        public ICommand AddPhoneCommand
        {
            get { return this._addPhoneCommand ?? (this._addPhoneCommand = new RelayCommand(OnAddPhone)); }
        }

        public ICommand DeletePhoneCommand
        {
            get { return this._deletePhoneCommand ?? (this._deletePhoneCommand = new RelayCommand(OnDeletePhone, CanDeletePhone)); }
        }

        #endregion

        #region Constructors

        public AddConsultationAntViewModel(IMessenger messenger, Usuario userLogin, Consultas consultation)
            : base(messenger, userLogin)
        {
             DataService _dataservice = new DataService();
            IsInEditMode = true;
            this.ConsultaSeleccionada = consultation;
            this.DiagnosticosConsulta = _dataservice.GetFormEspecConsultation(consultation,"A");
      
                AgregarDatosFormConsulta();
                IsInEditMode = false;
            

        }

        public AddConsultationAntViewModel(IMessenger messenger)
            : base(messenger)
        {
            this.DiagnosticosConsulta = new List<Form_Exp_Consulta>();
            
                AgregarDatosFormConsulta();
                IsInEditMode = false;
            
        }

        public AddConsultationAntViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            this.DiagnosticosConsulta = new List<Form_Exp_Consulta>();
            
                AgregarDatosFormConsulta();
                IsInEditMode = false;
            

        }

        public AddConsultationAntViewModel(IMessenger messenger, Usuario userLogin, List<Form_Exp_Consulta> customer, Consultas consultation)
            : base(messenger, userLogin)
        {
             DataService _dataservice = new DataService();
            IsInEditMode = true;
            this.Entity = customer;
            this.ConsultaSeleccionada = consultation;
            this.DiagnosticosConsulta = _dataservice.GetFormEspecConsultation(consultation,"A");
            
                AgregarDatosFormConsulta();
                IsInEditMode = false;
           
            
        }

        public AddConsultationAntViewModel(IMessenger messenger, Usuario userLogin, List<Form_Exp_Consulta> customer)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.Entity = customer;
            this.DiagnosticosConsulta = customer;
            
                AgregarDatosFormConsulta();
                IsInEditMode = false;
           


        }
        #endregion

        #region Privados

        //private void CargarDiagnosticos()
        //{
        //    DataService _dataservice = new DataService();
        //    ComboDiagnosticosItems= _dataservice.GetActiveDX();
        //}

        private void OnSelectSearchItem()
        {
            IsPopupOpen = false;
            //this.RaisePropertyChanged(" SearchText");

            if (this.SelectedDiagnosticID.ToString() != "-1")
            {
                CSLADBEntities contexto = new CSLADBEntities();

                //char
                SelectedDiagnostic = contexto.Diagnosticos.Where((u) => u.Id_Diagnostico == this.SelectedDiagnosticID).FirstOrDefault();
                contexto.Detach(SelectedDiagnostic);
                
            }

            this.SearchText = SelectedDiagnostic.Descripcion;
        }
        #endregion  

        #region Override Methods

        public override void Initialize()
        {
            base.Initialize();

            //CargarDiagnosticos();

        }

        #endregion

        #region Command Methods
        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }
        private void OnSaveCustomer()
        {
             DataService _dataservice = new DataService();
             List<Form_Exp_Consulta> actualizados = new List<Form_Exp_Consulta>();
            var returnStatus = false;
            //char
            returnStatus = _dataservice.AddAntsConsultation(this.DiagnosticosConsulta.Where(x => x.isNewItem).ToList());
            actualizados = _dataservice.GetFormEspecConsultation(ConsultaSeleccionada, "A");

            foreach (var item in actualizados)
            {
                
                DiagnosticosConsulta.Where(x => x.idFormulario == item.idFormulario).FirstOrDefault().IdForm_Exp_Consulta = item.IdForm_Exp_Consulta;
                
            }
            //: _dataservice.UpdateAntsConsultation(this.DiagnosticosConsulta.Where(x=>x.isNewItem == false));

            if (returnStatus)
            {
                returnStatus = _dataservice.UpdateAntsConsultation(this.DiagnosticosConsulta.Where(x => x.isNewItem == false));

                if (returnStatus)
                {
                    if (RefreshConsultationDX != null)
                        this.RefreshConsultationDX();

                    var messageDailog = new MessageDailog()
                    {
                        Caption = Resources.MessageResources.DataSavedSuccessfully,
                        DialogButton = DialogButton.Ok,
                        Title = Resources.TitleResources.Information
                    };

                    foreach (var item in this.DiagnosticosConsulta.Where(x => x.isNewItem))
                    {
                        if ((item.ValorTexto != null && item.ValorTexto != string.Empty)
                           || (item.ValSeleccionadoSi != null)
                              || (item.ValSeleccionaodNo != null)
                              || (item.ValSeleccionadoOtro != null)
                               || (item.Observacion != null && item.Observacion != string.Empty)
                                   )
                        {
                            item.isNewItem = false;
                        }
                    }

                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

                    if (this.CloseWindow != null)
                        this.CloseWindow();
                }
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            }
        }


        private void OnCancelCustomer()
        {
            var messageDailog = new MessageDailog((result) =>
                {
                    if (result == DialogResult.Ok)
                    {
                        if (this.ParentViewModel != null)
                            this.ParentViewModel.ChildViewModel = null;
                        this.Unload();

                        if (this.CloseWindow != null)
                            this.CloseWindow();
                    }
                }) { Caption = Resources.MessageResources.CancelWindowMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);


        }


        private void AgregarDatosFormConsulta()
        {
             DataService _dataservice = new DataService();
            try
            {
                bool DebeReordenar = false;
                List<Form_Especialidad> listaFormularios = new List<Form_Especialidad>();
                Especialidades pEspecialidad;
                if (ConsultaSeleccionada.medicos.Especialidades_Medico.FirstOrDefault() != null)
                {
                    pEspecialidad = ConsultaSeleccionada.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
                }
                else if (ConsultaSeleccionada.Especialidades != null)
                {
                    pEspecialidad = ConsultaSeleccionada.Especialidades;
                }
                else
                {
                    pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
                }
                //UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
                //_dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
                listaFormularios = _dataservice.GetActiveFormEspecialidad("A", pEspecialidad);

                if (ConsultaSeleccionada != null)
                {
                    if (DiagnosticosConsulta.Count() >= 1)
                        DebeReordenar = true;

                    if (this.DiagnosticosConsulta == null)
                        this.DiagnosticosConsulta = new List<Form_Exp_Consulta>();

                    List<Form_Exp_Consulta> tempCollection = new List<Form_Exp_Consulta>();
                    

                    foreach (Form_Especialidad item in listaFormularios)
                    {

                        tempCollection = this.DiagnosticosConsulta;

                        int Existe = DiagnosticosConsulta.Where(x => x.idFormulario == item.idForm_Expediente).Count();
                        if (Existe <= 0)
                            tempCollection.Add(new Form_Exp_Consulta() { isNewItem = true, Fecha = DateTime.Now, idFormulario = item.idForm_Expediente, Form_Expediente = item.Form_Expediente, idConsulta = ConsultaSeleccionada.ID, IdUsuario = this.UserLogin.Id.ToString() });

                        var listaHijos = _dataservice.GetActiveFormEspecialidadByFather(item.Form_Expediente.idFormulario);
                        foreach (Form_Expediente itemHijo in listaHijos)
                        {
                            int ExisteHijo = DiagnosticosConsulta.Where(x => x.idFormulario == itemHijo.idFormulario).Count();

                            if (ExisteHijo <= 0)
                                tempCollection.Add(new Form_Exp_Consulta() { isNewItem = true, Fecha = DateTime.Now, Form_Expediente = itemHijo, idFormulario = itemHijo.idFormulario, idConsulta = ConsultaSeleccionada.ID, IdUsuario = this.UserLogin.Id.ToString(), ValSeleccionaodNo = true });
                        }
                                                                      
                    }

                    if (DebeReordenar)
                        OrdenarFormularios();
                    else
                    {
                        this.DiagnosticosConsulta = null;
                        this.DiagnosticosConsulta = tempCollection;
                        this.RaisePropertyChanged("DiagnosticosConsulta");
                    }
                    
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private void OrdenarFormularios()
        {
           
            var tempCollection = this.DiagnosticosConsulta;
            this.DiagnosticosConsulta = new List<Form_Exp_Consulta>();
            foreach (var item in tempCollection.Where(x=>x.Form_Expediente.idFormulario_Padre == null).OrderBy(m=>m.Form_Expediente.idFormulario))
            {
                this.DiagnosticosConsulta.Add(item);
                foreach (var itemHijo in tempCollection.Where(x=> x.Form_Expediente.idFormulario_Padre == item.Form_Expediente.idFormulario).OrderBy(m=>m.Form_Expediente.Orden))
                {
                    this.DiagnosticosConsulta.Add(itemHijo);
                }
                
            }
            this.RaisePropertyChanged("DiagnosticosConsulta");
        
        }
        private void OnAddPhone()
        {
        //    IsInEditMode = false;
        //    if (ConsultaSeleccionada != null)
        //    {
        //        if (this.DiagnosticosConsulta == null)
        //            this.DiagnosticosConsulta = new List<Form_Exp_Consulta>();

        //        var tempCollection = this.DiagnosticosConsulta;
        //        tempCollection.Add(new Form_Exp_Consulta() { Diagnosticos = SelectedDiagnostic, FechaRegistro = DateTime.Now, Orden = tempCollection.Count + 1, idCita = ConsultaSeleccionada.ID, Estado = true, IsDeleted = false, IdUsuario = this.UserLogin.Id.ToString() });
        //        this.DiagnosticosConsulta = null;
        //        this.DiagnosticosConsulta = tempCollection;
        //        this.RaisePropertyChanged("DiagnosticosConsulta");
        //        this.SearchText = string.Empty;
        //    }
        //    else
        //    {
        //        var messageDailog = new MessageDailog() { Caption = "Se debe seleccionar una consulta antes.", DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
        //        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
        //    }
        //    //CargarDiagnosticos();

        }
        private bool CanDeletePhone()
        {
            return this.Entity != null && this.DiagnosticosConsulta != null &&
                   this.DiagnosticosConsulta.Any(x => x.IsSelected);
            
        }

        private void OnDeletePhone()
        {
            
            //if (this.DiagnosticosConsulta != null &&
            //     this.DiagnosticosConsulta.Any(x => x.IsSelected))
            //{
            //    //char
            //    var qureyItems = this.DiagnosticosConsulta.Where(x =>  x.IdDiagnosticoCita > 0 && (x.IsSelected) ).ToList();
            //    if (qureyItems.Any())
            //    {
            //        _dataservice.DeleteDXConsultation(qureyItems);
            //        qureyItems.ForEach(x => x.IsDeleted = true);
            //    }

            //    var items = this.DiagnosticosConsulta.Where(x => x.IsSelected && (x.IdDiagnosticoCita == 0)).ToList();

            //    if (items.Any())
            //    {
            //        foreach (var customerPhone in items)
            //        {
            //            this.DiagnosticosConsulta.Remove(customerPhone);
            //        }
            //        var tempCollection = new List<Diagnosticos_Cita>(this.DiagnosticosConsulta.ToList());
            //        this.DiagnosticosConsulta = null;
            //        this.DiagnosticosConsulta = tempCollection;
            //        this.RaisePropertyChanged("DiagnosticosConsulta");
            //    }
            //}

            //this.SearchText = "";
        }



        #endregion



    }
}