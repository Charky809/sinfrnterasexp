﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class AddConsultationProblemViewModel : BaseViewModel<List<Problemas_Consulta>>
    {

        private ICommand _selectSearchItemCommand;
        private DataService _dataservice = new DataService();

        private string _searchTextCode;
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                this.RaisePropertyChanged("SearchText");
                if(_searchText.Length >=5)
                    GetSearchList(_searchText,false);
            }
        }

        List<ComboData> _comboItems = new List<ComboData>();
        public List<ComboData> ComboItems
        {
            get
            {
                return _comboItems;
            }
            set
            {
                _comboItems = value;
                this.RaisePropertyChanged("ComboItems");
            }

        }

        public string SearchTextCode
        {
            get { return _searchTextCode; }
            set
            {
                _searchTextCode = value;
                this.RaisePropertyChanged("SearchTextCode");
                if (_searchTextCode.Length >= 2)
                    GetSearchList(_searchTextCode,true);
            }
        }

        private bool _isPopupOpen;
        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set
            {
                _isPopupOpen = value;
                this.RaisePropertyChanged("IsPopupOpen");
            }
        }

        private int _SelectedDiagnosticID;
        public int SelectedDiagnosticID
        {
            get { return _SelectedDiagnosticID; }
            set
            {
                _SelectedDiagnosticID = value;
                this.RaisePropertyChanged("SelectedDiagnosticID");
            }
        }


        

        private IDictionary<String, String> _customerSearchList;
        public IDictionary<String, String> CustomerSearchList
        {
            get { return _customerSearchList; }
            set
            {
                _customerSearchList = value;
                this.RaisePropertyChanged(" CustomerSearchList");
            }
        }

        private void GetSearchList(string strSearchText,bool isCode)
        {
            try
            {
                if (string.IsNullOrEmpty(strSearchText))
                {
                    CustomerSearchList = null;
                    IsPopupOpen = false;
                    return;
                    
                }

               
                    Task.Factory.StartNew(() =>
                    {
                        CustomerSearchList = _dataservice.GetActiveDXFilter(strSearchText,isCode);
                        IsPopupOpen = true;
                        if (CustomerSearchList == null || !CustomerSearchList.Any())
                        {
                            CustomerSearchList = new Dictionary<String, String>();
                            CustomerSearchList.Add(new KeyValuePair<string, string>("", "No se encontraron registros."));

                        }

                        this.RaisePropertyChanged("CustomerSearchList");
                    });
               

                this.SelectedDiagnosticID = 0;

            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured, ExceptionResources.ExceptionOccuredLogDetail);
            }

        }

        public ICommand SelectSearchItemCommand
        {
            get
            {
                return this._selectSearchItemCommand ??
                       (this._selectSearchItemCommand = new RelayCommand(OnSelectSearchItem));
            }
        }

        private Consultas _consulta;
        public Consultas ConsultaSeleccionada
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("ConsultaSeleccionada");
            }
        }

        private List<Problemas_Consulta> _DiagnosticosConsulta;
        public List<Problemas_Consulta> DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }


        private List<Diagnosticos> _ComboDiagnosticosItems;
           public List<Diagnosticos> ComboDiagnosticosItems
        {
            get { return _ComboDiagnosticosItems; }
            set
            {
                _ComboDiagnosticosItems = value;
                this.RaisePropertyChanged("ComboDiagnosticosItems");
            }
        }


           private Diagnosticos _SelectedDiagnostic;
           public Diagnosticos SelectedDiagnostic
           {
               get { return _SelectedDiagnostic; }
               set
               {
                   _SelectedDiagnostic = value;
                   this.RaisePropertyChanged("SelectedDiagnostic");
               }
           }

        
        #region Delegate
        public Action RefreshConsultationDX { get; set; }
        #endregion

        #region Fields
        //private WeakReference _weakRefCityCollection;
        //private WeakReference _weakRefStateCollection;
        //private WeakReference _weakRefLocationCollection;
        #endregion

        #region Properties

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #endregion

        #region Commands
        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private ICommand _cancelCommand;
        
        private ICommand _addPhoneCommand;
        private ICommand _deletePhoneCommand;


        public ICommand CancelCommand
        {
            get { return this._cancelCommand ?? (this._cancelCommand = new RelayCommand(OnCancelCustomer)); }
        }

        public ICommand AddPhoneCommand
        {
            get { return this._addPhoneCommand ?? (this._addPhoneCommand = new RelayCommand(OnAddPhone)); }
        }

        public ICommand DeletePhoneCommand
        {
            get { return this._deletePhoneCommand ?? (this._deletePhoneCommand = new RelayCommand(OnDeletePhone, CanDeletePhone)); }
        }

        #endregion

        #region Constructors

        public AddConsultationProblemViewModel(IMessenger messenger, Usuario userLogin, Consultas consultation)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.ConsultaSeleccionada = consultation;
            this.DiagnosticosConsulta = _dataservice.GetProblemsConsultation(consultation);

        }

        public AddConsultationProblemViewModel(IMessenger messenger)
            : base(messenger)
        {
            this.DiagnosticosConsulta = new List<Problemas_Consulta>();
        }

        public AddConsultationProblemViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            this.DiagnosticosConsulta = new List<Problemas_Consulta>();

        }

        public AddConsultationProblemViewModel(IMessenger messenger, Usuario userLogin, List<Problemas_Consulta> customer, Consultas consultation)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.Entity = customer;
            this.ConsultaSeleccionada = consultation;
            this.DiagnosticosConsulta = _dataservice.GetProblemsConsultation(consultation);
            

        }

        public AddConsultationProblemViewModel(IMessenger messenger, Usuario userLogin, List<Problemas_Consulta> customer)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.Entity = customer;
            this.DiagnosticosConsulta = customer;


        }
        #endregion

        #region Privados

        //private void CargarDiagnosticos()
        //{
        //    DataService _dataservice = new DataService();
        //    ComboDiagnosticosItems= _dataservice.GetActiveDX();
        //}

        private void OnSelectSearchItem()
        {
            IsPopupOpen = false;
            //this.RaisePropertyChanged(" SearchText");

            if (this.SelectedDiagnosticID.ToString() != "-1")
            {
                CSLADBEntities contexto = new CSLADBEntities();

                //char
                SelectedDiagnostic = contexto.Diagnosticos.Where((u) => u.Id_Diagnostico == this.SelectedDiagnosticID).FirstOrDefault();
                contexto.Detach(SelectedDiagnostic);
                
            }

            this.SearchText = SelectedDiagnostic.Descripcion;
        }
        #endregion  

        #region Override Methods

        public override void Initialize()
        {
            base.Initialize();

            List<ComboData> comboData = new List<ComboData>();

            comboData.Add(new ComboData { Value = "Crónico", Id = "C" });
            comboData.Add(new ComboData { Value = "Agudo", Id = "A" });
          

            this.ComboItems = comboData;
            //CargarDiagnosticos();

        }

        #endregion

        #region Command Methods
        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }
        private void OnSaveCustomer()
        {
            var returnStatus = false;
            
            returnStatus = !IsInEditMode ? _dataservice.AddProblemConsultation(this.DiagnosticosConsulta.Where(X=>X.isNewItem ==true)) : _dataservice.UpdateProblemConsultation(this.DiagnosticosConsulta);

            if (returnStatus)
            {
                if (RefreshConsultationDX != null)
                    this.RefreshConsultationDX();

                var messageDailog = new MessageDailog()
                {
                    Caption = Resources.MessageResources.DataSavedSuccessfully,
                    DialogButton = DialogButton.Ok,
                    Title = Resources.TitleResources.Information
                };

                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

                if (this.CloseWindow != null)
                    this.CloseWindow();
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            }
        }


        private void OnCancelCustomer()
        {
            var messageDailog = new MessageDailog((result) =>
                {
                    if (result == DialogResult.Ok)
                    {
                        if (this.ParentViewModel != null)
                            this.ParentViewModel.ChildViewModel = null;
                        this.Unload();

                        if (this.CloseWindow != null)
                            this.CloseWindow();
                    }
                }) { Caption = Resources.MessageResources.CancelWindowMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);


        }

        private void OnAddPhone()
        {
            IsInEditMode = false;
            if (ConsultaSeleccionada != null)
            {
                if (this.DiagnosticosConsulta == null)
                    this.DiagnosticosConsulta = new List<Problemas_Consulta>();

                var tempCollection = this.DiagnosticosConsulta;
                tempCollection.Add(new Problemas_Consulta() {  FechaRegistro = DateTime.Now, Orden = tempCollection.Count + 1, idConsulta = ConsultaSeleccionada.ID, Estado = 1, idUsuario = this.UserLogin.Id, isNewItem = true });
                this.DiagnosticosConsulta = null;
                this.DiagnosticosConsulta = tempCollection;
                this.RaisePropertyChanged("DiagnosticosConsulta");
                this.SearchText = string.Empty;
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = "Se debe seleccionar una consulta antes.", DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            }
            //CargarDiagnosticos();

        }
        private bool CanDeletePhone()
        {
            return this.Entity != null && this.DiagnosticosConsulta != null &&
                   this.DiagnosticosConsulta.Any(x => x.IsSelected);
        }

        private void OnDeletePhone()
        {
            
            if (this.DiagnosticosConsulta != null &&
                 this.DiagnosticosConsulta.Any(x => x.IsSelected))
            {
                //char
                var qureyItems = this.DiagnosticosConsulta.Where(x =>  x.idProblemaConsulta > 0 && (x.IsSelected) ).ToList();
                if (qureyItems.Any())
                {
                    _dataservice.DeleteProblemConsultation(qureyItems);
                    qureyItems.ForEach(x => x.IsDeleted = true);
                }

                var items = this.DiagnosticosConsulta.Where(x => x.IsSelected && (x.idProblemaConsulta == 0)).ToList();

                if (items.Any())
                {
                    foreach (var customerPhone in items)
                    {
                        this.DiagnosticosConsulta.Remove(customerPhone);
                    }
                    var tempCollection = new List<Problemas_Consulta>(this.DiagnosticosConsulta.ToList());
                    this.DiagnosticosConsulta = null;
                    this.DiagnosticosConsulta = tempCollection;
                    this.RaisePropertyChanged("DiagnosticosConsulta");
                }
            }

            this.SearchText = "";
        }



        #endregion



    }
}