﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class AddConsultationVacunasViewModel : BaseViewModel<List<Vacunas_Consulta>>
    {

        private ICommand _selectSearchItemCommand;
        private DataService _dataservice = new DataService();

        private string _searchTextCode;
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                this.RaisePropertyChanged("SearchText");
                if(_searchText.Length >=3)
                    GetSearchList(_searchText,false);
            }
        }

        public string SearchTextCode
        {
            get { return _searchTextCode; }
            set
            {
                _searchTextCode = value;
                this.RaisePropertyChanged("SearchTextCode");
                if (_searchTextCode.Length >= 2)
                    GetSearchList(_searchTextCode,true);
            }
        }

        private bool _isPopupOpen;
        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set
            {
                _isPopupOpen = value;
                this.RaisePropertyChanged("IsPopupOpen");
            }
        }

        private int _SelectedDiagnosticID;
        public int SelectedDiagnosticID
        {
            get { return _SelectedDiagnosticID; }
            set
            {
                _SelectedDiagnosticID = value;
                this.RaisePropertyChanged("SelectedDiagnosticID");
            }
        }


        

        private IDictionary<String, String> _customerSearchList;
        public IDictionary<String, String> CustomerSearchList
        {
            get { return _customerSearchList; }
            set
            {
                _customerSearchList = value;
                this.RaisePropertyChanged(" CustomerSearchList");
            }
        }

        private void GetSearchList(string strSearchText,bool isCode)
        {
            try
            {
                if (string.IsNullOrEmpty(strSearchText))
                {
                    CustomerSearchList = null;
                    IsPopupOpen = false;
                    return;
                    
                }

               
                    Task.Factory.StartNew(() =>
                    {
                        CustomerSearchList = _dataservice.GetActiveVacunasFilter(strSearchText,isCode);
                        IsPopupOpen = true;
                        if (CustomerSearchList == null || !CustomerSearchList.Any())
                        {
                            CustomerSearchList = new Dictionary<String, String>();
                            CustomerSearchList.Add(new KeyValuePair<string, string>("", "No se encontraron registros."));

                        }

                        this.RaisePropertyChanged("CustomerSearchList");
                    });
               

                this.SelectedDiagnosticID = 0;

            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured, ExceptionResources.ExceptionOccuredLogDetail);
            }

        }

        public ICommand SelectSearchItemCommand
        {
            get
            {
                return this._selectSearchItemCommand ??
                       (this._selectSearchItemCommand = new RelayCommand(OnSelectSearchItem));
            }
        }

        private Consultas _consulta;
        public Consultas ConsultaSeleccionada
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("ConsultaSeleccionada");
            }
        }

        private List<Vacunas_Consulta> _DiagnosticosConsulta;
        public List<Vacunas_Consulta> DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }


        private List<Vacunas> _ComboDiagnosticosItems;
           public List<Vacunas> ComboDiagnosticosItems
        {
            get { return _ComboDiagnosticosItems; }
            set
            {
                _ComboDiagnosticosItems = value;
                this.RaisePropertyChanged("ComboDiagnosticosItems");
            }
        }


           private Vacunas _SelectedDiagnostic;
           public Vacunas SelectedDiagnostic
           {
               get { return _SelectedDiagnostic; }
               set
               {
                   _SelectedDiagnostic = value;
                   this.RaisePropertyChanged("SelectedDiagnostic");
               }
           }

        
        #region Delegate
        public Action RefreshConsultationDX { get; set; }
        #endregion

        #region Fields
       
        #endregion

        #region Properties

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #endregion

        #region Commands
        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private ICommand _cancelCommand;
        
        private ICommand _addPhoneCommand;
        private ICommand _deletePhoneCommand;


        public ICommand CancelCommand
        {
            get { return this._cancelCommand ?? (this._cancelCommand = new RelayCommand(OnCancelCustomer)); }
        }

        public ICommand AddPhoneCommand
        {
            get { return this._addPhoneCommand ?? (this._addPhoneCommand = new RelayCommand(OnAddPhone)); }
        }

        public ICommand DeletePhoneCommand
        {
            get { return this._deletePhoneCommand ?? (this._deletePhoneCommand = new RelayCommand(OnDeletePhone, CanDeletePhone)); }
        }

        #endregion

        #region Constructors

        public AddConsultationVacunasViewModel(IMessenger messenger, Usuario userLogin, Consultas consultation)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.ConsultaSeleccionada = consultation;
            this.DiagnosticosConsulta = _dataservice.GetVacunasConsultation(consultation);

        }

        public AddConsultationVacunasViewModel(IMessenger messenger)
            : base(messenger)
        {
            this.DiagnosticosConsulta = new List<Vacunas_Consulta>();
        }

        public AddConsultationVacunasViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            this.DiagnosticosConsulta = new List<Vacunas_Consulta>();

        }

        public AddConsultationVacunasViewModel(IMessenger messenger, Usuario userLogin, List<Vacunas_Consulta> customer, Consultas consultation)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.Entity = customer;
            this.ConsultaSeleccionada = consultation;
            this.DiagnosticosConsulta = _dataservice.GetVacunasConsultation(consultation);
            

        }

        public AddConsultationVacunasViewModel(IMessenger messenger, Usuario userLogin, List<Vacunas_Consulta> customer)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.Entity = customer;
            this.DiagnosticosConsulta = customer;


        }
        #endregion

        #region Privados

        private void OnSelectSearchItem()
        {
            IsPopupOpen = false;
            //this.RaisePropertyChanged(" SearchText");

            if (this.SelectedDiagnosticID.ToString() != "-1")
            {
                CSLADBEntities contexto = new CSLADBEntities();

                //char
                SelectedDiagnostic = contexto.Vacunas.Where((u) => u.idVacuna == this.SelectedDiagnosticID).FirstOrDefault();
                contexto.Detach(SelectedDiagnostic);
                
            }

            this.SearchText = SelectedDiagnostic.Descripcion;
        }
        #endregion  

        #region Override Methods

        public override void Initialize()
        {
            base.Initialize();

            //CargarDiagnosticos();

        }

        #endregion

        #region Command Methods
        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }
        private void OnSaveCustomer()
        {
            var returnStatus = false;
                        //char
            returnStatus = !IsInEditMode ? _dataservice.AddVacunaConsultation(this.DiagnosticosConsulta) : _dataservice.UpdateVacunasConsultation(this.DiagnosticosConsulta);

            if (returnStatus)
            {
                if (RefreshConsultationDX != null)
                    this.RefreshConsultationDX();

                var messageDailog = new MessageDailog()
                {
                    Caption = Resources.MessageResources.DataSavedSuccessfully,
                    DialogButton = DialogButton.Ok,
                    Title = Resources.TitleResources.Information
                };

                //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
                MessengerInstance.Send(messageDailog);

                if (this.CloseWindow != null)
                    this.CloseWindow();
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                //Messenger.Default.Send(messageDailog);
                MessengerInstance.Send(messageDailog);
            }
        }


        private void OnCancelCustomer()
        {
            var messageDailog = new MessageDailog((result) =>
                {
                    if (result == DialogResult.Ok)
                    {
                        if (this.ParentViewModel != null)
                            this.ParentViewModel.ChildViewModel = null;
                        this.Unload();

                        if (this.CloseWindow != null)
                            this.CloseWindow();
                    }
                }) { Caption = Resources.MessageResources.CancelWindowMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);


        }

        private void OnAddPhone()
        {
            IsInEditMode = false;
            if (ConsultaSeleccionada != null)
            {
                if (this.DiagnosticosConsulta == null)
                    this.DiagnosticosConsulta = new List<Vacunas_Consulta>();

                var tempCollection = this.DiagnosticosConsulta;
                tempCollection.Add(new Vacunas_Consulta() { Vacunas = SelectedDiagnostic, Fecha_Aplicacion = DateTime.Now, Secuencia = tempCollection.Count + 1, idConsulta = ConsultaSeleccionada.ID, Tipo_Vacuna = 0, isDeleted = false, idUsuario = this.UserLogin.Id });
                this.DiagnosticosConsulta = null;
                this.DiagnosticosConsulta = tempCollection;
                this.RaisePropertyChanged("DiagnosticosConsulta");
                this.SearchText = string.Empty;
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = "Se debe seleccionar una consulta antes.", DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            }
            //CargarDiagnosticos();

        }

        private bool CanDeletePhone()
        {
            return this.Entity != null && this.DiagnosticosConsulta != null &&
                   this.DiagnosticosConsulta.Any(x => x.IsSelected);
        }

        private void OnDeletePhone()
        {
            
            if (this.DiagnosticosConsulta != null &&
                 this.DiagnosticosConsulta.Any(x => x.IsSelected))
            {
                //char
                var qureyItems = this.DiagnosticosConsulta.Where(x =>  x.IdVacunaConsulta > 0 && (x.IsSelected) ).ToList();
                if (qureyItems.Any())
                {
                    _dataservice.DeleteVacunaConsultation(qureyItems);
                    qureyItems.ForEach(x => x.isDeleted = true);
                }

                var items = this.DiagnosticosConsulta.Where(x => x.IsSelected && (x.IdVacunaConsulta == 0)).ToList();

                if (items.Any())
                {
                    foreach (var customerPhone in items)
                    {
                        this.DiagnosticosConsulta.Remove(customerPhone);
                    }
                    var tempCollection = new List<Vacunas_Consulta>(this.DiagnosticosConsulta.ToList());
                    this.DiagnosticosConsulta = null;
                    this.DiagnosticosConsulta = tempCollection;
                    this.RaisePropertyChanged("DiagnosticosConsulta");
                }
            }

            this.SearchText = "";
        }



        #endregion



    }
}