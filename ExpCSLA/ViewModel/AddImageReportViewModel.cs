﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.ComponentModel;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class AddImageReportViewModel : BaseViewModel<PlantillaMotivoConsulta>, IDataErrorInfo
    {


        private DataService _dataservice = new DataService();

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private ICommand _PrintReport;
        public ICommand PrintReport
        {
            get { return this._PrintReport ?? (this._PrintReport = new RelayCommand(OnPrintReport, ConsultaGuardada)); }
        }
        

        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }

        public bool ConsultaGuardada()
        {
            return this.Entity.Consultas != null; //&& this.Entity.HasValueInAllRequiredField;
        }

        private List<Motivos> _MotiveLists;
        public List<Motivos> MotiveLists
        {
            get { return _MotiveLists; }
            set
            {
                _MotiveLists = value;
                this.RaisePropertyChanged("MotiveLists");
            }
        }

        private Motivos _SelectedMotive;
        public Motivos SelectedMotive
        {
            get { return _SelectedMotive; }
            set
            {
                _SelectedMotive = value;
                this.RaisePropertyChanged("SelectedMotive");
                CambioMotivo();
            }
        }

        private Consultas _consulta;
        public Consultas ConsultaSeleccionada
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("ConsultaSeleccionada");
            }
        }

        /// <summary>
        /// Initializes a new instance of the AddImageReportViewModel class.
        /// </summary>
        public AddImageReportViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {

            MotiveLists = _dataservice.GetActiveMotives().Where(u => u.ConReporte == true).ToList();

        }

        public AddImageReportViewModel(IMessenger messenger, Usuario userLogin, Consultas consultation)
            : base(messenger, userLogin)
        {

            this.ConsultaSeleccionada = consultation;
            if (this.ConsultaSeleccionada.PlantillaMotivoConsulta.Count >= 1)
            {
                IsInEditMode = true;
                this.Entity = this.ConsultaSeleccionada.PlantillaMotivoConsulta.FirstOrDefault();
                this.SelectedMotive = this.Entity.Motivos;
                this.Entity.IdMedico = this.UserLogin.IdMedico;
                
            }
            else
            {
                IsInEditMode = false;
                this.Entity = new PlantillaMotivoConsulta() { IdConsulta = ConsultaSeleccionada.ID, FechaRegistro = DateTime.Now, IdMedico = this.UserLogin.IdMedico };
                this.SelectedMotive = this.ConsultaSeleccionada.Motivos;
            }

            MotiveLists = _dataservice.GetActiveMotives().Where(u => u.ConReporte == true).ToList();

        }

       

        private void CambioMotivo()
        {
            var rejectStatus = _dataservice.GetPlantillasMotive(SelectedMotive.IdMotivo).FirstOrDefault();

            if (rejectStatus != null)
            {
                PlantillaMotivoConsulta existe = _dataservice.GetPlantillaMotivoConsulta(int.Parse(this.Entity.IdConsulta.ToString()), SelectedMotive.IdMotivo);

                if (existe != null)
                {
                    if (existe.IdMotivo == SelectedMotive.IdMotivo)
                    {
                        this.Entity = existe;
                        
                    }
                    else
                    {
                        this.Entity.Resultado = rejectStatus.Plantillas.Resultado;
                        this.Entity.ImpresionDiag = rejectStatus.Plantillas.ImpresionDiag;
                        this.Entity.IdPlantillaOriginal = rejectStatus.IdPlantilla;
                        this.Entity.IdMotivo = SelectedMotive.IdMotivo;
                        this.Entity.Encabezado = rejectStatus.Plantillas.Descripcion;
                        this.Entity.IdMedico = this.UserLogin.IdMedico;
                        
                    }
                }else
                {
                    this.Entity.Resultado = rejectStatus.Plantillas.Resultado;
                    this.Entity.ImpresionDiag = rejectStatus.Plantillas.ImpresionDiag;
                    this.Entity.IdPlantillaOriginal = rejectStatus.IdPlantilla;
                    this.Entity.IdMotivo = SelectedMotive.IdMotivo;
                    this.Entity.Encabezado = rejectStatus.Plantillas.Descripcion;
                    this.Entity.IdMedico = this.UserLogin.IdMedico;
                }
                
                //this.Entity = RESULTADO;
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = "No existe una plantilla asignada.", DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
                this.Entity = new PlantillaMotivoConsulta() { IdConsulta = ConsultaSeleccionada.ID, FechaRegistro = DateTime.Now, IdMedico = this.UserLogin.IdMedico };
            }

        }

        private void OnSaveCustomer()
        {
            var returnStatus = false;
            //char
            //Views.ViewerReporte a = new Views.ViewerReporte(2);
            if (this.ConsultaSeleccionada != null)
            {
                returnStatus = !IsInEditMode ? _dataservice.AddPlantillaMotivoConsulta(this.Entity) : _dataservice.UpdatePlantillaMotivoConsulta(this.Entity);

                if (returnStatus)
                {
                    this.Entity = _dataservice.GetPlantillaMotivoConsulta(int.Parse(this.Entity.IdConsulta.ToString()));

                    var messageDailog = new MessageDailog()
                    {
                        Caption = Resources.MessageResources.DataSavedSuccessfully,
                        DialogButton = DialogButton.Ok,
                        Title = Resources.TitleResources.Information
                    };

                    MessengerInstance.Send(messageDailog);

                    if (!IsInEditMode)
                    {
                        Views.ViewerReporte a = new Views.ViewerReporte(this.Entity.IdPlantillaMotivoConsulta, true);
                    }

                    if (this.CloseWindow != null)
                        this.CloseWindow();
                }
                else
                {
                    var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                    MessengerInstance.Send(messageDailog);
                }
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = "Debe seleccionar una cita, antes de guardar. !!", DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                MessengerInstance.Send(messageDailog);
            }
        }

        private void OnPrintReport()
        {
            Views.ViewerReporte a = new Views.ViewerReporte(this.Entity.IdPlantillaMotivoConsulta,false);
            a.Show();
        }

        #region ErrorControl
        private string error = string.Empty;
        public string Error
        {
            get { return error; }
        }
        public string this[string columnName]
        {
            get
            {
                error = string.Empty;
                if (columnName == "Resultado" && string.IsNullOrWhiteSpace(this.Entity.Resultado))
                {
                    error = "El resultado es requerido!";
                }
                else
                {
                    error = "Campo requerido!";
                }

                return error;

            }
        }
        #endregion
    }
}