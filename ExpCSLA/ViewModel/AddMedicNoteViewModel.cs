﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.ComponentModel;

namespace ExpCSF.ViewModel
{
    class AddMedicNoteViewModel : BaseViewModel<Datos_Consulta>, IDataErrorInfo
    {
        private DataService _dataservice = new DataService();

        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private Consultas _consulta;
        public Consultas ConsultaSeleccionada
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("ConsultaSeleccionada");
            }
        }

        private Datos_Consulta _DiagnosticosConsulta;
        public Datos_Consulta DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.Entity = value;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }

        #region Constructors
        public AddMedicNoteViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            
            AgregarDatosConsulta();
        }

        public AddMedicNoteViewModel(IMessenger messenger, Usuario userLogin, Datos_Consulta patient)
            : base(messenger, userLogin)
        {

            IsInEditMode = true;
            this.Entity = patient;
            AgregarDatosConsulta();

        }

        public AddMedicNoteViewModel(IMessenger messenger, Usuario userLogin, Consultas consultation)
            : base(messenger,userLogin)
        {
            IsInEditMode = true;
            this.ConsultaSeleccionada = consultation;
            this.Entity = _dataservice.GetMedicNoteConsultation(consultation);
            if (this.Entity == null)
            {
                AgregarDatosConsulta();
                IsInEditMode = false;
            }
        }
        #endregion

        #region Virtual Methods
        public override void Initialize()
        {
            base.Initialize();
            //this.SignInCommand = new RelayCommand(OnDataProcess, CanExecuteSingInCommand);
        }
        #endregion

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;

            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;


            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #region ErrorControl
        private string error = string.Empty;
        public string Error
        {
            get { return error; }
        }
        public string this[string columnName]
        {
            get
            {
                error = string.Empty;
                if (columnName == "Evolucion" && string.IsNullOrWhiteSpace(this.Entity.Evolucion))
                {
                    error = "La evolución es requerida!";
                }
                else
                {
                    error = "Campo requerido!";
                }
            
                return error;

            }
        }
        #endregion

        public Action RefreshConsultationDX { get; set; }

        private void OnSaveCustomer()
        {
            var returnStatus = false;
            //char
            Datos_Consulta datosNota ;
            if (IsInEditMode == true)
            {
                datosNota = _dataservice.GetMedicNoteConsultation(this.ConsultaSeleccionada);
                if (datosNota != null)
                {
                    this.Entity.idDatosConsulta = datosNota.idDatosConsulta;
                }
            }

            returnStatus = !IsInEditMode ? _dataservice.AddMedicNoteConsultation(this.Entity) : _dataservice.UpdateMedicNoteConsultation(this.Entity);

            if (returnStatus)
            {
                if (RefreshConsultationDX != null)
                    this.RefreshConsultationDX();

                var messageDailog = new MessageDailog()
                {
                    Caption = Resources.MessageResources.DataSavedSuccessfully,
                    DialogButton = DialogButton.Ok,
                    Title = Resources.TitleResources.Information
                };

                //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
                MessengerInstance.Send(messageDailog);

                this.IsInEditMode = true;

                if (this.CloseWindow != null)
                    this.CloseWindow();
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                //Messenger.Default.Send(messageDailog);
                MessengerInstance.Send(messageDailog);
            }
        }

        private void AgregarDatosConsulta()
        {
            try
            {
                                                
                //UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
                //_dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals()).FirstOrDefault();
               
                if (ConsultaSeleccionada != null)
                {
                    Especialidades pEspecialidad;
                    if (ConsultaSeleccionada.medicos.Especialidades_Medico.FirstOrDefault() != null)
                    {
                        pEspecialidad = ConsultaSeleccionada.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
                    }
                    else if (ConsultaSeleccionada.Especialidades != null)
                    {
                        pEspecialidad = ConsultaSeleccionada.Especialidades;
                    }
                    else
                    {
                        pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
                    }

                    

                    if (this.DiagnosticosConsulta == null)
                        this.DiagnosticosConsulta = new Datos_Consulta() { FechaRegistro = DateTime.Now, idEspecialidad = pEspecialidad.ID, Estado = true, idUsuario = this.UserLogin.Id, idConsulta = ConsultaSeleccionada.ID };
                                                                               
                    this.RaisePropertyChanged("DiagnosticosConsulta");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
