﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExpCSF.Base;
using ExpWCF;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Controls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using GalaSoft.MvvmLight.Command;
using System.Data;

namespace ExpCSF.ViewModel
{
    public class AddPatientViewModel : BaseViewModel<Pacientes>, IDataErrorInfo
    {
        private Regex emailRegex = new Regex(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$");

        private RelayCommand _saveCommand;
        private RelayCommand _cancelCommand;

        private string error = string.Empty;
        public string Error
        {
            get { return error; }
        }

        public string this[string columnName]
        {
            get
            {
                error = string.Empty;

                if (columnName == "EmployeeIdentification" && string.IsNullOrWhiteSpace(EmployeeIdentification))
                {
                    error = "La cédula es requerida!";
                }
                else
                    if (columnName == "EmployeeName" && string.IsNullOrWhiteSpace(EmployeeName))
                    {
                        error = "El Nombre es requerido!";
                    }
                    else
                        if (columnName == "EmployeeLastName" && string.IsNullOrWhiteSpace(EmployeeLastName))
                        {
                            error = "El Apellido es requerido!";
                        }
                        else
                            if (columnName == "EmployeePhoneNumber" && string.IsNullOrWhiteSpace(EmployeePhoneNumber))
                            {
                                error = "El Teléfono es requerido!";
                            }
                             else
                                    if (columnName == "GenderSelected" && GenderSelected == null)
                                    {
                                        error = "Campo requerido!";
                                    }
                //else if (columnName == "PhoneNumber" && PhoneNumber == 0)
                //{
                //    error = "Phone number is required!";
                //}
                //else if (columnName == "PhoneNumber" && PhoneNumber.ToString().Length > 10)
                //{
                //    error = "Phone number must have less than or equal to 10 digits!";
                //}
                //else if (columnName == "Email" && string.IsNullOrWhiteSpace(Email))
                //{
                //    error = "Email address is required!";
                //}
                //else if (columnName == "Email" && !IsValidEmailAddress)
                //{
                //    error = "Please enter valid email address!";
                //}
                return error;

            }
        }

        #region Commands

        public RelayCommand SaveCommand
        {
            get { return _saveCommand; }
            set
            {
                _saveCommand = value;
                this.RaisePropertyChanged("SaveCommand");
            }
        }

        public RelayCommand CancelCommand
        {
            get { return _cancelCommand; }
            set
            {
                _cancelCommand = value;
                this.RaisePropertyChanged("CancelCommand");
            }
        }

        #endregion

        #region Properties

        
        public string EmployeeIdentification
        {
            get
            {
                return this.Entity != null ? this.Entity.Cedula : String.Empty;
            }
            set { this.Entity.Cedula = value; RaisePropertyChanged("EmployeeIdentification"); }
        }

        
        public string EmployeeName
        {
            get
            {
                return this.Entity != null ? this.Entity.Nombre : String.Empty;
            }
            set { this.Entity.Nombre = value; RaisePropertyChanged("EmployeeName"); }
        }

        
        public string EmployeeLastName
        {
            get
            {
                return this.Entity != null ? this.Entity.Apellidos : String.Empty;
            }
            set { this.Entity.Apellidos = value; RaisePropertyChanged("EmployeeLastName"); }
        }


        
        public string EmployeePhoneNumber
        {
            get
            {
                return this.Entity != null ? this.Entity.Telefono1 : String.Empty;
            }
            set { this.Entity.Telefono1 = value; RaisePropertyChanged("EmployeePhoneNumber"); }
        }

        
        public DateTime? DateOfBirth
        {
            get
            {
                return this.Entity != null ? this.Entity.FechaNacimiento : null;
            }
            set
            {
                this.Entity.FechaNacimiento = value;
                RaisePropertyChanged("DateOfBirth");
            }
        }

        List<ComboData> comboItems = new List<ComboData>();
        public List<ComboData> ComboItems
        {
            get
            {
                return comboItems;
            }
            set
            {
                comboItems = value;
                this.RaisePropertyChanged("ComboItems");
            }
        }

        object _GenderSelected;
        public object GenderSelected
        {
            get
            {
                return _GenderSelected;
            }
            set
            {
                if (value != null)
                {
                    if (value.GetType().Name == "ComboData")
                    {
                        _GenderSelected = value;
                        this.Entity.Sexo = Convert.ToByte(((ComboData)value).Id);
                        //Convert.ToByte(((ComboData)value).Id);
                    }
                    else
                    {
                        _GenderSelected = ComboItems.Where(x => x.Id.Equals(value.ToString())).FirstOrDefault();
                        this.Entity.Sexo = Convert.ToByte(value);
                    }

                }
                this.RaisePropertyChanged("GenderSelected");
            }
        }

        List<GeographicDistribution> _provinces;
        public List<GeographicDistribution> Provinces
        {
            get
            {
                return _provinces;
            }
            set
            {
                _provinces = value;
                this.RaisePropertyChanged("Provinces");
            }
        }

        GeographicDistribution _provinceSelected;
        public GeographicDistribution ProvinceSelected
        {
            get
            {
                return _provinceSelected;
            }
            set
            {
                _provinceSelected = value;
                
                if (value != null)
                {
                    this.Entity.GeographicDistributionId = value.GeographicDistributionId;

                    DataService _dataservice = new DataService();
                    this._cantons = _dataservice.GetGeographicDistribution(value.GeographicDistributionId);
                    this.RaisePropertyChanged("Cantons");
                }
                this.RaisePropertyChanged("ProvinceSelected");
            }
        }

        List<GeographicDistribution> _cantons;
        public List<GeographicDistribution> Cantons
        {
            get
            {
                return _cantons;
            }
            set
            {
                _cantons = value;
                this.RaisePropertyChanged("Cantons");
            }
        }

        GeographicDistribution _cantonSelected;
        public GeographicDistribution CantonSelected
        {
            get
            {
                return _cantonSelected;
            }
            set
            {
                _cantonSelected = value;

                //if (value != null)
                //{
                //    this.Entity.GeographicDistributionId = value.GeographicDistributionId;

                //    DataService _dataservice = new DataService();
                //    this._districts = _dataservice.GetGeographicDistribution(value.GeographicDistributionId);
                //    this.RaisePropertyChanged("Districts");
                //}
                //else
                //{
                //    this._districts = null;
                //    this.RaisePropertyChanged("Districts");
                //}

                this.RaisePropertyChanged("CantonSelected");
            }
        }

        List<GeographicDistribution> _districts;
        public List<GeographicDistribution> Districts
        {
            get
            {
                return _districts;
            }
            set
            {
                _districts = value;
                this.RaisePropertyChanged("Districts");
            }
        }

        GeographicDistribution _districtSelected;
        public GeographicDistribution DistrictSelected
        {
            get
            {
                return _districtSelected;
            }
            set
            {
                _districtSelected = value;

                //if (value != null)
                //{
                //    this.Entity.GeographicDistributionId = value.GeographicDistributionId;
                //}

                this.RaisePropertyChanged("DistrictSelected");
            }
        }
        #endregion

        #region Constructors
        public AddPatientViewModel(IMessenger messenger)
            : base(messenger)
        {
            //this.Entity.CustomerAddresseCollection = new Collection<CustomerAddress> {new CustomerAddress()};
            //this.Entity.CustomerPhoneCollection = new Collection<CustomerPhone> { new CustomerPhone() };
        }

        public AddPatientViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            //this.Entity.CustomerAddresseCollection = new Collection<CustomerAddress> { new CustomerAddress(){} };
            //this.Entity.CustomerPhoneCollection = new Collection<CustomerPhone> { new CustomerPhone() };

            //if (this.Entity != null)
            //    this.Entity.CustomerAddresseCollection.ForEach(x =>
            //    {
            //        x.PropertyChanged += (sender, args) =>
            //        {
            //            if (args.PropertyName == "City")
            //            {
            //                var item = sender as CustomerAddress;

            //                if (item != null)
            //                {
            //                    item.IsOtherCity = (item.City == "Other");

            //                    if (!_weakRefLocationCollection.IsAlive)
            //                        GetLocationCollection();
            //                    if (_weakRefLocationCollection.IsAlive)
            //                    {
            //                        var collection = _weakRefLocationCollection.Target as Dictionary<string, string>;
            //                        if (collection != null)
            //                            item.State = collection.FirstOrDefault(y => y.Key == item.City).Value;
            //                        item.Country = CountryCollection.FirstOrDefault();
            //                    }
            //                }
            //            }
            //        };
            //    });
        }

        public AddPatientViewModel(IMessenger messenger, Usuario userLogin, Pacientes customer)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.Entity = customer;
            this.GenderSelected = this.Entity.Sexo;
            decimal? DatosGeographic = this.Entity.GeographicDistributionId;
            this.ProvinceSelected = Provinces.Where(x => x.GeographicDistributionId == DatosGeographic).FirstOrDefault();

            //this.Entity.CustomerAddresseCollection = new [] {customer.FirstOrDefaultAddress};
            //this.Entity.CustomerAddresseCollection = customer.CustomerAddresseCollection;
            //this.Entity.CustomerPhoneCollection = customer.CustomerPhoneCollection;

            //if (this.Entity != null)
            //    this.Entity.CustomerAddresseCollection.ForEach(x =>
            //    {
            //        x.PropertyChanged += (sender, args) =>
            //        {
            //            if (args.PropertyName == "City")
            //            {
            //                var item = sender as CustomerAddress;
            //                if (item != null)
            //                {
            //                    item.IsOtherCity = (item.City == "Other");
            //                    if (_weakRefLocationCollection.IsAlive)
            //                    {
            //                        var collection = _weakRefLocationCollection.Target as Dictionary<string, string>;
            //                        item.State = collection.FirstOrDefault(y => y.Key == item.City).Value;
            //                    }
            //                }
            //            }
            //        };
            //    });
        }

        public AddPatientViewModel(IMessenger messenger, Usuario userLogin,String _Cedula)
            : base(messenger)
        {
            this.EmployeeIdentification = _Cedula;
            //this.Entity.CustomerAddresseCollection = new Collection<CustomerAddress> {new CustomerAddress()};
            //this.Entity.CustomerPhoneCollection = new Collection<CustomerPhone> { new CustomerPhone() };
        }
        #endregion

        #region Command Methods
        //public void ExecuteSingInCommand()
        //{
        //    this.OnDataProcess();
        //}
        public bool CanExecuteSingInCommand()
        {
            return (!string.IsNullOrEmpty(this.EmployeeIdentification) && !string.IsNullOrEmpty(this.EmployeeName)
                && !string.IsNullOrEmpty(this.EmployeeLastName) && GenderSelected != null );
        }
        #endregion

        #region Virtual Methods
        public override void Initialize()
        {
            base.Initialize();
            this.SaveCommand = new RelayCommand(OnDataProcess, CanExecuteSingInCommand);
            this.CancelCommand = new RelayCommand(OnCancel);

            List<ComboData> comboData = new List<ComboData>();

            comboData.Add(new ComboData { Value = "Femenino", Id = "1" });
            comboData.Add(new ComboData { Value = "Masculino", Id = "2" });

            this.comboItems = comboData;

            DataService _dataservice = new DataService();
            this._provinces = _dataservice.GetGeographicDistribution(null);
        }

        protected void OnCancel()
        {
            this.CloseWindow();
        }

        protected override void OnAction(ActionResult<Pacientes> result)
        {
            try
            {

                DataService _dataservice = new DataService();

                var p = this.Entity;
                p.NombreCompleto = p.Nombre.Trim() + " " + p.Apellidos.Trim();

                if (p.IdPaciente == 0)
                {
                    if (_dataservice.PatientInsert(p))
                    {
                        result.Result = ActionResultType.DataFetched;
                        StatusMessage = "Paciente almacenado exitosamente.";
                    }
                }
                else
                {

                    if (_dataservice.PatientUpdate(p))
                    {
                        result.Result = ActionResultType.DataFetched;
                        StatusMessage = "Paciente almacenado exitosamente.";
                    }
                }

            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured,
                //                    ExceptionResources.ExceptionOccuredLogDetail);
            }
        }
        #endregion

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;

            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

    }

    public class ComboData
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }
}


