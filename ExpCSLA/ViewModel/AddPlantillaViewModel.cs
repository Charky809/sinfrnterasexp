﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.ComponentModel;
using ExpCSF.Resources;

namespace ExpCSF.ViewModel
{
    class AddPlantillaViewModel : BaseViewModel<Plantillas>, IDataErrorInfo
    {
        private DataService _dataservice = new DataService();

        private Plantillas _selectedMenuItem;
        public Plantillas SelectedMenuItem
        {
            get
            {
               return _selectedMenuItem;
               
            }
            set
            {
                _selectedMenuItem = value;
                this.RaisePropertyChanged("SelectedMenuItem");
                OnSelectedMenuItem();
            }
        }

        private List<Plantillas> _MenuList;
        public List<Plantillas> MenuList
        {
            get
            {
                List<Plantillas> itemsMenu = new List<Plantillas>();
                
                itemsMenu = _dataservice.GetPlantillas();
                
                return itemsMenu;
            }
            set {
                _MenuList = value;
                this.RaisePropertyChanged("MenuList");
            }
            
        }

        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private Plantillas _DiagnosticosConsulta;
        public Plantillas DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.Entity = value;
                IsInEditMode = true;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }

        #region Constructors
        public AddPlantillaViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {

            AgregarDatosConsulta();
        }

        public AddPlantillaViewModel(IMessenger messenger, Usuario userLogin, Plantillas patient)
            : base(messenger, userLogin)
        {

            this.Entity = patient;
            AgregarDatosConsulta();

        }

              #endregion

        #region Virtual Methods
        public override void Initialize()
        {
            base.Initialize();
            DeleteCommand = new RelayCommand(OnDeleteItem, CanDeleteItem);
            //this.SignInCommand = new RelayCommand(OnDataProcess, CanExecuteSingInCommand);
        }

        public override void OnDeleteItem()
        {
            try
            {
                _dataservice.DeletePlantilla(this.Entity);
                var a = MenuList;
                IsInEditMode = false;
                
            }
            catch (Exception )
            {

            }
        }
        #endregion

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;

            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;


            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #region ErrorControl
        private string error = string.Empty;
        public string Error
        {
            get { return error; }
        }
        public string this[string columnName]
        {
            get
            {
                error = string.Empty;
                if (columnName == "Resultado" && string.IsNullOrWhiteSpace(this.Entity.Resultado))
                {
                    error = "El resultado es requerido!";
                }
                else
                {
                    error = "Campo requerido!";
                }

                return error;

            }
        }
        #endregion

        
        private void OnSaveCustomer()
        {
            var returnStatus = false;
            //char
            //Views.ViewerReporte a = new Views.ViewerReporte(2);

            returnStatus = this.Entity.IdPlantilla == 0 ? _dataservice.AddPlantilla(this.Entity) : _dataservice.UpdatePlantilla(this.Entity);

            if (returnStatus)
            {
               
                var messageDailog = new MessageDailog()
                {
                    Caption = Resources.MessageResources.DataSavedSuccessfully,
                    DialogButton = DialogButton.Ok,
                    Title = Resources.TitleResources.Information
                };

                //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
                MessengerInstance.Send(messageDailog);

                if (this.CloseWindow != null)
                    this.CloseWindow();
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                //Messenger.Default.Send(messageDailog);
                MessengerInstance.Send(messageDailog);
            }
        }

        private void AgregarDatosConsulta()
        {
            try
            {
                this.DiagnosticosConsulta = null;

                if (this.DiagnosticosConsulta == null)
                    this.DiagnosticosConsulta = new Plantillas() { Descripcion = "", Resultado = "", Encabezado = "", PiePagina = "", ImpresionDiag = "" };

                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void OnSelectedMenuItem()
        {

            this.DiagnosticosConsulta = SelectedMenuItem;
            return;


        }
    }
}
