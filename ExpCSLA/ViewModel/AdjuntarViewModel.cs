﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using ExpCSF.Base;
using ExpCSF.Commands;
using ExpCSF.Controls;
using ExpWCF;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Windows.Media;

namespace ExpCSF.ViewModel
{

    public class AdjuntarViewModel : BaseViewModel<Usuario>
    {
        private RelayCommand _signInCommand;
        private RelayCommand _CerrarCommand;
        private RelayCommand _AdjuntarCommand;
        private RelayCommand _LoadCommand;



        private BitmapImage _generatedImage;

        public BitmapImage GeneratedImage
        {
            get { return _generatedImage; }
            set
            {
                if (value == _generatedImage) return;
                _generatedImage = value;
                RaisePropertyChanged("GeneratedImage");
            }
        }

        private Stream _Source;
        public Stream Source
        {
            get { return _Source; }
            set
            {
                _Source = value;
                this.RaisePropertyChanged("Source");
            }
        }

        private Consultas _selectedConsultation;
        public Consultas SelectedConsultation
        {
            get { return _selectedConsultation; }
            set
            {
                _selectedConsultation = value;
                this.RaisePropertyChanged("SelectedConsultation");
            }
        }

        #region Fields

        #endregion

        #region Properties
        public override string Title
        {
            get
            {
                return "ADJUNTAR ARCHIVOS PDF";
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 35;
            }
        }

        public override double WindowHeight
        {
            get
            {
                return 600;
            }
        }

        public override double WindowWidth
        {
            get
            {
                return 600;
            }
        }

        private string _LoginName;
        public string LoginName
        {
            get { return _LoginName; }
            set
            {
                if (_LoginName != value)
                {
                    _LoginName = value;
                    this.RaisePropertyChanged("LoginName");
                }
            }
        }

        private string _LoginPassword;
        public string LoginPassword
        {
            get { return _LoginPassword; }
            set
            {
                if (_LoginPassword != value)
                {
                    _LoginPassword = value;
                    this.RaisePropertyChanged("LoginPassword");

                }
            }
        }
        #endregion

        #region Commands

        public RelayCommand SignInCommand
        {
            get { return _signInCommand; }
            set
            {
                _signInCommand = value;
                this.RaisePropertyChanged("SignInCommand");
            }
        }

         public RelayCommand CerrarCommand
        {
            get { return _CerrarCommand; }
            set
            {
                _CerrarCommand = value;
                this.RaisePropertyChanged("CerrarCommand");
            }
        }

         public RelayCommand AdjuntarCommand
         {
             get { return _AdjuntarCommand; }
             set
             {
                 _AdjuntarCommand = value;
                 this.RaisePropertyChanged("AdjuntarCommand");
             }
         }

         public RelayCommand LoadCommand
         {
             get { return _LoadCommand; }
             set
             {
                 _LoadCommand = value;
                 this.RaisePropertyChanged("LoadCommand");
             }
         }

        
        

        #endregion

        #region Constructors
        public AdjuntarViewModel(IMessenger messenger)
                : base(messenger)
            {

            }

        public AdjuntarViewModel(IMessenger messenger, Usuario userlogin)
            : base(messenger,userlogin)
        {

        }
        public AdjuntarViewModel(IMessenger messenger, Usuario userlogin,Consultas consulta)
            : base(messenger, userlogin)
        {
            this.SelectedConsultation = consulta;
           

        }
            #endregion

            #region Public Methods

            #endregion

            #region Privae Methods

            #endregion

            #region Command Methods
            //public void ExecuteSingInCommand()
            //{
            //    this.OnDataProcess();
            //}
            public bool CanExecuteSingInCommand()
            {
                return (!string.IsNullOrEmpty(this.LoginName) && !string.IsNullOrEmpty(this.LoginPassword));
            }
            #endregion

            #region Virtual Methods
            public override void Initialize()
            {
                base.Initialize();
                this.SignInCommand = new RelayCommand(OnDataProcess, CanExecuteSingInCommand);

                this.CerrarCommand = new RelayCommand(CerrarHijo);

                this.AdjuntarCommand = new RelayCommand(AdjuntarFile);

                this.LoadCommand = new RelayCommand(LoadFile);

                
            }

            private void CerrarHijo() {
                this.CloseChild();
            }

            private void AdjuntarFile()
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.DefaultExt = ".jpeg"; // Default file extension
                dlg.Filter = "Image files (*.bmp, *.jpg)|*.bmp;*.jpg|All files (*.*)|*.*"; // Filter files by extension 

                // Show open file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process open file dialog box results 
                if (result == true)
                {
                    // Open document 
                    string filename = dlg.FileName;
                    UploadFile(filename);
                }
            }

            private void UploadFile(string fullName)
            {
                string Cnx = string.Empty;
                Cnx = ConfigurationManager.ConnectionStrings["ExpCSF.Properties.Settings.ExpCsfConnectionString"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(Cnx))
                {
                    cn.Open();
                    FileStream fStream = File.OpenRead(fullName);
                    byte[] contents = new byte[fStream.Length];
                    fStream.Read(contents, 0, (int)fStream.Length);
                    fStream.Close();
                    using (SqlCommand cmd = new SqlCommand("insert into SavePDFTable " + "(PDFFile,idConsulta,FechaRegistro,UsuarioId)values(@data,@consulta,@fecha,@usuario)", cn))
                    {
                        //cmd.Parameters.Add("@data", contents);
                        cmd.Parameters.AddWithValue("@data", contents);
                        cmd.Parameters.AddWithValue("@consulta", SelectedConsultation.ID);
                        cmd.Parameters.AddWithValue("@fecha", DateTime.Now);
                        cmd.Parameters.AddWithValue("@usuario", this.UserLogin.Id);
                        

                        cmd.ExecuteNonQuery();
                       
                    }
                }
            }

            //private void CargarFile()
            //{

            //    Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            //    dlg.DefaultExt = "MyPDF";
            //    dlg.DefaultExt = ".jpeg"; // Default file extension
            //    //dlg.Filter = "Text documents (.pdf)|*.pdf"; // Filter files by extension 
            //    dlg.Filter = "Image files (*.bmp, *.jpg)|*.bmp;*.jpg|All files (*.*)|*.*";

            //    // Show open file dialog box
            //    Nullable<bool> result = dlg.ShowDialog();

            //    // Process open file dialog box results 
            //    if (result == true)
            //    {
            //        // Open document 
            //        string filename = dlg.FileName;
            //        LoadFile(filename);
                    
            //    }
            //}

       
            private void LoadFile()
            {
                string Cnx = string.Empty;
                Cnx = ConfigurationManager.ConnectionStrings["ExpCSF.Properties.Settings.ExpCsfConnectionString"].ConnectionString;

                using (SqlConnection cn
                    = new SqlConnection(Cnx))
                {
                    cn.Open();
                    using (SqlCommand cmd
                        = new SqlCommand("select PDFFile from SavePDFTable  where ID='" + "1" + "' ", cn))
                    {
                        using (SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.Default))
                        {
                            if (dr.Read())
                            {

                                byte[] data = (byte[])dr.GetValue(0);
                                /*Nuevo*/
                                MemoryStream strm = new MemoryStream();
                                strm.Write(data, 0, data.Length);
                                strm.Position = 0;
                                System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                                BitmapImage bi = new BitmapImage();
                                bi.BeginInit();
                                MemoryStream ms = new MemoryStream();
                                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                                ms.Seek(0, SeekOrigin.Begin);
                                bi.StreamSource = ms;
                                bi.EndInit();
                                GeneratedImage = bi;

                                /**********/
                                //using (System.IO.FileStream fs = new System.IO.FileStream(fullName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite))
                                //{
                                //    Source = fs;
                                //    GeneratedImage = Bitmap.FromStream(fs);

                                //    using (System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs))
                                //    {
                                //        bw.Write(fileData);
                                //        bw.Close();
                                //    }
                                //}
                            }

                            dr.Close();
                        }
                    }
                }
            }


           
            #endregion
        }

    }
