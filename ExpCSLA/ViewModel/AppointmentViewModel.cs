﻿using GalaSoft.MvvmLight;
using ExpCSF.Controls;
using ExpCSF.Base;
using ExpWCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Controls.Helpers;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class AppointmentViewModel : BaseViewModel<Consultas>
    {
        #region Delegate
        public Action RefreshCustomerAppointment { get; set; }
        #endregion

        #region Fields

        #endregion

        #region Properties
        #region Window Properties
        public override string Title
        {
            get
            {
                return Resources.TitleResources.AddAppointment;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 95;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 600;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 650;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.ByPercentage;
            }
        }
        #endregion

        private Pacientes _selectedCustomer;
        public Pacientes SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                _selectedCustomer = value;
                if (this.SelectedCustomer != null)
                    this.Entity.Paciente = this.SelectedCustomer.IdPaciente;
                this.RaisePropertyChanged("SelectedCustomer");
            }
        }


        private List<medicos> _ComboMedicosItems;
        public List<medicos> ComboMedicosItems
        {
            get { return _ComboMedicosItems; }
            set
            {
                _ComboMedicosItems = value;
                this.RaisePropertyChanged("ComboMedicosItems");
            }
        }

        private List<Motivos> _ComboMotivosItems;
        public List<Motivos> ComboMotivosItems
        {
            get { return _ComboMotivosItems; }
            set
            {
                _ComboMotivosItems = value;
                this.RaisePropertyChanged("ComboMotivosItems");
            }
        }

        private medicos _selectedMedic;
        public medicos SelectedMedic
        {
            get { return _selectedMedic; }
            set
            {
                _selectedMedic = value;
                this.Entity.Medico = _selectedMedic.IDMedico;
                if (_selectedMedic.Especialidades_Medico.Count >= 1)
                {
                    this.Entity.idEspecialidad = _selectedMedic.Especialidades_Medico.FirstOrDefault().idEspecialidad;
                }
                this.RaisePropertyChanged("SelectedMedic");
            }
        }

        private Motivos _selectedMotive;
        public Motivos SelectedMotive
        {
            get { return _selectedMotive; }
            set
            {
                _selectedMotive = value;
                this.Entity.Motivo = _selectedMotive.IdMotivo;
                this.RaisePropertyChanged("SelectedMotive");

            }
        }

        private String _selectedType;
        public String SelectedType
        {
            get { return _selectedType; }
            set
            {
                _selectedType = value;
                this.Entity.SelectedCat = value;
                this.RaisePropertyChanged("SelectedType");

            }
        }

        private int _TempConsulta;
        public int TempConsulta
        {
            get { return _TempConsulta; }
            set
            {
                _TempConsulta = value;
                this.RaisePropertyChanged("TempConsulta");

            }
        }


        public TimeSpan DefaultTimeInterval
        {
            get
            {
                return new TimeSpan(0, 10, 0);
            }
        }

        public List<Consultas> AppointmentCollection
        {
            get { return _appointmentCollection; }
            set
            {
                _appointmentCollection = value;
                this.RaisePropertyChanged("AppointmentCollection");
            }
        }

        private Consultas _SelectedAppointment;
        public Consultas SelectedAppointment
        {
            get {
                if(_SelectedAppointment != null)
                    _SelectedAppointment.IsSelected = true;

                return _SelectedAppointment; 
            }
            set
            {
                _SelectedAppointment = value;
                this.RaisePropertyChanged("SelectedAppointment");
            }
        }


        public List<Recordatorios> ReminderList
        {
            get { return _reminderList ?? (_reminderList = new List<Recordatorios>()); }
            set
            {
                _reminderList = value;
                this.RaisePropertyChanged("ReminderList");
            }
        }

        public CheckItemCollection CheckItemCollection
        {
            get { return _checkItemCollection ?? (_checkItemCollection = new CheckItemCollection()); }
            set
            {
                _checkItemCollection = value;
                this.RaisePropertyChanged("CheckItemCollection");
            }
        }

        public string SelectedAppointmentTypes
        {
            get
            {
                if (this.CheckItemCollection.Any(x => x.IsChecked))
                {
                    string strConcate = string.Empty;
                    this.CheckItemCollection.ForEach(x =>
                    {
                        if (x.IsChecked)
                            strConcate += string.IsNullOrEmpty(strConcate) ? x.ItemName : ", " + x.ItemName;
                    });
                    return strConcate;
                }
                return string.Empty;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    return;

                var spiltedItem = value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (spiltedItem.Any())
                {
                    foreach (var curr in spiltedItem.Select(item => this.CheckItemCollection.FirstOrDefault(
                        x => x.ItemName.Trim().ToLower() == item.Trim().ToLower())).Where(curr => curr != null))
                    {
                        curr.IsChecked = true;
                    }
                }
            }
        }

        
        private bool _CanChangeMedic;
        public bool CanChangeMedic
        {
            get { return _CanChangeMedic; }
            set
            {
                _CanChangeMedic = value;
                this.RaisePropertyChanged("CanChangeMedic");

            }
        }
        #endregion

        #region Command Properties
        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveAppointment, CanSaveAppointment)); }
        }

        private ICommand _cancelCommand;
        private List<Consultas> _appointmentCollection;
        private List<Recordatorios> _reminderList;
        private CheckItemCollection _checkItemCollection;

        public ICommand CancelCommand
        {
            get { return this._cancelCommand ?? (this._cancelCommand = new RelayCommand(OnCancelAppointment)); }
        }

        #endregion

        private void NewSetAppointmet()
        {
            DataService _dataservice = new DataService();

            this.Entity.Fecha = DateTime.Now;
            this.Entity.Hora = DateTime.Now.ToString().Remove(0, 11);
            this.Entity.Duracion = 30;
            this.Entity.Confirmado = false;
            //this.Entity.idEspecialidad = UserLogin.medicos.Especialidades_Medico.FirstOrDefault().idEspecialidad;  
            //_dataservice.GetSpecialities().Where(x => x.VARIABLE.Equals("M6")).FirstOrDefault().ID;
            this.Entity.IdUsuario = this.UserLogin.Id;
            
        }
        private void NewSetAppointmet(DateTime _fechaDefinida)
        {
            DataService _dataservice = new DataService();
            this.Entity = new Consultas();
            this.Entity.Fecha = _fechaDefinida;
            this.Entity.Hora = _fechaDefinida.ToString().Remove(0, 11);  
            this.Entity.Duracion = 30;
            this.Entity.Confirmado = false;
            this.Entity.Medico = SelectedMedic.IDMedico;
            this.Entity.Motivo = SelectedMotive.IdMotivo;
            this.Entity.Paciente = SelectedCustomer.IdPaciente;
            //this.Entity.idEspecialidad = UserLogin.medicos.Especialidades_Medico.FirstOrDefault().idEspecialidad;
                //_dataservice.GetSpecialities().Where(x => x.VARIABLE.Equals("M6")).FirstOrDefault().ID;
            this.Entity.IdUsuario = this.UserLogin.Id;
        }

        #region Constructors
        public AppointmentViewModel(IMessenger messenger)
            : base(messenger)
        {
            NewSetAppointmet();
        }

        public AppointmentViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            NewSetAppointmet();
            this.PropertyChangedCapture();
            //this.GetAppointmentsByDate(this.Entity, true, true);

            if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("medico"))
            { 
                DataService _dataservice = new DataService();
                this.SelectedMedic = _dataservice.GetActiveMedicsById((int)(UserLogin.IdMedico));
                CanChangeMedic = true;
                ComboMedicosItems = new List<medicos>();
                ComboMedicosItems.Add(_dataservice.GetActiveMedicsById((int)(UserLogin.IdMedico)));
            }

            if (this.Entity.Medico != 0)
                this.GetAppointmentsByDateMedic(this.Entity, false);
            else
                this.GetAppointmentsByDate(this.Entity, false, true);


        }
        public AppointmentViewModel(IMessenger messenger, Usuario userLogin, Pacientes customer)
            : base(messenger, userLogin)
        {
            NewSetAppointmet();
            this.SelectedCustomer = customer;
            this.PropertyChangedCapture();
            //this.GetAppointmentsByDate(this.Entity, true, true);
            if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("medico"))
            {
                DataService _dataservice = new DataService();
                this.SelectedMedic = _dataservice.GetActiveMedicsById((int)(UserLogin.IdMedico));
                CanChangeMedic = true;
                ComboMedicosItems = new List<medicos>();
                ComboMedicosItems.Add(_dataservice.GetActiveMedicsById((int)(UserLogin.IdMedico)));
            }

            if (this.Entity.Medico != 0)
                this.GetAppointmentsByDateMedic(this.Entity, false, true);
            else
                this.GetAppointmentsByDate(this.Entity, false, true);


        }

        public AppointmentViewModel(IMessenger messenger, Usuario userLogin, Consultas appointment, Pacientes customer = null)
            : base(messenger, userLogin)
        {
            this.Entity = appointment;
            this.PropertyChangedCapture();
            this.SelectedCustomer = customer;
            this.Entity.Confirmado = false;
            //this.GetAppointmentsByDate(this.Entity, true, true);
            if (this.Entity.Medico != 0)
                this.GetAppointmentsByDateMedic(this.Entity, false, true);
            else
                this.GetAppointmentsByDate(this.Entity, false, true);

        }
        #endregion

        #region Command Methods
        private bool CanSaveAppointment()
        {
            //return this.Entity != null && this.Entity.IsSaveEnabled;
            return true;
        }

        private void OnSaveAppointment()
        {
            this.ShowProgressBar = true;
            bool NOhayPaciente = false;
            DataService _dataservice = new DataService();
            //if (!this.Entity.IsValidAppiontSechduleDateRange)
            //{
            //    var msg = new MessageDailog() { Caption = Resources.MessageResources.InvalidAppointmentDate, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Warning };
            //    MessengerInstance.Send(msg);
            //    return;
            //}
            bool returnStatus = false;
            if (this.Entity.ID == 0)
            {
                if (this.Entity.Paciente != 0)
                {
                    Consultas AppInsertar = this.Entity;
                    //AppInsertar.Hora = AppInsertar.Fecha.Value.TimeOfDay.ToString();
                    returnStatus = _dataservice.AddAppointment(AppInsertar);
                }
                else
                {
                    NOhayPaciente = true;
                    var messageDailog = new MessageDailog() { Caption = "Tiene que tener un paciente seleccionado.", DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
                    
                }
                                
            }
            else
            {
                if (TempConsulta != this.Entity.Medico)
                {
                    NewSetAppointmet((DateTime)(this.Entity.Fecha));
                    returnStatus = _dataservice.AddAppointment(this.Entity);
                }
                else
                    returnStatus = _dataservice.UpdateAppointment(this.Entity);
            }

            if (returnStatus)
            {
                this.Entity = new Consultas();
                this.PropertyChangedCapture();
                //this.GetAppointmentsByDate(this.Entity, true, true);
                if (this.Entity.Medico != 0)
                    this.GetAppointmentsByDateMedic(this.Entity, true, true);
                else
                    this.GetAppointmentsByDate(this.Entity, true, true);

                if (RefreshCustomerAppointment != null)
                    RefreshCustomerAppointment();
                this.CheckItemCollection.ForEach(x => x.IsChecked = false);
            }


            if (!NOhayPaciente)
            {
                var messageDailog2 = returnStatus ? new MessageDailog() { Caption = Resources.MessageResources.DataSavedSuccessfully, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Information } :
                    new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog2);
            }

            this.ShowProgressBar = false;
        }

        private void OnCancelAppointment()
        {
            var messageDailog = new MessageDailog((result) =>
            {
                if (result == DialogResult.Ok)
                {
                    this.Entity = new Consultas();
                    //FindNextAvailableAppointmentTime();
                    this.PropertyChangedCapture();
                    OnRefreshAppointment();
                    this.CheckItemCollection.ForEach(x => x.IsChecked = false);
                }
            }) { Caption = Resources.MessageResources.ClearMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
        }

        private bool CanDeleteAppointment()
        {
            return (this.AppointmentCollection != null && this.AppointmentCollection != null && this.AppointmentCollection.Any(x => x.IsSelected));
        }

        private void OnDeleteAppointment()
        {
            ShowProgressBar = true;
            var messageDailog = new MessageDailog(DeleteAppointment) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
        }


        private void OnRefreshAppointment()
        {
            this.ShowProgressBar = true;

            if (this.Entity.Medico != 0)
                this.GetAppointmentsByDateMedic(this.Entity, false);
            else
                this.GetAppointmentsByDate(this.Entity, false);
        }

        private bool CanEditAppointment()
        {
            return this.AppointmentCollection != null &&
                   (this.AppointmentCollection.Count(x => x.IsSelected) >= 1);
        }

        private void OnEditAppointment()
        {
            this.Entity = this.AppointmentCollection.FirstOrDefault(x => x.IsSelected);
            this.SelectedCustomer = this.Entity.Pacientes;
            this.SelectedMedic = this.Entity.medicos;
            this.SelectedMotive = this.Entity.Motivos;

             if (this.Entity.idCategoria ==1)
                 SelectedType = "Roja";
                else if (this.Entity.idCategoria ==2)
                 SelectedType = "Amarilla";
                else if (this.Entity.idCategoria ==3)
                 SelectedType = "Verde";
                else
                 SelectedType = "Verde";

             //SelectedType = this.Entity.SelectedCat;

            TempConsulta = this.Entity.Medico;
            //char solo 1era linea
            //if (this.Entity != null) this.SelectedAppointmentTypes = this.Entity.AppointmentType;
            //this.CheckTimeOverLap(this.AppointmentCollection, this.Entity);
            this.PropertyChangedCapture();

        }
        #endregion

        #region Override Methods
        public override void HandleViewModeChanges(dynamic data)
        {
            //base.HandleViewModeChanges(data);
            if (this.ParentViewModel is MainViewModel)
            {
                ((MainViewModel)this.ParentViewModel).IsAppointmentTabSelected = true;
            }

        }

        public override void Initialize()
        {
            base.Initialize();

            if (this.Entity.Fecha == null)
                this.Entity.Fecha = DateTime.Now;

            DataService _dataservice = new DataService();

            DeleteCommand = new RelayCommand(OnDeleteItem, CanDeleteItem);
            EditCommand = new RelayCommand(OnEditItem, CanEditItem);
            RefreshCommand = new RelayCommand(OnRefreshItem);
            GetReminders((DateTime)this.Entity.Fecha);
            AddInfoCommand = new RelayCommand(OnAddInfo, CanAddInfo);
            EditPatientCommand = new RelayCommand(OnEditPatient, CanEditPatient);

            var appointmentTypes = Enum.GetValues(typeof(AppointmentType));
            foreach (var appointmentType in appointmentTypes)
            {
                this.CheckItemCollection.Add(new CheckItem() { ItemName = appointmentType.ToString() });
            }

            //'Carga Medicos Activos
          
            ComboMedicosItems = _dataservice.GetActiveMedics();

            //'Carga Motivos de Citas
            ComboMotivosItems = _dataservice.GetActiveMotives();


            foreach (var item in this.CheckItemCollection)
            {
                item.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "IsChecked")
                    {
                        this.RaisePropertyChanged("SelectedAppointmentTypes");
                        //this.Entity.Motivo = int.Parse(this.SelectedAppointmentTypes);
                        this.Entity.Tipo = this.SelectedAppointmentTypes;
                    }
                };
            }
        }
        #endregion

        #region Override Command Methods
        public override void OnDeleteItem()
        {
            this.OnDeleteAppointment();
        }
        public override bool CanDeleteItem()
        {
            return this.CanDeleteAppointment();
        }
        public override void OnEditItem()
        {
            this.OnEditAppointment();
        }
        public override bool CanEditItem()
        {
            return this.CanEditAppointment();
        }
        public override void OnRefreshItem()
        {
            this.OnRefreshAppointment();
        }
        #endregion

        #region Public Methods

        #endregion

        #region Private Methods
        private void        DeleteAppointment(DialogResult dialogResult)
        {               
            if (dialogResult == DialogResult.Ok)
            {
                DataService _dataservice = new DataService();
                Task.Factory.StartNew(() =>
                {
                    var result = _dataservice.DeleteAppointments(this.AppointmentCollection.Where(x => x.IsSelected));
                    if (RefreshCustomerAppointment != null)
                        RefreshCustomerAppointment();

                    if (result)
                    {
                        if (this.Entity.Medico != 0)
                            this.GetAppointmentsByDateMedic(this.Entity, true);
                        else
                            this.GetAppointmentsByDate(this.Entity, true);
                        var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DeletedSuccessfully, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Information };
                        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
                    }
                    else
                    {
                        var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DeletionFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
                    }
                    ShowProgressBar = false;
                });
            }
            else
            {
                ShowProgressBar = false;
            }
        }
        private void PropertyChangedCapture()
        {
            try
            {
                this.Entity.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "Fecha")
                    {
                        Task.Factory.StartNew(() =>
                        {
                            var item = (Consultas)sender;

                            this.Entity.Hora = this.Entity.Fecha.ToString().Remove(0, 11);

                            if (this.Entity.Medico != 0)
                                this.GetAppointmentsByDateMedic(this.Entity, true);
                            else
                                this.GetAppointmentsByDate(this.Entity, true);

                            this.GetReminders((DateTime)item.Fecha);

                        });
                    }else if (args.PropertyName == "Medico")
                    {
                        if (this.Entity.Medico != 0)
                            this.GetAppointmentsByDateMedic(this.Entity, true);
                        else
                            this.GetAppointmentsByDate(this.Entity, true);
                    }
                    
                };
            }
            catch (Exception )
            {

            }
        }



        private void GetAppointmentsByDateMedic(Consultas appointment, bool doAsync = false, bool findNextAppointment = false)
        {
            try
            {
                DataService _dataservice = new DataService();
                if (appointment.Fecha == null)
                    appointment.Fecha = DateTime.Now;

                if (doAsync)
                    Task.Factory.StartNew(() =>
                    {
                        this.AppointmentCollection = _dataservice.GetAppointmentsMedic((DateTime)appointment.Fecha, appointment.Medico);

                        foreach (var item in AppointmentCollection)
                        {
                            if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("digitador"))
                                item.EsDigitador = true;
                            else
                                item.EsDigitador = false;

                        }

                        this.ShowProgressBar = false;
                    });
                else
                {
                    this.AppointmentCollection = _dataservice.GetAppointmentsMedic((DateTime)appointment.Fecha, appointment.Medico);
                    foreach (var item in AppointmentCollection)
                    {
                        if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("digitador"))
                            item.EsDigitador = true;
                        else
                            item.EsDigitador = false;

                    }
                    this.ShowProgressBar = false;
                }
            }
            catch (Exception )
            {

            }
        }

        private void GetAppointmentsByDate(Consultas appointment, bool doAsync = false, bool findNextAppointment = false)
        {
            try
            {


                DataService _dataservice = new DataService();
                if (appointment.Fecha == null)
                    appointment.Fecha = DateTime.Now;

                if (doAsync)
                    Task.Factory.StartNew(() =>
                    {

                        this.AppointmentCollection = _dataservice.GetAppointments((DateTime)appointment.Fecha);
                        foreach (var item in AppointmentCollection)
                        {
                            if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("digitador"))
                                item.EsDigitador = true;
                            else
                                item.EsDigitador = false;

                        }

                        this.ShowProgressBar = false;
                    });
                else
                {

                    this.AppointmentCollection = _dataservice.GetAppointments((DateTime)appointment.Fecha);
                    foreach (var item in AppointmentCollection)
                    {
                        if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("digitador"))
                            item.EsDigitador = true;
                        else
                            item.EsDigitador = false;

                    }
                    this.ShowProgressBar = false;
                }
            }
            catch (Exception )
            {

            }
        }



        private void GetReminders(DateTime dateTime)
        {
            try
            {
                DataService _dataservice = new DataService();
                Task.Factory.StartNew(() =>
                {
                    this.ReminderList = _dataservice.GetReminders("", dateTime);


                });
            }
            catch (Exception )
            {

            }
        }
        #endregion

        public ICommand AddInfoCommand { get; set; }
        public virtual bool CanAddInfo()
        {
            return true;
        }

        public void OnAddInfo()
        {
            try
            {
                this.ParentViewModel.ContentViewModel = null;
                this.ParentViewModel.ContentViewModel = new DetailPatientInfoViewModel(this.Messenger, this.UserLogin, this.SelectedAppointment) { ParentViewModel = this.ParentViewModel.ParentViewModel };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ParentViewModel.ContentViewModel);

              }
            catch (Exception )
            {

            }
        }

        public ICommand EditPatientCommand { get; set; }
        public virtual bool CanEditPatient()
        {
            return true;
        }
        private void OnEditPatient()
        {
            //ShowProgressBar = true;
            //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            //MessengerInstance.Send(messageDailog);
            try
            {

                var childVM = new AddPatientViewModel(this.Messenger, this.UserLogin, this.SelectedAppointment.Pacientes) { ParentViewModel = this };
                //childVM.RefreshCustomers += this.GetCustomerCollection;
                var messageDailog = new ExpCSF.Controls.VMMessageDailog() { ChildViewModel = childVM };
                ShowProgressBar = true;
                //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
                //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = "Esta seguro que desea eliminar esto ?", DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

                //MessengerInstance.Send(messageDailog);
            }
            catch (Exception exception)
            {
                var a = exception;
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured,
                //                    ExceptionResources.ExceptionOccuredLogDetail);
            }
        }

    }
}