﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.ComponentModel;
using ExpCSF.Resources;
using System.Windows.Media.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace ExpCSF.ViewModel
{
    class AttachDocumentViewModel : BaseViewModel<SavePDFTable>
    {
        private DataService _dataservice = new DataService();

        private SavePDFTable _selectedMenuItem;
        public SavePDFTable SelectedMenuItem
        {
            get
            {
                return _selectedMenuItem;

            }
            set
            {
                _selectedMenuItem = value;
                this.RaisePropertyChanged("SelectedMenuItem");
                OnSelectedMenuItem();
            }
        }


        private BitmapImage _generatedImage;

        public BitmapImage GeneratedImage
        {
            get { return _generatedImage; }
            set
            {
                if (value == _generatedImage) return;
                _generatedImage = value;
                RaisePropertyChanged("GeneratedImage");
            }
        }

        private List<SavePDFTable> _MenuList;
        public List<SavePDFTable> MenuList
        {
            get
            {
                List<SavePDFTable> itemsMenu = new List<SavePDFTable>();

                itemsMenu = _dataservice.GetSAvedImages(SelectedConsultation.ID);

                return itemsMenu;
            }
            set
            {
                _MenuList = value;
                this.RaisePropertyChanged("MenuList");
            }

        }

        private Consultas _selectedConsultation;
        public Consultas SelectedConsultation
        {
            get { return _selectedConsultation; }
            set
            {
                _selectedConsultation = value;
                this.RaisePropertyChanged("SelectedConsultation");
            }
        }

        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private SavePDFTable _DiagnosticosConsulta;
        public SavePDFTable DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.Entity = value;
                IsInEditMode = true;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }

        #region Constructors
        public AttachDocumentViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {

            AgregarDatosConsulta();
        }

        public AttachDocumentViewModel(IMessenger messenger, Usuario userLogin, SavePDFTable patient)
            : base(messenger, userLogin)
        {

            this.Entity = patient;
            AgregarDatosConsulta();

        }

        public AttachDocumentViewModel(IMessenger messenger, Usuario userlogin, Consultas consulta)
            : base(messenger, userlogin)
        {
            this.SelectedConsultation = consulta;


        }

        #endregion

        #region Virtual Methods
        public override void Initialize()
        {
            base.Initialize();
            DeleteCommand = new RelayCommand(OnDeleteItem, CanDeleteItem);
            //this.SignInCommand = new RelayCommand(OnDataProcess, CanExecuteSingInCommand);
        }

        public override void OnDeleteItem()
        {
            try
            {
                _dataservice.DeleteSavedimage(this.Entity);
                var a = MenuList;
                IsInEditMode = false;

            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;

            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;


            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion



        private void OnSaveCustomer()
        {
            AdjuntarFile();

            //var returnStatus = false;
            ////char
            ////Views.ViewerReporte a = new Views.ViewerReporte(2);

            //returnStatus = this.Entity.ID == 0 ? _dataservice.AddImage(this.Entity) : _dataservice.UpdateImage();

            //if (returnStatus)
            //{

            //    var messageDailog = new MessageDailog()
            //    {
            //        Caption = Resources.MessageResources.DataSavedSuccessfully,
            //        DialogButton = DialogButton.Ok,
            //        Title = Resources.TitleResources.Information
            //    };

            //    //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            //    MessengerInstance.Send(messageDailog);

            //    if (this.CloseWindow != null)
            //        this.CloseWindow();
            //}
            //else
            //{
            //    var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
            //    //Messenger.Default.Send(messageDailog);
            //    MessengerInstance.Send(messageDailog);
            //}
        }

        private void AdjuntarFile()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".jpeg"; // Default file extension
            dlg.Filter = "Image files (*.bmp, *.jpg)|*.bmp;*.jpg|All files (*.*)|*.*"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;

                UploadFile(filename, dlg.SafeFileName);
            }
        }

        private void UploadFile(string fullName, string titulo)
        {
            string Cnx = string.Empty;
            Cnx = ConfigurationManager.ConnectionStrings["ExpCSF.Properties.Settings.ExpCsfConnectionString"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(Cnx))
            {
                cn.Open();
                FileStream fStream = File.OpenRead(fullName);
                byte[] contents = new byte[fStream.Length];
                fStream.Read(contents, 0, (int)fStream.Length);
                fStream.Close();
                using (SqlCommand cmd = new SqlCommand("insert into SavePDFTable " + "(PDFFile,idConsulta,FechaRegistro,UsuarioId,Descripcion)values(@data,@consulta,@fecha,@usuario,@descripcion)", cn))
                {
                    //cmd.Parameters.Add("@data", contents);
                    cmd.Parameters.AddWithValue("@data", contents);
                    cmd.Parameters.AddWithValue("@consulta", SelectedConsultation.ID);
                    cmd.Parameters.AddWithValue("@fecha", DateTime.Now);
                    cmd.Parameters.AddWithValue("@usuario", this.UserLogin.Id);
                    cmd.Parameters.AddWithValue("@descripcion", titulo);
                    
                    cmd.ExecuteNonQuery();
                    this.RaisePropertyChanged("MenuList");

                    var messageDailog = new MessageDailog()
                {
                    Caption = Resources.MessageResources.DataSavedSuccessfully,
                    DialogButton = DialogButton.Ok,
                    Title = Resources.TitleResources.Information
                };

                    MessengerInstance.Send(messageDailog);

                  
                    //_MenuList = _dataservice.GetSAvedImages(SelectedConsultation.ID);

                }
            }
        }

        private void LoadFile()
        {
            if (SelectedMenuItem != null) {
                byte[] data = SelectedMenuItem.PDFFile;
                /*Nuevo*/
                MemoryStream strm = new MemoryStream();
                strm.Write(data, 0, data.Length);
                strm.Position = 0;
                System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                ms.Seek(0, SeekOrigin.Begin);
                bi.StreamSource = ms;
                bi.EndInit();
                GeneratedImage = bi;
            }
          
        }

        private void AgregarDatosConsulta()
        {
            try
            {
                this.DiagnosticosConsulta = null;

                if (this.DiagnosticosConsulta == null)
                    this.DiagnosticosConsulta = new SavePDFTable() { FechaRegistro = DateTime.Now, idConsulta = SelectedConsultation.ID, UsuarioId = UserLogin.Id };

                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void OnSelectedMenuItem()
        {
            LoadFile();
            
            return;


        }
    }
}
