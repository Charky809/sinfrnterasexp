﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExpCSF.Base;
using ExpWCF;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Controls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using GalaSoft.MvvmLight.Command;
using System.Data;
using ExpCSF.Resources;

namespace ExpCSF.ViewModel
{
    class DetailPatientInfoViewModel : BaseViewModel
    {
        #region Fields
        private Pacientes _selectedCustomer;
        private string _selectedMenuItem;
        private IBaseViewModel _detailSectionViewModel;
        private Consultas _selectedConsultation;
        private Especialidades _SelectedSpeciality;
        //private RelayCommand<object> _selectedMenuItem;

        #endregion

        #region Properties

        public Especialidades SelectedSpeciality
        {
            get { return _SelectedSpeciality; }
            set
            {
                _SelectedSpeciality = value;

                if (DetailSectionViewModel.GetType().Name == "HistPreConsultationViewModel")
                {
                    if (HPreConsultationViewModel != null)
                        this.HPreConsultationViewModel.RefreshConsultSpeciality(_SelectedSpeciality);
                }
                else if (DetailSectionViewModel.GetType().Name == "HistConsultationAntViewModel")
                {
                    if (HConsultationAntViewModel != null)
                        this.HConsultationAntViewModel.RefreshConsultSpeciality(_SelectedSpeciality);

                }
                else if (DetailSectionViewModel.GetType().Name == "HistMedicNoteViewModel")
                {
                    if (HMedicNoteViewModel != null)
                        this.HMedicNoteViewModel.RefreshConsultSpeciality(_SelectedSpeciality);

                }
                else if (DetailSectionViewModel.GetType().Name == "HistConsultationEXFXViewModel")
                {
                    if (HConsultationEXFXViewModel != null)
                        this.HConsultationEXFXViewModel.RefreshConsultSpeciality(_SelectedSpeciality);

                }

               

                this.RaisePropertyChanged("SelectedSpeciality");

            }
        }

        private List<Especialidades> _SpecialitiesList;
        public List<Especialidades> SpecialitiesList
        {
            get { return _SpecialitiesList; }
            set
            {
                _SpecialitiesList = value;
                this.RaisePropertyChanged("SpecialitiesList");
            }
        }


        private bool _isHistorico;
        public bool isHistorico
        {
            get
            {
                return _isHistorico;
            }
            set
            {
                _isHistorico = value;
                this.RaisePropertyChanged("isHistorico");
                if (_isHistorico)
                {
                    if (UserLogin.medicos != null)
                    {
                        int IdEspec = UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades.ID;
                        SelectedSpeciality = SpecialitiesList.Where(u => u.ID == IdEspec).Single();
                    }

                    MenuList = new List<string>();
                        //{
                        //    MenuResources.Appointments,
                        //    MenuResources.Detail,
                        //    MenuResources.PreConsultation,
                        //    MenuResources.Diagnosticos,
                        //    MenuResources.Problemas,
                        //    MenuResources.NotaMedica,
                        //    MenuResources.Antecedentes,
                        //    MenuResources.ExFisico,
                        //    MenuResources.Papanicolau,
                        //    MenuResources.ReportesUS,
                        //};

                    this.RaisePropertyChanged("isHistorico");
                }
                else
                {
                    MenuList = new List<string>();
                
                }
            }
        }

        public Pacientes SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                _selectedCustomer = value;
                this.RaisePropertyChanged("SelectedCustomer");
            }
        }

        public Consultas SelectedConsultation
        {
            get { return _selectedConsultation; }
            set
            {
                _selectedConsultation = value;
                this.RaisePropertyChanged("SelectedConsultation");

            }
        }


        List<string> _MenuList = new List<string>();
        public List<string> MenuList
        {
            get
            {
                List<string> itemsMenu = new List<string>();
                if(isHistorico)
                {
                    _MenuList = new List<string>()
                        {
                            MenuResources.Detail,
                            MenuResources.PreConsultation,
                            MenuResources.Diagnosticos,
                            MenuResources.Problemas,
                            MenuResources.NotaMedica,
                            MenuResources.Antecedentes,
                            MenuResources.ExFisico,
                            MenuResources.Papanicolau,
                            MenuResources.Formularios,
                            MenuResources.ReportesUS,
                        };
                
                }
                else if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("enfermero"))
                {
                    _MenuList = new List<string>()
                    {
                        
                        MenuResources.Appointments,
                        MenuResources.Detail,
                        MenuResources.PreConsultation,
                        MenuResources.Vacunas,
                        MenuResources.Adjuntar,
                        //MenuResources.Farmacia,
                        
                        //MenuResources.Repairs,
                        //MenuResources.Appointments,
                        //MenuResources.WarrantyInformed,
                    };
                }
                else if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("medico"))
                {
                    if (UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades.DESCRIPCION.ToLower().Equals("imagenes"))
                    {
                        _MenuList = new List<string>()
                        {
                            MenuResources.Appointments,
                            MenuResources.Detail,
                            MenuResources.NotaMedica,
                            MenuResources.ReportesUS,
                    
                        };
                    }
                    else if (UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades.DESCRIPCION.ToLower().Equals("general"))
                    {
                        _MenuList = new List<string>()
                        {
                            MenuResources.Appointments,
                            MenuResources.Detail,
                            MenuResources.PreConsultation,
                            MenuResources.Diagnosticos,
                            MenuResources.Problemas,
                            MenuResources.NotaMedica,
                            MenuResources.Antecedentes,
                            MenuResources.ExFisico,
                            MenuResources.Papanicolau,
                            MenuResources.Adjuntar,
                        };
                    }
                    else
                    {
                        _MenuList = new List<string>()
                        {
                            MenuResources.Appointments,
                            MenuResources.Detail,
                            MenuResources.PreConsultation,
                            MenuResources.Diagnosticos,
                            MenuResources.Problemas,
                            MenuResources.NotaMedica,
                            MenuResources.Antecedentes,
                            MenuResources.ExFisico,
                            MenuResources.Formularios,
                            MenuResources.Adjuntar,
                           
                        };
                    }
                }
                else if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("digitador"))
                {
                    _MenuList = new List<string>()
                    {
                        MenuResources.Appointments,
                        MenuResources.Detail,
                        MenuResources.PreConsultation,
                        MenuResources.Diagnosticos,
                        MenuResources.Problemas,
                        MenuResources.NotaMedica,
                        MenuResources.Antecedentes,
                        MenuResources.ExFisico,
                        MenuResources.Papanicolau,
                        MenuResources.Formularios,
                        MenuResources.Adjuntar,
                    };
                }
                else
                {
                    _MenuList = new List<string>()
                    {
                        MenuResources.Appointments,
                        MenuResources.Adjuntar,
                       
                    };
                }

                return _MenuList;
            }
            set {
                _MenuList = value;
                this.RaisePropertyChanged("MenuList");
                
            }
        }

        public string SelectedMenuItem
        {
            get
            {
                if (_selectedMenuItem == null)
                {
                    if (this.SelectedCustomer == null)
                    {
                        SetAppointmentsList();
                    }
                    else
                    {
                        //SetSummaryView();
                        SelectedMenuItem = MenuResources.Detail;
                    }
                }

                return _selectedMenuItem;
            }
            set
            {
                _selectedMenuItem = value;
                this.RaisePropertyChanged("SelectedMenuItem");
                OnSelectedMenuItem();
            }
        }

        public PreConsultationViewModel CustomerRepairViewModel { get; set; }

        public ListaViewModel CustomerAppointmentViewModel { get; set; }

        public PreConsultationViewModel CustomerWarrantyInformedViewModel { get; set; }

        public PreConsultationViewModel CustomerOrderViewModel { get; set; }

        public PatientSummaryViewModel CustomerSummaryViewModel { get; set; }

        public AddConsultationDXViewModel ConsultationDxViewModel { get; set; }

        public AddConsultationVacunasViewModel ConsultationVacunasViewModel { get; set; }

        public AddConsultationProblemViewModel ConsultationProblemViewModel { get; set; }

        public AddConsultationAntViewModel ConsultationAntViewModel { get; set; }

        public HistConsultationAntViewModel HConsultationAntViewModel { get; set; }

        public HistConsultationEXFXViewModel HConsultationEXFXViewModel { get; set; }

        public HistConsultationDXViewModel HConsultationDXViewModel { get; set; }

        public HistConsultationProblemViewModel HConsultationProblemViewModel { get; set; }

        public AddMedicNoteViewModel MedicNoteViewModel { get; set; }

        public PreConsultationViewModel PreconsultationViewModel { get; set; }

        public AddConsultationEXFXViewModel ConsultationExFxViewModel { get; set; }

        public HistPreConsultationViewModel HPreConsultationViewModel { get; set; }

        public HistMedicNoteViewModel HMedicNoteViewModel { get; set; }

        public HistImagesReportsViewModel HImagesReportsViewModel { get; set; }       

        public AddImageReportViewModel AddImageReportViewModel { get; set; }

        public AddConsultationPAPViewModel AddConsultationPAPViewModel { get; set; }

        public HistConsultationPAPViewModel HConsultationPAPViewModel { get; set; }

        public AttachDocumentViewModel AttachDocumentViewModel { get; set; }

        

        public IBaseViewModel DetailSectionViewModel
        {
            get { return _detailSectionViewModel; }
            set
            {
                _detailSectionViewModel = value;
                this.RaisePropertyChanged("DetailSectionViewModel");
            }
        }

        #endregion

        #region Command Properties

        //public RelayCommand<object> SelectedMenuItem
        //{
        //    get { return _selectedMenuItem; }
        //    set
        //    {
        //        _selectedMenuItem = value;
        //        this.RaisePropertyChanged(() => SelectedMenuItem);
        //    }
        //}

        #endregion

        #region Constructors

        DataService _dataservice = new DataService();

        public DetailPatientInfoViewModel(IMessenger messenger)
            : base(messenger)
        {
            GetSpecialities();
        }

        public DetailPatientInfoViewModel(IMessenger messenger, Usuario userLogin, Pacientes selectedCustomer)
            : base(messenger, userLogin)
        {
            SelectedCustomer = selectedCustomer;

            GetSpecialities();
        }

        public DetailPatientInfoViewModel(IMessenger messenger, Usuario userLogin, Consultas selectedConsultation)
            : base(messenger, userLogin)
        {
            SelectedConsultation = selectedConsultation;
            SelectedCustomer = selectedConsultation.Pacientes;
            GetSpecialities();

        }

        public DetailPatientInfoViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            GetSpecialities();

        }

        #endregion

        private void GetSpecialities()
        {
            SpecialitiesList = _dataservice.GetSpecialities().OrderBy(u => u.DESCRIPCION).ToList();
            SpecialitiesList.Add(new Especialidades { ID = 1000, DESCRIPCION = "TODAS", ESTADO = true, VARIABLE = "TDO", BORRADO = false });
        }
        #region Override Methods
        public override void Initialize()
        {
            base.Initialize();
            //SelectedMenuItem = new RelayCommand<object>(OnSelectedMenuItem);
        }

        public override void Unload()
        {
            base.Unload();
            SelectedMenuItem = null;
        }
        #endregion

        #region Command Methods

        #endregion

        #region Private Methods
        private void OnSelectedMenuItem()
        {

            if (SelectedMenuItem == MenuResources.Detail)
            {
                SetSummaryView();
                return;
            }
            else if (SelectedMenuItem == MenuResources.Appointments)
            {
                DetailSectionViewModel = null;

                if (CustomerAppointmentViewModel == null)
                    CustomerAppointmentViewModel = new ListaViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };

                DetailSectionViewModel = CustomerAppointmentViewModel;
                return;
            }
            else if (SelectedMenuItem == MenuResources.PreConsultation)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    if (PreconsultationViewModel == null)
                        PreconsultationViewModel = new PreConsultationViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };

                    DetailSectionViewModel = PreconsultationViewModel;
                }
                else
                {
                    HPreConsultationViewModel = new HistPreConsultationViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer, this.SelectedSpeciality) { ParentViewModel = this };
                    DetailSectionViewModel = HPreConsultationViewModel;
                }

                return;
            }
            else if (SelectedMenuItem == MenuResources.Diagnosticos)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    if (ConsultationDxViewModel == null)
                        ConsultationDxViewModel = new AddConsultationDXViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };
                    DetailSectionViewModel = ConsultationDxViewModel;
                }
                else
                {
                    HConsultationDXViewModel = new HistConsultationDXViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };
                    DetailSectionViewModel = HConsultationDXViewModel;
                }

                return;
            }
            else if (SelectedMenuItem == MenuResources.Vacunas)
            {
                DetailSectionViewModel = null;

                if (ConsultationVacunasViewModel == null)
                    ConsultationVacunasViewModel = new AddConsultationVacunasViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };


                DetailSectionViewModel = ConsultationVacunasViewModel;
                return;
            }
            else if (SelectedMenuItem == MenuResources.Antecedentes)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    //if (ConsultationAntViewModel == null)
                    ConsultationAntViewModel = new AddConsultationAntViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };
                    DetailSectionViewModel = ConsultationAntViewModel;
                }
                else
                {
                    if (SelectedSpeciality != null)
                        HConsultationAntViewModel = new HistConsultationAntViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer, this.SelectedSpeciality) { ParentViewModel = this };
                    else
                        HConsultationAntViewModel = new HistConsultationAntViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };
                    DetailSectionViewModel = HConsultationAntViewModel;
                }

                return;
            }
            else if (SelectedMenuItem == MenuResources.ExFisico)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    //if (ConsultationExFxViewModel == null)
                    ConsultationExFxViewModel = new AddConsultationEXFXViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };
                    DetailSectionViewModel = ConsultationExFxViewModel;
                }
                else
                {
                    HConsultationEXFXViewModel = new HistConsultationEXFXViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };
                    DetailSectionViewModel = HConsultationEXFXViewModel;
                }

                return;
            }
            else if (SelectedMenuItem == MenuResources.Problemas)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    //if (ConsultationProblemViewModel == null)
                    ConsultationProblemViewModel = new AddConsultationProblemViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };
                    DetailSectionViewModel = ConsultationProblemViewModel;
                }
                else
                {
                    HConsultationProblemViewModel = new HistConsultationProblemViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };
                    DetailSectionViewModel = HConsultationProblemViewModel;
                }

                return;

            }
            else if (SelectedMenuItem == MenuResources.NotaMedica)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    if (MedicNoteViewModel == null)
                        MedicNoteViewModel = new AddMedicNoteViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };

                    DetailSectionViewModel = MedicNoteViewModel;
                }
                else
                {
                    HMedicNoteViewModel = new HistMedicNoteViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer, this.SelectedSpeciality) { ParentViewModel = this };
                    DetailSectionViewModel = HMedicNoteViewModel;
                }              

                return;
            }
            else if (SelectedMenuItem == MenuResources.ReportesUS)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    //if (AddImageReportViewModel == null)
                    AddImageReportViewModel = new AddImageReportViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };
                    DetailSectionViewModel = AddImageReportViewModel;
                }
                else
                {
                    HImagesReportsViewModel = new HistImagesReportsViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };
                    DetailSectionViewModel = HImagesReportsViewModel;
                }

                
                return;
            }
            else if (SelectedMenuItem == MenuResources.Papanicolau)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    //if (AddImageReportViewModel == null)
                    AddConsultationPAPViewModel = new AddConsultationPAPViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };
                    DetailSectionViewModel = AddConsultationPAPViewModel;
                }
                else
                {
                    HConsultationPAPViewModel = new HistConsultationPAPViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };
                    DetailSectionViewModel = HConsultationPAPViewModel;
                }


                return;
            }
            else if (SelectedMenuItem == MenuResources.Adjuntar)
            {
                DetailSectionViewModel = null;

                if (!isHistorico)
                {
                    //if (AddImageReportViewModel == null)
                    AttachDocumentViewModel = new AttachDocumentViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };
                    DetailSectionViewModel = AttachDocumentViewModel;
                }
                else
                {
                    //HConsultationPAPViewModel = new HistConsultationPAPViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };
                    DetailSectionViewModel = null;
                }


                return;
            }


        }

        private void SetSummaryView()
        {
            DetailSectionViewModel = null;

            if (CustomerSummaryViewModel == null)
            {
                if(this.SelectedConsultation != null)
                CustomerSummaryViewModel = new PatientSummaryViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = this };
                else
                    CustomerSummaryViewModel = new PatientSummaryViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };

            }

            DetailSectionViewModel = CustomerSummaryViewModel;
            //this.SelectedMenuItem = MenuResources.Detail;
        }

        private void SetAppointmentsList()
        {
            DetailSectionViewModel = null;

            if (CustomerAppointmentViewModel == null)
                CustomerAppointmentViewModel = new ListaViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };

            DetailSectionViewModel = CustomerAppointmentViewModel;

        }
        #endregion

    }
}
