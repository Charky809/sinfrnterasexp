﻿using GalaSoft.MvvmLight;
using ExpCSF.Controls;
using ExpCSF.Base;
using ExpWCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Resources;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class DiagnosticosViewModel : BaseViewModel<Consultas>
    {
       #region Fields

        #endregion

        #region Properties

        protected Diagnosticos SelectedDiag { get; set; }

        private List<Diagnosticos_Cita> _diagnosticosCollection;
        public List<Diagnosticos_Cita> DiagnosticosCollection
        {
            get { return _diagnosticosCollection; }
            set
            {
                _diagnosticosCollection = value;
                this.RaisePropertyChanged("DiagnosticosCollection");
            }
        }

        #endregion

        #region Command Properties
        public ICommand AddHearingAIDCommand { get; set; }
        public ICommand DeleteHearingAIDCommand { get; set; }
        public ICommand EditHearingAIDCommand { get; set; }
        public ICommand RefreshHearingAIDCommand { get; set; }
        private ICommand _printHearingAidReceiptCommand;
        public ICommand PrintHearingAidReceiptCommand
        {
            get { return this._printHearingAidReceiptCommand ?? (this._printHearingAidReceiptCommand = new RelayCommand(OnPrintHearingAidReceipt, CanPrintHearingAidReceipt)); }
        }

        public ICommand AddEarMoldCommand { get; set; }
        public ICommand DeleteEarMoldCommand { get; set; }
        public ICommand EditEarMoldCommand { get; set; }
        public ICommand RefreshEarMoldCommand { get; set; }

        /*
        private ICommand _printEarMoldReceiptCommand;
        public ICommand PrintEarMoldReceiptCommand
        {
            get { return this._printEarMoldReceiptCommand ?? (this._printEarMoldReceiptCommand = new RelayCommand(OnPrintEarMoldReceipt, CanPrintEarMoldReceipt)); }
        }*/

        #endregion

        #region Constructors
        public DiagnosticosViewModel(IMessenger messenger, Usuario userLogin, Consultas customer)
            : base(messenger, userLogin)
        {
            Entity = customer; 

            //char
           //if(this.Entity  != null && this.Entity.CustomerEarMoldOrderCollection == null)
           //    this.RefreshCustomerEarMoldOrderCollection(false);

           if (this.Entity != null && DiagnosticosCollection == null)
               this.RefreshCustomerHearingAidOrderCollection(false);
        }

        #endregion

        #region Override Methods
        public override void Initialize()
        {
            base.Initialize();
            AddEarMoldCommand = new RelayCommand(OnAddItem);
            DeleteEarMoldCommand = new RelayCommand(OnDeleteItem, CanDeleteItem);
            EditEarMoldCommand = new RelayCommand(OnEditItem, CanEditItem);
            RefreshEarMoldCommand = new RelayCommand(OnRefreshItem);


            AddHearingAIDCommand = new RelayCommand(OnAddHearingAIDItem);
            DeleteHearingAIDCommand = new RelayCommand(OnDeleteHearingAIDItem, CanDeleteHearingAIDItem);
            EditHearingAIDCommand = new RelayCommand(OnEditHearingAIDItem, CanEditHearingAIDItem);
            RefreshHearingAIDCommand = new RelayCommand(OnRefreshHearingAIDItem);
        }
        #endregion

        #region Command Methods
       /* public override void OnAddItem()
        {
            try
            {
                var childVM = new AddCustomerEarMoldOrderViewModel(this.Messenger, this.UserLogin, this.Entity) { ParentViewModel = this };
                childVM.RefreshCustomerEarMoldOrder += this.RefreshCustomerEarMoldOrderCollection;
                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                MessengerInstance.Send(messageDailog);

            }
            catch (Exception exception)
            {
                
            }
        }*/

        //char
        //public override void OnDeleteItem()
        //{
        //    //this.ParentViewModel.ShowProgressBar = true;
        //    var messageDailog = new MessageDailog(DeleteCustomerEarMoldOrder) { Caption = MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = TitleResources.Warning };
        //    MessengerInstance.Send(messageDailog);
        //}
        //public override bool CanDeleteItem()
        //{
        //    return (this.Entity != null && this.Entity.CustomerEarMoldOrderCollection != null && this.Entity.CustomerEarMoldOrderCollection.Any(x => x.IsSelected));
        //}

        //public override void OnEditItem()
        //{
        //    var childVM = new AddCustomerEarMoldOrderViewModel(this.Messenger, this.UserLogin, this.Entity.CustomerEarMoldOrderCollection.FirstOrDefault(x => x.IsSelected)) { ParentViewModel = this };
        //    childVM.RefreshCustomerEarMoldOrder += this.RefreshCustomerEarMoldOrderCollection;
        //    var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
        //    MessengerInstance.Send(messageDailog);
        //}
        //public override bool CanEditItem()
        //{
        //    return (this.Entity != null && this.Entity.CustomerEarMoldOrderCollection != null && this.Entity.CustomerEarMoldOrderCollection.Count(x => x.IsSelected) == 1);
        //}

        //public override void OnRefreshItem()
        //{
        //    this.RefreshCustomerEarMoldOrderCollection();
        //}
        /*******************************/

        public void OnAddHearingAIDItem()
        {
            try
            {
                var childVM = new AddConsultationDXViewModel(this.Messenger, this.UserLogin, this.Entity) { ParentViewModel = this };
                //Delegate de evento messenger
                childVM.RefreshConsultationDX += this.RefreshCustomerHearingAidOrderCollection;
                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                MessengerInstance.Send(messageDailog);

            }
            catch (Exception )
            {
              
            }
        }

        public void OnDeleteHearingAIDItem()
        {
            //this.ParentViewModel.ShowProgressBar = true;
            var messageDailog = new MessageDailog(DeleteCustomerHearingAidOrder) { Caption = MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = TitleResources.Warning };
            MessengerInstance.Send(messageDailog);
        }
        public bool CanDeleteHearingAIDItem()
        {
            return (this.Entity != null && DiagnosticosCollection != null && DiagnosticosCollection.Any(x => x.IsSelected));
        }

        public  void OnEditHearingAIDItem()
        {
            var childVM = new AddConsultationDXViewModel(this.Messenger, this.UserLogin, this.DiagnosticosCollection) { ParentViewModel = this };
            childVM.RefreshConsultationDX += this.RefreshCustomerHearingAidOrderCollection;
            var messageDailog = new ExpCSF.Controls.VMMessageDailog() { ChildViewModel = childVM };
            MessengerInstance.Send(messageDailog);
        }
        public bool CanEditHearingAIDItem()
        {
            return (this.Entity != null && this.DiagnosticosCollection != null && this.DiagnosticosCollection.Count(x => x.IsSelected) == 1);
        }

        public  void OnRefreshHearingAIDItem()
        {
            this.RefreshCustomerHearingAidOrderCollection();
        }

        private bool CanPrintHearingAidReceipt()
        {
            return (this.Entity != null && this.DiagnosticosCollection != null && this.DiagnosticosCollection.Count(x => x.IsSelected) == 1);
        }

        private void OnPrintHearingAidReceipt()
        {
            try
            {
                var childVM = new PrintViewModel(this.Messenger, this.UserLogin, this.DiagnosticosCollection.FirstOrDefault(x => x.IsSelected)) { ParentViewModel = this };
                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                MessengerInstance.Send(messageDailog);
            }
            catch (Exception )
            {
             
            }
        }
        //char
        //private bool CanPrintEarMoldReceipt()
        //{
        //    return (this.Entity != null && this.Entity.CustomerEarMoldOrderCollection != null && this.Entity.CustomerEarMoldOrderCollection.Count(x => x.IsSelected) == 1);
        //}

        //private void OnPrintEarMoldReceipt()
        //{
        //    try
        //    {
        //        var childVM = new PrintViewModel(this.Messenger, this.UserLogin, this.Entity.CustomerEarMoldOrderCollection.FirstOrDefault(x => x.IsSelected)) { ParentViewModel = this };
        //        var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
        //        MessengerInstance.Send(messageDailog);
        //    }
        //    catch (Exception exception)
        //    {
                
        //    }
        //}
        /*************************/
        #endregion

        #region public Methods

        #endregion

        #region Private Methods
        /*
        private void RefreshCustomerEarMoldOrderCollection(bool doAsync = false)
        {
            try
            {
                if (doAsync)
                    Task.Factory.StartNew(() =>
                    {
                        var items = CustomerAction.GetCustomerEarMoldOrderList(this.DBConnectionString, this.Entity);
                        if (items != null && items.InternalList.Any())
                            this.Entity.CustomerEarMoldOrderCollection = items.InternalList;
                    });
                else
                {
                    var items = CustomerAction.GetCustomerEarMoldOrderList(this.DBConnectionString, this.Entity);
                    if (items != null && items.InternalList.Any())
                        this.Entity.CustomerEarMoldOrderCollection = items.InternalList;
                }
            }
            catch (Exception exception)
            {
                
            }
           
        }

        private void RefreshCustomerEarMoldOrderCollection()
        {
            try
            {
                Task.Factory.StartNew(() =>
                    {
                        var items = CustomerAction.GetCustomerEarMoldOrderList(this.DBConnectionString, this.Entity);
                        if (items != null && items.InternalList.Any())
                            this.Entity.CustomerEarMoldOrderCollection = items.InternalList;
                    });

            }
            catch (Exception exception)
            {
                
            }

        }*/

        private void RefreshCustomerHearingAidOrderCollection(bool doAsync = false)
        {
            try
            {
                DataService _dataservice = new DataService();
                if (doAsync)
                    Task.Factory.StartNew(() =>
                        {
                            var items = _dataservice.GetDXConsultation(this.Entity);
                            if (items != null && items.Any())
                                this.DiagnosticosCollection = items;

                        });
                else
                {
                    var items = _dataservice.GetDXConsultation(this.Entity);
                    if (items != null && items.Any())
                        this.DiagnosticosCollection = items;
                }
            }
            catch (Exception)
            {
               
            }

        }

        private void RefreshCustomerHearingAidOrderCollection()
        {
            try
            {
                DataService _dataservice = new DataService();

                Task.Factory.StartNew(() =>
                    {
                        var items = _dataservice.GetDXConsultation(this.Entity);
                        if (items != null && items.Any())
                            this.DiagnosticosCollection = items;

                    });

            }
            catch (Exception )
            {
                
            }

        }
        /*
        private void DeleteCustomerEarMoldOrder(DialogResult dialogResult)
        {
            if (dialogResult == DialogResult.Ok)
            {
                Task.Factory.StartNew(() =>
                {
                    CustomerAction.DeleteCustomerEarMoldOrders(this.DBConnectionString, this.Entity.CustomerEarMoldOrderCollection.Where(x => x.IsSelected));
                    RefreshCustomerEarMoldOrderCollection();
                });
            }
            else
            {
                this.ParentViewModel.ShowProgressBar = false;
            }
        }
        */
        private void DeleteCustomerHearingAidOrder(DialogResult dialogResult)
        {
            if (dialogResult == DialogResult.Ok)
            {
                DataService _dataservice = new DataService();
                Task.Factory.StartNew(() =>
                {
                    _dataservice.DeleteDXConsultation(this.DiagnosticosCollection.Where(x => x.IsSelected));
                    RefreshCustomerHearingAidOrderCollection();
                });
            }
            else
            {
                this.ParentViewModel.ShowProgressBar = false;
            }
        }
        #endregion
    }
}