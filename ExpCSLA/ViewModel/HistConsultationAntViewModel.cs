﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class HistConsultationAntViewModel : BaseViewModel<List<Form_Exp_Consulta>>
    {

        private DataService _dataservice = new DataService();
      
        private Pacientes _consulta;
        public Pacientes PatientSelected
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("PatientSelected");
            }
        }

        private Consultas apf = new Consultas();

        
        private List<Form_Exp_Consulta> _DiagnosticosConsulta;
        public List<Form_Exp_Consulta> DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                
                _DiagnosticosConsulta = value;
                this.RaisePropertyChanged("DiagnosticosConsulta");
               
            }
        }

        
 


        private List<Diagnosticos> _ComboDiagnosticosItems;
           public List<Diagnosticos> ComboDiagnosticosItems
        {
            get { return _ComboDiagnosticosItems; }
            set
            {
                _ComboDiagnosticosItems = value;
                this.RaisePropertyChanged("ComboDiagnosticosItems");
            }
        }


           private Diagnosticos _SelectedDiagnostic;
           public Diagnosticos SelectedDiagnostic
           {
               get { return _SelectedDiagnostic; }
               set
               {
                   _SelectedDiagnostic = value;
                   this.RaisePropertyChanged("SelectedDiagnostic");
               }
           }

        
        #region Delegate
        public Action RefreshConsultationDX { get; set; }
        #endregion

        #region Properties

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #endregion

        #region Commands
        
        private ICommand _cancelCommand;
        
    

        public ICommand CancelCommand
        {
            get { return this._cancelCommand ?? (this._cancelCommand = new RelayCommand(OnCancelCustomer)); }
        }

               
        #endregion

        #region Constructors

        public HistConsultationAntViewModel(IMessenger messenger, Usuario userLogin, Pacientes _paciente,Especialidades speciality)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.PatientSelected = _paciente;

            if (speciality.ID != 1000)
            this.DiagnosticosConsulta = _dataservice.GetFormEspecPatient(PatientSelected, "A").Where(u => u.Consultas.idEspecialidad == speciality.ID).ToList();
            else
                this.DiagnosticosConsulta = _dataservice.GetFormEspecPatient(PatientSelected, "A").ToList();

            OrdenarFormularios();

            //AgregarDatosFormConsulta();
            IsInEditMode = false;


        }

        public HistConsultationAntViewModel(IMessenger messenger, Usuario userLogin, Pacientes _paciente)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.PatientSelected = _paciente;
            this.DiagnosticosConsulta = _dataservice.GetFormEspecPatient(PatientSelected, "A");
            OrdenarFormularios();
                //AgregarDatosFormConsulta();
                IsInEditMode = false;
            

        }

        public HistConsultationAntViewModel(IMessenger messenger)
            : base(messenger)
        {
            this.DiagnosticosConsulta = new List<Form_Exp_Consulta>();
            
                //AgregarDatosFormConsulta();
                IsInEditMode = false;
            
        }

        public HistConsultationAntViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            this.DiagnosticosConsulta = new List<Form_Exp_Consulta>();
            
                //AgregarDatosFormConsulta();
                IsInEditMode = false;
            
        }

       
        #endregion

        #region Privados

        #endregion  

        #region Override Methods

        public override void Initialize()
        {
            base.Initialize();

            //CargarDiagnosticos();

        }

        #endregion

        #region Command Methods
        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }
        


        private void OnCancelCustomer()
        {
            var messageDailog = new MessageDailog((result) =>
                {
                    if (result == DialogResult.Ok)
                    {
                        if (this.ParentViewModel != null)
                            this.ParentViewModel.ChildViewModel = null;
                        this.Unload();

                        if (this.CloseWindow != null)
                            this.CloseWindow();
                    }
                }) { Caption = Resources.MessageResources.CancelWindowMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);


        }

        public void RefreshConsultSpeciality(Especialidades _idSpec)
        {
            try
            {

                if (_idSpec.ID != 1000)
                {


                    var task = Task.Factory.StartNew(() =>
                    {
                        this.DiagnosticosConsulta = _dataservice.GetFormEspecPatient(PatientSelected, "A").Where(u => u.Consultas.idEspecialidad == _idSpec.ID).ToList();
                        OrdenarFormularios();

                    });

                    task.Wait();
                }
                else
                {


                    var task = Task.Factory.StartNew(() =>
                    {
                        this.DiagnosticosConsulta = _dataservice.GetFormEspecPatient(PatientSelected, "A");
                        OrdenarFormularios();
                    });

                    task.Wait();

                }

            }
            catch (Exception )
            {

            }

        }
        
        private void OrdenarFormularios()
        {
           
            var tempCollection = this.DiagnosticosConsulta;
            this.DiagnosticosConsulta = new List<Form_Exp_Consulta>();
            //Acomodo por cada cita
            bool PrimerFilaCita;
            foreach (var itemConsultation in _dataservice.GetConsultasPatient(PatientSelected))
            {
                PrimerFilaCita = true;
                //Acomo por padre
                foreach (var item in tempCollection.Where(x => x.idConsulta == itemConsultation.ID && x.Form_Expediente.idFormulario_Padre == null).OrderBy(m => m.Form_Expediente.idFormulario))
                {
                    item.isFirst = PrimerFilaCita;
                    this.DiagnosticosConsulta.Add(item);
                    PrimerFilaCita = false;
                    //Acomodo por hijos.
                    foreach (var itemHijo in tempCollection.Where(x => x.idConsulta == itemConsultation.ID && x.Form_Expediente.idFormulario_Padre == item.Form_Expediente.idFormulario).OrderBy(m => m.Form_Expediente.Orden))
                    {
                        itemHijo.isFirst = PrimerFilaCita;
                        this.DiagnosticosConsulta.Add(itemHijo);
                    }
                }
            }
           
            this.RaisePropertyChanged("DiagnosticosConsulta");
        
        }
       
        private bool CanDeletePhone()
        {
            return this.Entity != null && this.DiagnosticosConsulta != null &&
                   this.DiagnosticosConsulta.Any(x => x.IsSelected);
            
        }

       
        #endregion



    }
}