﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using ExpCSF.Base;
using ExpCSF.Commands;
using ExpCSF.Controls;
using ExpWCF;
using System.Windows.Input;
using ExpCSF.Resources;

namespace ExpCSF.ViewModel
{
    
    public class HistImagesReportsViewModel : BaseViewModel<Comodin>
    {
        
        DataService _dataservice = new DataService();

         private Pacientes _consulta;
        public Pacientes PatientSelected
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("PatientSelected");
            }
        }

         public HistImagesReportsViewModel(IMessenger messenger, Usuario userLogin, Pacientes _paciente)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.PatientSelected = _paciente;
            IsInEditMode = false;
            

        }

        public ICommand AddInfoCommand { get; set; }
        public virtual bool CanAddInfo()
        {
            return true;
        }

        private ICommand _NavigateToClientCommand;
        public ICommand NavigateToClientCommand
        {

            get { return this._NavigateToClientCommand ?? (this._NavigateToClientCommand = new RelayCommand<object>((valor) => OnNavigateClient(valor))); }

        }



        private List<PlantillaMotivoConsulta> _appointmentCollection;
        public List<PlantillaMotivoConsulta> AppointmentCollection
        {
            get
            {
               
                    _appointmentCollection = _dataservice.GetPlantillasConsPatient(PatientSelected.IdPaciente);
               

                return _appointmentCollection;
            }
            set
            {
                _appointmentCollection = value; ;
                this.RaisePropertyChanged("AppointmentCollection");
            }
        }

       
        private Consultas _selectedConsultation;
        public Consultas SelectedConsultation
        {
            get { return _selectedConsultation; }
            set
            {
                _selectedConsultation = value;
                this.RaisePropertyChanged("OnAddCustomer");
            }
        }

        private void OnNavigateClient(object _Padre)
        {
            this.ShowPatientInfo(_Padre);


        }

      
        public override void OnEditItem()
        {
            try
            {
                var childVM = new AdjuntarViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation)
                {
                    ParentViewModel = this
                };

                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                MessengerInstance.Send(messageDailog);

            }
            catch (Exception )
            {

            }
        }

        public  void OnAddInfo()
        {
            try
            {

                this.ContentViewModel = new DetailPatientInfoViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
                
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);

            }
            catch (Exception )
            {

            }
        }

        private void ShowPatientInfo(object data)
        {
            try
            {
            Views.ViewerReporte a = new Views.ViewerReporte(int.Parse(data.ToString()),false);
            a.Show();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void OnAddCustomer()
        {
            //ShowProgressBar = true;
            //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            //MessengerInstance.Send(messageDailog);
            try
            {

                var childVM = new AddPatientViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
                //childVM.RefreshCustomers += this.GetCustomerCollection;
                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                ShowProgressBar = true;
                //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
                //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = "Esta seguro que desea eliminar esto ?", DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

                //MessengerInstance.Send(messageDailog);
            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured,
                //                    ExceptionResources.ExceptionOccuredLogDetail);
            }
        }

        private void DeleteCustomer(DialogResult dialogResult)
        {
            if (dialogResult == DialogResult.Ok)
            {
                Task.Factory.StartNew(() =>
                {
                    for (int i = 0; i < 1000000; i++)
                    {
                        var a = "prueba";
                    }

                    ShowProgressBar = false;
                });
            }
            else
            {
                ShowProgressBar = false;
            }
        }

        #region Fields

        #endregion

        #region Properties
        public override string Title
        {
            get
            {
                return "PRUEBA CARGAR DATOS";
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 50;
            }
        }

        public override double WindowHeight
        {
            get
            {
                return 400;
            }
        }

        public override double WindowWidth
        {
            get
            {
                return 600;
            }
        }


        #endregion

        #region Commands


        #endregion

       
        #region Public Methods

        #endregion

        #region Privae Methods

        #endregion

        #region Command Methods

        #endregion

        #region Virtual Methods
        public override void Initialize()
        {
            base.Initialize();
            //AddCommand = new RelayCommand(OnAddItem, CanAddItem);
            //EditCommand = new RelayCommand(OnEditItem, CanEditItem);
            //AddInfoCommand = new RelayCommand(OnAddInfo, CanAddInfo);
        }

        #endregion
    }

}
