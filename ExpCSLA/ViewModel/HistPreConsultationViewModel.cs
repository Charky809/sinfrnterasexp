﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Threading;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class HistPreConsultationViewModel : BaseViewModel<List<SIGNOS_VITALES>>
    {

        private DataService _dataservice = new DataService();

        private Pacientes _consulta;
        public Pacientes PatientSelected
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("PatientSelected");
            }
        }

        private Consultas apf = new Consultas();


        private List<SIGNOS_VITALES> _DiagnosticosConsulta;
        public List<SIGNOS_VITALES> DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {

                _DiagnosticosConsulta = value;
                //OrdenarFormularios();
                this.RaisePropertyChanged("DiagnosticosConsulta");

            }
        }




        private Diagnosticos _SelectedDiagnostic;
        public Diagnosticos SelectedDiagnostic
        {
            get { return _SelectedDiagnostic; }
            set
            {
                _SelectedDiagnostic = value;
                this.RaisePropertyChanged("SelectedDiagnostic");
            }
        }


        #region Delegate
        public Action RefreshConsultationDX { get; set; }
        #endregion

        #region Properties

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #endregion

        #region Commands

        private ICommand _cancelCommand;
        


        public ICommand CancelCommand
        {
            get { return this._cancelCommand ?? (this._cancelCommand = new RelayCommand(OnCancelCustomer)); }
        }


        #endregion

        #region Constructors

        public HistPreConsultationViewModel(IMessenger messenger, Usuario userLogin, Pacientes _paciente, Especialidades _especialidad)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.PatientSelected = _paciente;
            RefreshConsultSpeciality(_especialidad);
            //OrdenarFormularios();
            //AgregarDatosFormConsulta();
            IsInEditMode = false;
            

        }

        public HistPreConsultationViewModel(IMessenger messenger, Usuario userLogin, Pacientes _paciente)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.PatientSelected = _paciente;
            this.DiagnosticosConsulta = _dataservice.GetPreConsultationsPatient(PatientSelected);
            OrdenarFormularios();
            //AgregarDatosFormConsulta();
            IsInEditMode = false;

        }

        public HistPreConsultationViewModel(IMessenger messenger)
            : base(messenger)
        {
            this.DiagnosticosConsulta = new List<SIGNOS_VITALES>();

            //AgregarDatosFormConsulta();
            IsInEditMode = false;

        }

        public HistPreConsultationViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            this.DiagnosticosConsulta = new List<SIGNOS_VITALES>();

            //AgregarDatosFormConsulta();
            IsInEditMode = false;

        }


        #endregion

        #region Privados

        #endregion

        #region Override Methods

        public override void Initialize()
        {
            base.Initialize();

            //CargarDiagnosticos();

        }

        #endregion

        #region Command Methods
        public void RefreshConsultSpeciality(Especialidades _idSpec)
        {
            try
            {

                if (_idSpec.ID != 1000)
                {
                    

                    var task = Task.Factory.StartNew(() =>
                    {
                        this.DiagnosticosConsulta = _dataservice.GetPreConsultationsPatient(PatientSelected).Where(u => u.Consultas.idEspecialidad == _idSpec.ID).ToList();
                        OrdenarFormularios();

                    });

                    task.Wait();
                }
                else
                {


                    var task = Task.Factory.StartNew(() =>
                    {
                        this.DiagnosticosConsulta = _dataservice.GetPreConsultationsPatient(PatientSelected);
                        OrdenarFormularios();
                    });

                    task.Wait();

                }

            }
            catch (Exception )
            {

            }

        }

        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }



        private void OnCancelCustomer()
        {
            var messageDailog = new MessageDailog((result) =>
                {
                    if (result == DialogResult.Ok)
                    {
                        if (this.ParentViewModel != null)
                            this.ParentViewModel.ChildViewModel = null;
                        this.Unload();

                        if (this.CloseWindow != null)
                            this.CloseWindow();
                    }
                }) { Caption = Resources.MessageResources.CancelWindowMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);


        }


        private void OrdenarFormularios()
        {

            var tempCollection = this.DiagnosticosConsulta;
            this.DiagnosticosConsulta = new List<SIGNOS_VITALES>();
            //Acomodo por cada cita
            bool PrimerFilaCita;
            foreach (var itemConsultation in _dataservice.GetConsultasPatient(PatientSelected))
            {
                PrimerFilaCita = true;
                //Acomo por padre
                foreach (var item in tempCollection.Where(x => x.IdConsulta == itemConsultation.ID))
                {
                    item.isFirst = PrimerFilaCita;
                    this.DiagnosticosConsulta.Add(item);
                    PrimerFilaCita = false;

                }
            }

            this.RaisePropertyChanged("DiagnosticosConsulta");

        }

        private bool CanDeletePhone()
        {
            return this.Entity != null && this.DiagnosticosConsulta != null &&
                   this.DiagnosticosConsulta.Any(x => x.IsSelected);

        }


        #endregion



    }
}