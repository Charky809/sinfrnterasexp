﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using ExpCSF.Base;
using ExpCSF.Commands;
using ExpCSF.Controls;
using ExpWCF;
using System.Windows.Input;
using ExpCSF.Resources;

namespace ExpCSF.ViewModel
{
    
    public class ListaViewModel : BaseViewModel<Comodin>
    {
        
        DataService _dataservice = new DataService();

        public ICommand AddInfoCommand { get; set; }
        public virtual bool CanAddInfo()
        {
            return true;
        }

        private ICommand _customerCommand;
        public ICommand CustomerCommand
        {
            get
            {
                return this._customerCommand ??
                       (this._customerCommand = new RelayCommand(OnAddItem, CanAddItem));
            }
        }

        private List<Consultas> _appointmentCollection;
        public List<Consultas> AppointmentCollection
        {
            get
            {
                //return _appointmentCollection; 
                if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("enfermero"))
                {
                    _appointmentCollection = _dataservice.GetConsultasEnfermeria(DateTime.Now);
                }
                else if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("medico"))
                {
                    if (UserLogin.medicos != null)
                    {
                        if (UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades.DESCRIPCION.ToLower().Equals("imagenes"))
                        {
                            _appointmentCollection = _dataservice.GetConsultasReportes(DateTime.Now);
                        }
                        else
                        {
                            _appointmentCollection = _dataservice.GetConsultasMedicos(DateTime.Now, ((int)UserLogin.IdMedico));
                        }

                    }
                    else
                    {
                        _appointmentCollection = _dataservice.GetConsultasMedicos(DateTime.Now, ((int)UserLogin.IdMedico));
                    }
                    
                }
                else
                {
                    _appointmentCollection = _dataservice.GetConsultasRecepcion(DateTime.Now);
                }

                foreach (var item in _appointmentCollection)
                {
                    if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("digitador"))
                        item.EsDigitador = true;
                    else
                        item.EsDigitador = false;
                    
                }

                return _appointmentCollection;
            }
            set
            {
                _appointmentCollection = value; ;
                this.RaisePropertyChanged("AppointmentCollection");
            }
        }

        private List<Consultas> _unasConsultas = new List<Consultas>();
        public List<Consultas> UnasConsultas
        {
            get
            {
                if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("enfermero"))
                {
                    _unasConsultas = _dataservice.GetConsultasEnfermeria(DateTime.Now);
                }
                else if (UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("medico"))
                {
                    _unasConsultas = _dataservice.GetConsultasMedicos(DateTime.Now, ((int)UserLogin.IdMedico));
                }
                else
                {
                    _unasConsultas = _dataservice.GetConsultasRecepcion(DateTime.Now);
                }

                return _unasConsultas;


                //return null;

            }


        }

        int _pruebaCategoria;
        public int PruebaCategoria
        {
            get
            { return 1; }
            set
            {
                _pruebaCategoria = value;
                this.RaisePropertyChanged("PruebaCategoria");
            }
        }

       
        private Consultas _selectedConsultation;
        public Consultas SelectedConsultation
        {
            get { return _selectedConsultation; }
            set
            {
                _selectedConsultation = value;
                this.RaisePropertyChanged("OnAddCustomer");
            }
        }

        #region Override Command Methods
        public override void OnAddItem()
        {
            this.ShowPatientInfo();
        }
        #endregion

        public override void OnEditItem()
        {
            try
            {
                var childVM = new AdjuntarViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation)
                {
                    ParentViewModel = this
                };

                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                MessengerInstance.Send(messageDailog);

            }
            catch (Exception )
            {

            }
        }

        public  void OnAddInfo()
        {
            try
            {

                this.ContentViewModel = new DetailPatientInfoViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
                
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);

            }
            catch (Exception )
            {

            }
        }

        private void ShowPatientInfo()
        {
            try
            {
                //this.ContentViewModel = new PatientSummaryViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation.Pacientes) { ParentViewModel = this.ParentViewModel.ParentViewModel }; 
                //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
                if (this.ParentViewModel.GetType().Name == "DetailPatientInfoViewModel")
                {
                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).DetailSectionViewModel = null;

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).CustomerSummaryViewModel = new PatientSummaryViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ParentViewModel };

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).DetailSectionViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).CustomerSummaryViewModel;

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ConsultationDxViewModel = new AddConsultationDXViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ParentViewModel };

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ConsultationProblemViewModel = new AddConsultationProblemViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ParentViewModel };

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ConsultationAntViewModel = new AddConsultationAntViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ParentViewModel };

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ConsultationExFxViewModel = new AddConsultationEXFXViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ParentViewModel };

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ConsultationVacunasViewModel = new AddConsultationVacunasViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ParentViewModel };

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).MedicNoteViewModel = new AddMedicNoteViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ParentViewModel };

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).PreconsultationViewModel = new PreConsultationViewModel(this.Messenger, this.UserLogin, this.SelectedConsultation) { ParentViewModel = ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).ParentViewModel };

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).SelectedMenuItem = MenuResources.Detail;

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).SelectedCustomer = this.SelectedConsultation.Pacientes;

                    ((ExpCSF.ViewModel.DetailPatientInfoViewModel)(this.ParentViewModel)).SelectedConsultation = this.SelectedConsultation;


                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void OnAddCustomer()
        {
            //ShowProgressBar = true;
            //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            //MessengerInstance.Send(messageDailog);
            try
            {

                var childVM = new AddPatientViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
                //childVM.RefreshCustomers += this.GetCustomerCollection;
                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                ShowProgressBar = true;
                //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
                //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = "Esta seguro que desea eliminar esto ?", DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

                //MessengerInstance.Send(messageDailog);
            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured,
                //                    ExceptionResources.ExceptionOccuredLogDetail);
            }
        }

        private void DeleteCustomer(DialogResult dialogResult)
        {
            if (dialogResult == DialogResult.Ok)
            {
                Task.Factory.StartNew(() =>
                {
                    for (int i = 0; i < 1000000; i++)
                    {
                        var a = "prueba";
                    }

                    ShowProgressBar = false;
                });
            }
            else
            {
                ShowProgressBar = false;
            }
        }

        #region Fields

        #endregion

        #region Properties
        public override string Title
        {
            get
            {
                return "PRUEBA CARGAR DATOS";
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 50;
            }
        }

        public override double WindowHeight
        {
            get
            {
                return 400;
            }
        }

        public override double WindowWidth
        {
            get
            {
                return 600;
            }
        }


        #endregion

        #region Commands


        #endregion

        #region Constructors
        public ListaViewModel(IMessenger messenger)
            : base(messenger)
        {

        }
        public ListaViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {

        }
        #endregion

        #region Public Methods

        #endregion

        #region Privae Methods

        #endregion

        #region Command Methods

        #endregion

        #region Virtual Methods
        public override void Initialize()
        {
            base.Initialize();
            AddCommand = new RelayCommand(OnAddItem, CanAddItem);
            EditCommand = new RelayCommand(OnEditItem, CanEditItem);
            AddInfoCommand = new RelayCommand(OnAddInfo, CanAddInfo);
        }

        #endregion
    }

}
