﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Base;
using ExpWCF;
using ExpCSF.Controls;

namespace ExpCSF.ViewModel
{
    public class MainViewModel : BaseViewModel
    {
        #region · Fields ·

        private WindowState windowState;
        
        private bool _isVisibile;
        private string _searchText;
        private IDictionary<String, String> _customerSearchList;
        private int _selectedCustomerID;
        private bool _isPopupOpen;

        #endregion

        #region · Commands Fileds·

        private ICommand _maximizeCommand;
        private ICommand _minimizeCommand;
        private ICommand _ChangeUserCommand;
        private ICommand _shutdownCommand;
        private ICommand _showAboutBoxCommand;

        private ICommand _customerCommand;
        private ICommand _searchCommand;
        private ICommand _selectSearchItemCommand;
        private ICommand _appointmentCommand;


        private ICommand _homeCommand;
        private ICommand _callRegistryCommand;
        private bool _isHomeSelected;
        private bool _isCustomerTabSelected;
        private bool _isAppointmentTabSelected;
        
        private bool _isCallRegistryTabSelected;
        private ICommand _settingCommand;
        private bool _isSettingTabSelected;
        private ICommand _miscellaneousCommand;
        private bool _isMiscellaneousTabSelected;

        #endregion

        #region · Commands ·

        /// <summary>
        /// Gets the maximize command.
        /// </summary>
        /// <value>The maximize command.</value>
        public ICommand MaximizeCommand
        {
            get
            {
                if (this._maximizeCommand == null)
                {
                    this._maximizeCommand = new RelayCommand(OnMaximizeWindow);
                }

                return this._maximizeCommand;
            }
        }

        /// <summary>
        /// Gets the minimize command.
        /// </summary>
        /// <value>The minimize command.</value>
        public ICommand MinimizeCommand
        {
            get { return this._minimizeCommand ?? (this._minimizeCommand = new RelayCommand(OnMinimizeWindow)); }
        }

        public ICommand ChangeUserCommand
        {
            get { return this._ChangeUserCommand ?? (this._ChangeUserCommand = new RelayCommand(OnChangeUser)); }
        }
        


        /// <summary>
        /// Gets the about box command.
        /// </summary>
        /// <value>The about box command.</value>
        public ICommand ShowAboutBoxCommand
        {
            get { return this._showAboutBoxCommand ?? (this._showAboutBoxCommand = new RelayCommand(OnShowAboutBoxCommand)); }
        }

        /// <summary>
        /// Gets the shutdown command
        /// </summary>
        public ICommand ShutdownCommand
        {
            get { return this._shutdownCommand ?? (this._shutdownCommand = new RelayCommand(OnShutdown)); }
        }

        /// <summary>
        /// Gets the log off command
        /// </summary>
        public ICommand CloseSessionCommand
        {
            get { return this._customerCommand ?? (this._customerCommand = new RelayCommand(OnCloseSession)); }
        }

        public ICommand CustomerCommand
        {
            get
            {
                return this._customerCommand ??
                       (this._customerCommand = new RelayCommand(OnCustomerSelect, CanCustomerSelect));
            }
        }

        public ICommand SearchCommand
        {
            get { return this._searchCommand ?? (this._searchCommand = new RelayCommand(OnSearchSelect, CanSearchSelect)); }
        }

        public ICommand SelectSearchItemCommand
        {
            get
            {
                return this._selectSearchItemCommand ??
                       (this._selectSearchItemCommand = new RelayCommand(OnSelectSearchItem));
            }
        }

        public ICommand AppointmentCommand
        {
            get { return this._appointmentCommand ?? (this._appointmentCommand = new RelayCommand(OnAppointmentSelected)); }
        }

        public ICommand HomeCommand
        {
            get { return this._homeCommand ?? (this._homeCommand = new RelayCommand(OnHomeSelected)); }
        }

        public ICommand CallRegistryCommand
        {
            get { return _callRegistryCommand ?? (_callRegistryCommand = new RelayCommand(OnCallRegistrySelected)); }
        }

        public ICommand SettingCommand
        {
            get { return _settingCommand ?? (_settingCommand = new RelayCommand(OnSettingSelected)); }
        }

        public ICommand MiscellaneousCommand
        {
            get { return _miscellaneousCommand ?? (_miscellaneousCommand = new RelayCommand(OnMiscellaneousSelected)); }
        }


        #endregion

        #region Constructors

        public MainViewModel(IMessenger messenger)
            : base(messenger)
        {

            if (1 == 1)
                this.ChildViewModel = new UserLoginViewModel(messenger) { ParentViewModel = this };

            //this.OnHomeSelected();
            //this.ContentViewModel = new AddPatientViewModel(messenger) { ParentViewModel = this };
            //this.ContentViewModel = new AppointmentViewModel(messenger) { ParentViewModel = this };
        }

        #endregion

        #region · Properties ·

        /// <summary>
        /// Gets or sets the state of the window.
        /// </summary>
        /// <value>The state of the window.</value>
        public WindowState WindowState
        {
            get { return this.windowState; }
            set
            {
                if (this.windowState == value) return;
                this.windowState = value;
                this.RaisePropertyChanged("WindowState");
            }
        }

        /// <summary>
        /// Gets the logged in user name
        /// </summary>
        public string UserName
        {
            get
            {
                return (this.UserLogin != null && !String.IsNullOrEmpty(this.UserLogin.NombreUsuario)
                            ? this.UserLogin.NombreUsuario
                            : "No user Logged in");
            }
            //private set
            //{
            //    this.userName = value;
            //    this.RaisePropertyChanged(() => UserName);
            //}
        }

        public bool IsHomeTabSelected
        {
            get { return _isHomeSelected; }
            set
            {
                _isHomeSelected = value;
                this.RaisePropertyChanged("IsHomeTabSelected");
            }
        }

        public bool IsCustomerTabSelected
        {
            get { return _isCustomerTabSelected; }
            set
            {
                _isCustomerTabSelected = value;
                this.RaisePropertyChanged("IsCustomerTabSelected");
            }
        }

        public bool IsAppointmentTabSelected
        {
            get { return _isAppointmentTabSelected; }
            set
            {
                _isAppointmentTabSelected = value;
                this.RaisePropertyChanged("IsAppointmentTabSelected");
            }
        }

        public bool IsCallRegistryTabSelected
        {
            get { return _isCallRegistryTabSelected; }
            set
            {
                _isCallRegistryTabSelected = value;
                this.RaisePropertyChanged("IsCallRegistryTabSelected");
            }
        }

        public bool IsMiscellaneousTabSelected
        {
            get { return _isMiscellaneousTabSelected; }
            set
            {
                _isMiscellaneousTabSelected = value;
                this.RaisePropertyChanged("IsMiscellaneousTabSelected");
            }
        }

        public bool IsSettingTabSelected
        {
            get { return _isSettingTabSelected; }
            set
            {
                _isSettingTabSelected = value;
                this.RaisePropertyChanged("IsSettingTabSelected");
            }
        }

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                this.RaisePropertyChanged("SearchText");
                GetSearchList(_searchText);
            }
        }

        public IDictionary<String, String> CustomerSearchList
        {
            get { return _customerSearchList; }
            set
            {
                _customerSearchList = value;
                this.RaisePropertyChanged(" CustomerSearchList");
            }
        }

        public bool IsVisibile
        {
            get { return _isVisibile; }
            set
            {
                _isVisibile = value;
                this.RaisePropertyChanged("IsVisibile");
            }
        }

        public int SelectedCustomerID
        {
            get { return _selectedCustomerID; }
            set
            {
                _selectedCustomerID = value;
                this.RaisePropertyChanged("SelectedCustomerID");
            }
        }

        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set
            {
                _isPopupOpen = value;
                this.RaisePropertyChanged("IsPopupOpen");
            }
        }

        #endregion

        #region · Command Actions ·

        private void OnHomeSelected()
        {
            this.ContentViewModel = null;
            //cambio tile
            this.SelectedCustomerID = 0;

            //this.ContentViewModel = new PruebaViewModel(this.Messenger) { ParentViewModel = this };
            /***/
            //if (this.UserLogin != null)
            //    if (this.UserLogin.NombreUsuario == "Admin")
            //    {
            this.ContentViewModel = new MenuViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
            //    }
            //    else
            //    {
            //        this.ContentViewModel = new AppointmentViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
            //    }
            //else
            //{
            //    this.ContentViewModel = new MenuViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
            //}
            ////this.ContentViewModel = new LoginViewModel(this.Messenger) { ParentViewModel = this };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
        }

        /// <summary>
        /// Handles the shutdown command action
        /// </summary>
        private void OnShutdown()
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Closes the session
        /// </summary>
        private void OnCloseSession()
        {

        }

        /// <summary>
        /// Maximizes the window.
        /// </summary>
        private void OnMaximizeWindow()
        {
            if (this.WindowState == WindowState.Maximized)
            {
                this.WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

        }

        /// <summary>
        /// Handles the minimize window command action
        /// </summary>
        private void OnMinimizeWindow()
        {
            this.WindowState = WindowState.Minimized;
        }

        private void OnChangeUser()
        {
            this.ChildViewModel = new UserLoginViewModel(this.Messenger) { ParentViewModel = this };
        }
        
        private void OnShowAboutBoxCommand()
        {

        }

        private void OnCustomerSelect()
        {
            this.ContentViewModel = null;
            //char
            this.ContentViewModel = new ListaViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
        }

        private bool CanCustomerSelect()
        {
            return true;
        }

        private void OnSearchSelect()
        {
            this.ContentViewModel = null;
        }

        public void OnAppointmentSelected()
        {
            //this.ContentViewModel = null;
            this.ContentViewModel = new AppointmentViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };

            //this.ContentViewModel = new LoginViewModel(this.Messenger) { ParentViewModel = this };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);

        }

        private void OnCallRegistrySelected()
        {
            this.ContentViewModel = null;
            //char
            //this.ContentViewModel = new CallRegistryViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
            //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
        }

        private bool CanSearchSelect()
        {
            return true;
        }

        private void OnAddCustomer()
        {
            //ShowProgressBar = true;
            //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            //MessengerInstance.Send(messageDailog);
            try
            {

                var childVM = new AddPatientViewModel(this.Messenger, this.UserLogin, this.SearchText) { ParentViewModel = this };
                //childVM.RefreshCustomers += this.GetCustomerCollection;
                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                ShowProgressBar = true;
                //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
                //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = "Esta seguro que desea eliminar esto ?", DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

                //MessengerInstance.Send(messageDailog);
            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured,
                //                    ExceptionResources.ExceptionOccuredLogDetail);
            }
        }

        private void OnSelectSearchItem()
        {
            IsPopupOpen = false;
            //this.RaisePropertyChanged(" SearchText");

            if (this.SelectedCustomerID.ToString() != "-1")
            {

                CSLADBEntities contexto = new CSLADBEntities();

                //char
                Pacientes selectedCustomer = contexto.Pacientes.Where((u) => u.IdPaciente == this.SelectedCustomerID).FirstOrDefault();
                //                                                                this.SelectedCustomerID.ToString(
                //                                                                    CultureInfo.InvariantCulture));
                //if (this.ContentViewModel != null && this.ContentViewModel.GetType() == typeof(AppointmentViewModel))
                //{
                //    var vm = ((AppointmentViewModel)this.ContentViewModel);
                //    vm.Entity.SelectedCustomer = selectedCustomer;
                //    return;
                //}

                if (selectedCustomer != null)
                {
                    if (this.ContentViewModel.ContentViewModel is AppointmentViewModel)
                    {
                        ((AppointmentViewModel)(this.ContentViewModel.ContentViewModel)).SelectedCustomer = selectedCustomer;
                        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel.ContentViewModel);
                    }
                    else if (this.ContentViewModel is AppointmentViewModel)
                    {
                        ((AppointmentViewModel)(this.ContentViewModel)).SelectedCustomer = selectedCustomer;
                        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
                    }
                    else
                    {
                        this.ContentViewModel = null;
                        this.ContentViewModel = new DetailPatientInfoViewModel(this.Messenger, this.UserLogin, selectedCustomer) { ParentViewModel = this };
                        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
                    }
                }

            }
            else if (this.SelectedCustomerID.ToString() == "-1")
            {
                OnAddCustomer();
            }

            this.SearchText = "";
        }

        private void OnSettingSelected()
        {
            this.ContentViewModel = null;
            //char
            //this.ContentViewModel = new SettingsViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
            //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
        }

        private void OnMiscellaneousSelected()
        {
            this.ContentViewModel = null;
            //char
            //this.ContentViewModel = new MiscellaneousViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
            //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
        }
        #endregion

        #region Override Methods

        public override void Initialize()
        {
            base.Initialize();

            this.SelectDefaultTab();
        }

        #endregion

        #region Private Methods

        private void GetSearchList(string strSearchText)
        {
            try
            {
                if (string.IsNullOrEmpty(strSearchText))
                {
                    CustomerSearchList = null;
                    IsPopupOpen = false;
                    return;
                }

                Task.Factory.StartNew(() =>
                {
                    DataService _CSLAEntities = new DataService();
                    CustomerSearchList = _CSLAEntities.GetPatients(strSearchText);
                    IsPopupOpen = true;
                    if (CustomerSearchList == null || !CustomerSearchList.Any())
                    {
                        CustomerSearchList = new Dictionary<String, String>();
                        CustomerSearchList.Add(new KeyValuePair<string, string>("-1", "Agregar un nuevo paciente."));
                        CustomerSearchList.Add(new KeyValuePair<string, string>("-2", "Buscar Hijos"));
                    }

                    this.RaisePropertyChanged("CustomerSearchList");
                });

                this.SelectedCustomerID = 0;

            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured, ExceptionResources.ExceptionOccuredLogDetail);
            }

        }

        private void SelectDefaultTab()
        {
            if (!this.IsHomeTabSelected && !this.IsAppointmentTabSelected && !this.IsCustomerTabSelected &&
                !this.IsCallRegistryTabSelected)
                this.IsHomeTabSelected = true;
        }

        #endregion
    }
}