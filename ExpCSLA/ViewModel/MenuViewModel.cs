﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExpCSF.Base;
using ExpCSF.Commands;
using System.Windows;
using Telerik.Windows.Controls;
using System.Windows.Input;
using ExpCSF.Controls;
using System.Windows.Media;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Command;
using ExpWCF;
using System.Threading.Tasks;

namespace ExpCSF.ViewModel
{
    class MenuViewModel : BaseViewModel<Usuario>
    {
        private RelayCommand _tileSelectionChanged;

        private enum TileName
        {
            History,
            Settings,
            Reports,
            Agenda,
            File,
            Nursing
        }

        public RelayCommand tileSelectionChanged
        {
            get { return _tileSelectionChanged; }
            set
            {
                _tileSelectionChanged = value;
                this.RaisePropertyChanged("tileSelectionChanged");
            }
        }

        #region Constructors
        public MenuViewModel(IMessenger messenger)
            : base(messenger)
        {
            this.displayValue = 3498;
            getMenu();
        }
        public MenuViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            this.displayValue = 3498;
            getMenu();
        }
        #endregion

        public override void Initialize()
        {
            base.Initialize();
            this.tileSelectionChanged = new RelayCommand(OnDataProcess);
            

           
        }

                    
        #region Properties
        public override string Title
        {
            get
            {
                return "ACCESO";
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 100;
            }
        }

        public override double WindowHeight
        {
            get
            {
                return 600;
            }
        }

        public override double WindowWidth
        {
            get
            {
                return 600;
            }
        }

        private double displayValue;
        public double DisplayValue
        {
            get
            {
                return this.displayValue;
            }
            set
            {
                if (this.displayValue != value)
                {
                    this.displayValue = value;
                    this.RaisePropertyChanged("DisplayValue");
                }
            }
        }

        #endregion

        private List<Tile> items;
        public List<Tile> Items
        {
            get { return items; }
            set
            {
                items = value;
                this.RaisePropertyChanged("Items");
            }
        }

        public void UpdateDisplayValue()
        {
            if (this.DisplayValue == 3498)
            {
                this.DisplayValue = 3470;
            }
            else
            {
                this.DisplayValue = (int)3498;
            }
        }

        private object _selectedTileMenu;
        public object SelectedTileMenu
        {
            get { return _selectedTileMenu; }
            set
            {
                _selectedTileMenu = value;
                if (_selectedTileMenu != null)
                {
                    SelectedChange((Tile)value);
                }
            }
        }

        public void getMenu()
        {

            try
            {
                Uri uri = new Uri(@"..\images\ExpMed1.png", UriKind.Relative);
                Uri uri2 = new Uri(@"..\images\ExpMed2.png", UriKind.Relative);
                Uri uri3 = new Uri(@"..\images\ExpMed3.png", UriKind.Relative);


                Tile newItem = new Tile();
                AnimationView contentTile = new AnimationView();
                int index = 0;
                Items = new List<Tile>();

                int TagAssing = 1;
                string UsuarioConectado = UserLogin.UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower();
                if (UsuarioConectado.Equals("enfermero"))
                {
                    newItem = new Tile();
                    contentTile = new AnimationView();
                    contentTile.ImageSource = uri.OriginalString;
                    contentTile.Text = "Enfermería";
                    newItem.TileType = TileType.Double;
                    newItem.Content = contentTile;
                    newItem.Tag = TagAssing;
                    newItem.DisplayIndex = index;
                    newItem.Background = Brushes.CadetBlue;
                    newItem.Name = TileName.Nursing.ToString();
                    index++;
                    Items.Add(newItem);
                    TagAssing = TagAssing + 1;
                }


                if (UsuarioConectado.Equals("medico"))
                {
                    newItem = new Tile();
                    contentTile = new AnimationView();
                    contentTile.ImageSource = uri2.OriginalString;
                    contentTile.Text = "Expediente";
                    newItem.TileType = TileType.Double;
                    newItem.Content = contentTile;
                    newItem.Tag = TagAssing;
                    newItem.DisplayIndex = index;
                    newItem.Background = Brushes.Navy;
                    newItem.Name = TileName.File.ToString();
                    index++;
                    Items.Add(newItem);
                    TagAssing = TagAssing + 1;
                }

                /*
                * VERIFICAR OPCIONES DE MENU QUE SERAN DEFINITIVAS
                */
                newItem = new Tile();
                contentTile = new AnimationView();
                //contentTile.ImageSource = @"..\images\History.png";
                contentTile.ImageSource = uri3.OriginalString;
                contentTile.Text = "Histórico";
                newItem.TileType = TileType.Single;
                newItem.Content = contentTile;
                newItem.Tag = TagAssing;
                newItem.DisplayIndex = index;
                newItem.Background = Brushes.LightSteelBlue;
                newItem.Name = TileName.History.ToString();
                index++;
                Items.Add(newItem);

                if (UsuarioConectado.Equals("medico") || UsuarioConectado.Equals("digitador"))
                {
                    newItem = new Tile();
                    contentTile = new AnimationView();
                    contentTile.ImageSource = @"..\images\Settings.png";
                    contentTile.Text = "Mantenimiento";
                    newItem.TileType = TileType.Single;
                    newItem.Content = contentTile;
                    newItem.Tag = TagAssing;
                    newItem.DisplayIndex = index;
                    newItem.Background = Brushes.Coral;
                    newItem.Name = TileName.Settings.ToString();
                    index++;
                    Items.Add(newItem);

                    newItem = new Tile();
                    contentTile = new AnimationView();
                    contentTile.ImageSource = @"..\images\Reports.png";
                    contentTile.Text = "(N/A)Reportes";
                    newItem.TileType = TileType.Single;
                    newItem.Content = contentTile;
                    newItem.Tag = TagAssing;
                    newItem.DisplayIndex = index;
                    newItem.Background = Brushes.MediumBlue;
                    newItem.Name = TileName.Reports.ToString();
                    index++;
                    Items.Add(newItem);

                }


                if (UsuarioConectado != "enfermero")
                {
                    newItem = new Tile();
                    contentTile = new AnimationView();
                    contentTile.ImageSource = @"..\images\Others.png";
                    contentTile.Text = "Agenda";
                    newItem.TileType = TileType.Single;
                    newItem.Content = contentTile;
                    newItem.Tag = TagAssing;
                    newItem.DisplayIndex = index;
                    newItem.Background = Brushes.Maroon;
                    newItem.Name = TileName.Agenda.ToString();
                    index++;
                    Items.Add(newItem);

                    this.RaisePropertyChanged("Items");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }


        }

        private void SelectedChange(Tile tile)
        {
            this.ContentViewModel = null;

            TileName tileName = (TileName)Enum.Parse(typeof(TileName), tile.Name);

            switch (tileName)
            {
                case TileName.History:
                    this.ContentViewModel = new ListaViewModel(this.Messenger,this.UserLogin) { ParentViewModel = this.ParentViewModel };
                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
                    break;
                case TileName.Settings:
                    this.ContentViewModel = new SettingsViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this.ParentViewModel };
                  GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
                    break;
                case TileName.Nursing:
                    this.ContentViewModel = new DetailPatientInfoViewModel(this.Messenger,this.UserLogin) { ParentViewModel = this.ParentViewModel };
                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
                    break;
                case TileName.File:
                    this.ContentViewModel = new DetailPatientInfoViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this.ParentViewModel };
                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
                    break;
                case TileName.Agenda:
                     this.ContentViewModel = new AppointmentViewModel(this.Messenger,this.UserLogin,null) { ParentViewModel = this.ParentViewModel };
                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(this.ContentViewModel);
                    break;
                default:
                    break;
            }

            

           
        }


        #region Override Command Methods
        public override void OnAddItem()
        {
            this.OnAddCustomer();
        }
        #endregion

        private void OnAddCustomer()
        {
            //ShowProgressBar = true;
            //var messageDailog = new MessageDailog(DeleteCustomer) { Caption = Resources.MessageResources.DeleteMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            //MessengerInstance.Send(messageDailog);
            try
            {
                var childVM = new AddPatientViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };
                //childVM.RefreshCustomers += this.GetCustomerCollection;
                var messageDailog = new VMMessageDailog() { ChildViewModel = childVM };
                //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IBaseViewModel>(messageDailog);

            }
            catch (Exception )
            {
                //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured,
                //                    ExceptionResources.ExceptionOccuredLogDetail);
            }
        }

        private void DeleteCustomer(DialogResult dialogResult)
        {
            if (dialogResult == DialogResult.Ok)
            {

                Task.Factory.StartNew(() =>
                {
                    //CustomerAction.DeleteCustomers(this.DBConnectionString,
                    //                                this.Entity.InternalList.Where(x => x.IsSelected));
                    //GetCustomerCollection();
                    ShowProgressBar = false;
                });
            }
            else
            {
                ShowProgressBar = false;
            }
        }
    }
}
