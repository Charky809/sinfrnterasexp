﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExpCSF.Base;
using ExpWCF;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Controls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using GalaSoft.MvvmLight.Command;
using System.Data;
using ExpCSF.Resources;
using System.Threading.Tasks;

namespace ExpCSF.ViewModel
{
    class PatientSummaryViewModel : BaseViewModel<Pacientes>
    {
        #region Fields

        #endregion

        #region Properties
        
        public string Gender
        {
            get
            {
                string sexo = String.Empty;
                if (this.Entity != null && this.Entity.Sexo.HasValue)
                {
                    switch (this.Entity.Sexo.Value)
                    {
                        case 1:
                            sexo = "Femenino";
                            break;
                        case 2:
                            sexo = "Masculino";
                            break;
                        default:
                            break;
                    }
                }
                return this.Entity != null ? sexo : String.Empty;
            }
        }
        #endregion

        #region Command Properties

        #endregion

        #region Constructors
        public PatientSummaryViewModel(IMessenger messenger, Usuario userLogin, Pacientes customer)
            : base(messenger, userLogin)
        {
            this.Entity = customer;          
                         
            //this.RefreshCustomerHearingAidOrderCollection();
            //this.RefreshCustomerEarMoldOrderCollection();
            //this.GetRefreshCustomerRepairCollection();
        }

        public PatientSummaryViewModel(IMessenger messenger, Usuario userLogin, Consultas consultation)
            : base(messenger, userLogin)
        {
            this.Entity = consultation.Pacientes;

            if( consultation.idEspecialidad == null)
            {
                if (UserLogin.medicos != null)
                {
                    if (UserLogin.medicos.Especialidades_Medico.Count != 0)
                    {
                        Consultas modificar = consultation;
                        modificar.idEspecialidad = UserLogin.medicos.Especialidades_Medico.FirstOrDefault().idEspecialidad;

                        DataService _dataservice = new DataService();
                        _dataservice.UpdateAppointment(modificar);


                    }
                }
                else
                {
                    DataService _dataservice = new DataService();
                    Consultas modificar = consultation;
                    modificar.idEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault().ID;                    
                    _dataservice.UpdateAppointment(modificar);
                    
                }
            }


            //this.RefreshCustomerHearingAidOrderCollection();
            //this.RefreshCustomerEarMoldOrderCollection();
            //this.GetRefreshCustomerRepairCollection();
        }
        #endregion

        #region Override Methods

        #endregion

        #region Command Methods

        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        //private void GetRefreshCustomerRepairCollection()
        //{

        //    Task.Factory.StartNew(() =>
        //    {
        //        var items = CustomerAction.GetCustomerRepairList(this.DBConnectionString, this.Entity);
        //        if (items != null && items.InternalList.Any())
        //        {
        //            this.Entity.CustomerRepairCollection = null;
        //            this.Entity.CustomerRepairCollection = items.InternalList;
        //        }
        //    });
        //}

        //private void RefreshCustomerEarMoldOrderCollection()
        //{
        //    Task.Factory.StartNew(() =>
        //    {
        //        var items = CustomerAction.GetCustomerEarMoldOrderList(this.DBConnectionString, this.Entity);
        //        if (items != null && items.InternalList.Any())
        //            this.Entity.CustomerEarMoldOrderCollection = items.InternalList;
        //    });
        //}

        //private void RefreshCustomerHearingAidOrderCollection()
        //{
        //    Task.Factory.StartNew(() =>
        //    {
        //        var items = CustomerAction.GetCustomerHearingAidOrderList(this.DBConnectionString, this.Entity);
        //        if (items != null && items.InternalList.Any())
        //            this.Entity.CustomerHearingAidOrderCollection = items.InternalList;
        //    });
        //}
        #endregion



    }
}