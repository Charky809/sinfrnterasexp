﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.ComponentModel;


namespace ExpCSF.ViewModel
{
    class PreConsultationViewModel : BaseViewModel<SIGNOS_VITALES>, IDataErrorInfo
    {
        private DataService _dataservice = new DataService();

        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        #region Constructors
        public PreConsultationViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger,userLogin)
        {
            AgregarDatosConsulta();
        }

        public PreConsultationViewModel(IMessenger messenger,Usuario userLogin, SIGNOS_VITALES patient)
            : base(messenger,userLogin)
        {
            IsInEditMode = true;
            this.Entity = patient;
            AgregarDatosConsulta();

        }

        public PreConsultationViewModel(IMessenger messenger, Usuario userLogin, Consultas consultation)
            : base(messenger,userLogin)
        {
            IsInEditMode = true;
            this.ConsultaSeleccionada = consultation;
            this.Entity = _dataservice.GetPreConsultation(consultation);
            if (this.Entity == null)
            {
                AgregarDatosConsulta();
                IsInEditMode = false;
            }
        }
        #endregion


        private SIGNOS_VITALES _DiagnosticosConsulta;
        public SIGNOS_VITALES DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.Entity = value;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }

        private Consultas _consulta;
        public Consultas ConsultaSeleccionada
        {
            get { return _consulta; }
            set
            {
                _consulta = value;
                this.RaisePropertyChanged("ConsultaSeleccionada");
            }
        }

        #region Virtual Methods
        public override void Initialize()
        {
            base.Initialize();
            //this.SignInCommand = new RelayCommand(OnDataProcess, CanExecuteSingInCommand);
        }
        #endregion

        private void AgregarDatosConsulta()
        {
            try
            {

                if (ConsultaSeleccionada != null)
                {
                    Especialidades pEspecialidad;
                    if (ConsultaSeleccionada.medicos.Especialidades_Medico.FirstOrDefault() != null)
                    {
                        pEspecialidad = ConsultaSeleccionada.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
                    }
                    else if (ConsultaSeleccionada.Especialidades != null)
                    {
                        pEspecialidad = ConsultaSeleccionada.Especialidades;
                    }
                    else
                    {
                        pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
                    }

                    if (this.DiagnosticosConsulta == null)
                        this.DiagnosticosConsulta = new SIGNOS_VITALES() {  FECHA = DateTime.Now, DIAS_ENFERMEDAD = 0, IdConsulta= ConsultaSeleccionada.ID };

                    this.RaisePropertyChanged("DiagnosticosConsulta");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void OnSaveCustomer()
        {
            var returnStatus = false;
            //char
            returnStatus = !IsInEditMode ? _dataservice.AddVitalSigns(this.Entity) : _dataservice.UpdatePreConsultation(this.Entity);

            if (returnStatus)
            {
            
                var messageDailog = new MessageDailog()
                {
                    Caption = Resources.MessageResources.DataSavedSuccessfully,
                    DialogButton = DialogButton.Ok,
                    Title = Resources.TitleResources.Information
                };

                //GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
                MessengerInstance.Send(messageDailog);

                this.IsInEditMode = true;

                if (this.CloseWindow != null)
                    this.CloseWindow();
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                //Messenger.Default.Send(messageDailog);
                MessengerInstance.Send(messageDailog);
            }
        }

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? Resources.TitleResources.AddNewCustomer : Resources.TitleResources.EditCustomer;
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;

            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;


            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #region ErrorControl
        private string error = string.Empty;
        public string Error
        {
            get { return error; }
        }
        public string this[string columnName]
        {
            get
            {
                error = string.Empty;
                if (columnName == "NotaEnfermeria" && string.IsNullOrWhiteSpace(this.Entity.NotaEnfermeria))
                {
                    error = "La nota es requerida!";
                }
                else
                {
                    error = "Campo requerido!";
                }
            
                return error;

            }
        }
        #endregion
    }
}
