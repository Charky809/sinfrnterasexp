﻿using GalaSoft.MvvmLight;
using ExpCSF.Base;
using ExpWCF;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Resources;
using System.Data.Objects.DataClasses;
using ExpCSF.Controls;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class PrintViewModel : BaseViewModel
    {
        
        public PrintViewModel(IMessenger messenger, Usuario userLogin, EntityObject entity)
            : base(messenger, userLogin)
        {
            this.Entity = entity;
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean own resources if needed

        ////    base.Cleanup();
        ////}
        private EntityObject _entity;

        #region Properties
        public EntityObject Entity
        {
            get { return _entity; }
            set
            {
                _entity = value;
                this.RaisePropertyChanged("Entity");
            }
        }

        #region Window Properties
        public override string Title
        {
            get { return TitleResources.Print; }
        }


        public override double DialogStartupCustomHeight
        {
            get
            {
                return 750;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 950;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #endregion  
    }
}