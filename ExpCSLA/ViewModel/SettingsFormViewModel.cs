﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class SettingsFormViewModel : BaseViewModel<List<Form_Expediente>>
    {

        private DataService _dataservice = new DataService();


        List<ComboData> _comboItems = new List<ComboData>();
        public List<ComboData> ComboItems
        {
            get
            {
                return _comboItems;
            }
            set
            {
                _comboItems = value;
                this.RaisePropertyChanged("ComboItems");
            }
        }


        private int _SelectedDiagnosticID;
        public int SelectedDiagnosticID
        {
            get { return _SelectedDiagnosticID; }
            set
            {
                _SelectedDiagnosticID = value;
                this.RaisePropertyChanged("SelectedDiagnosticID");
            }
        }


        private List<Form_Expediente> _DiagnosticosConsulta;
        public List<Form_Expediente> DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }

        private List<Form_Expediente> _DiagnosticosConsultaChild;
        public List<Form_Expediente> DiagnosticosConsultaChild
        {
            get { return _DiagnosticosConsultaChild; }
            set
            {
                _DiagnosticosConsultaChild = value;
                this.RaisePropertyChanged("DiagnosticosConsultaChild");
            }
        }

            

           private Form_Expediente _SelectedForm;
           public Form_Expediente SelectedForm
           {
               get { return _SelectedForm; }
               set
               {
                   _SelectedForm = value;
                   this.RaisePropertyChanged("SelectedForm");
               }
           }

           private int _SelectedFather;
           public int SelectedFather
           {
               get { return _SelectedFather; }
               set
               {
                   _SelectedFather = value;
                   this.RaisePropertyChanged("SelectedFather");
               }
           }
                  
                           
        #region Delegate
        #endregion

        #region Fields
      
        #endregion

        #region Properties

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? "Formularios" : "Editar Formularios";
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #endregion

        #region Commands
        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private ICommand _saveCommandChild;
        public ICommand SaveCommandChild
        {
            get { return this._saveCommandChild ?? (this._saveCommandChild = new RelayCommand(OnSaveCustomerChild, CanSaveCustomer)); }
        }

        
        
        private ICommand _addPhoneCommand;
        private ICommand _deletePhoneCommand;
        private ICommand _addPhoneCommandChild;
        private ICommand _deletePhoneCommandChild;
        private ICommand _ChildLookCommand;


        //public ICommand CancelCommand
        //{
        //    get { return this._cancelCommand ?? (this._cancelCommand = new RelayCommand(OnCancelCustomer)); }
        //}

        public ICommand AddPhoneCommand
        {
            get { return this._addPhoneCommand ?? (this._addPhoneCommand = new RelayCommand(OnAddPhone)); }
        }

        public ICommand DeletePhoneCommand
        {
            get { return this._deletePhoneCommand ?? (this._deletePhoneCommand = new RelayCommand(OnDeletePhone, CanDeletePhone)); }
        }

        public ICommand AddPhoneCommandChild
        {
            get { return this._addPhoneCommandChild ?? (this._addPhoneCommandChild = new RelayCommand(OnAddPhoneChild)); }
        }

        public ICommand DeletePhoneCommandChild
        {
            get { return this._deletePhoneCommandChild ?? (this._deletePhoneCommandChild = new RelayCommand(OnDeletePhoneChild, CanDeletePhoneChild)); }
        }

        public ICommand ChildLookCommand
        {
            get { return this._ChildLookCommand ?? (this._ChildLookCommand = new RelayCommand<object>((valor)=>OnChildLook(valor))); }
        }

        #endregion

        #region Constructors

        private void ButtonClick(object button) { }

        public SettingsFormViewModel(IMessenger messenger)
            : base(messenger)
        {
            Especialidades pEspecialidad;
            if (UserLogin.medicos.Especialidades_Medico.FirstOrDefault() != null)
            {
                pEspecialidad = UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
            }
            else
            {
                pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            }
            
                //_dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            this.DiagnosticosConsulta = _dataservice.GetActiveFormEspecialidad(pEspecialidad);
            if(DiagnosticosConsulta.Count == 0)
                this.DiagnosticosConsulta = new List<Form_Expediente>();

            List<ComboData> comboData = new List<ComboData>();

            comboData.Add(new ComboData { Value = "Antecedentes", Id = "A" });
            comboData.Add(new ComboData { Value = "Exp. Fisica", Id = "E" });
            comboData.Add(new ComboData { Value = "Nutricion", Id = "NT" });
            comboData.Add(new ComboData { Value = "Psicologia", Id = "PS" });
            comboData.Add(new ComboData { Value = "Ter. Fisica", Id = "TF" });
            comboData.Add(new ComboData { Value = "Ter. Lenguaje", Id = "TL" });
            comboData.Add(new ComboData { Value = "Papanicolau", Id = "PP" });
            comboData.Add(new ComboData { Value = "Otros", Id = "OT" });

            this.ComboItems = comboData;

        }

        public SettingsFormViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            Especialidades pEspecialidad;
            if (UserLogin.medicos != null)
            {
                if (UserLogin.medicos.Especialidades_Medico.FirstOrDefault() != null)
                {
                    pEspecialidad = UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
                }
                else
                {
                    pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
                }
            }
            else
            {
                pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            }

                //_dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            this.DiagnosticosConsulta = _dataservice.GetActiveFormEspecialidad(pEspecialidad);
            if (DiagnosticosConsulta.Count == 0)
                this.DiagnosticosConsulta = new List<Form_Expediente>();

            List<ComboData> comboData = new List<ComboData>();

            comboData.Add(new ComboData { Value = "Antecedentes", Id = "A" });
            comboData.Add(new ComboData { Value = "Exp. Fisica", Id = "E" });
            comboData.Add(new ComboData { Value = "Nutricion", Id = "NT" });
            comboData.Add(new ComboData { Value = "Psicologia", Id = "PS" });
            comboData.Add(new ComboData { Value = "Ter. Fisica", Id = "TF" });
            comboData.Add(new ComboData { Value = "Ter. Lenguaje", Id = "TL" });
            comboData.Add(new ComboData { Value = "Papanicolau", Id = "PP" });
            comboData.Add(new ComboData { Value = "Otros", Id = "OT" });

            this.ComboItems = comboData;
        }

        public SettingsFormViewModel(IMessenger messenger, Usuario userLogin, List<Form_Expediente> customer)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.Entity = customer;
            this.DiagnosticosConsulta = customer;

            List<ComboData> comboData = new List<ComboData>();

            comboData.Add(new ComboData { Value = "Antecedentes", Id = "A" });
            comboData.Add(new ComboData { Value = "Exp. Fisica", Id = "E" });
            comboData.Add(new ComboData { Value = "Nutricion", Id = "NT" });
            comboData.Add(new ComboData { Value = "Psicologia", Id = "PS" });
            comboData.Add(new ComboData { Value = "Ter. Fisica", Id = "TF" });
            comboData.Add(new ComboData { Value = "Ter. Lenguaje", Id = "TL" });
            comboData.Add(new ComboData { Value = "Papanicolau", Id = "PP" });
            comboData.Add(new ComboData { Value = "Otros", Id = "OT" });

            this.ComboItems = comboData;

        }
        #endregion

        #region Override Methods

        public override void Initialize()
        {
            base.Initialize();

            //CargarDiagnosticos();

        }

        #endregion

        #region Command Methods
        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }
        private void OnSaveCustomer()
        {
            var returnStatus = false;
                        //char
            //returnStatus = !IsInEditMode ? _dataservice.AddFatherForm(this.DiagnosticosConsulta) : _dataservice.UpdateProblemConsultation(this.DiagnosticosConsulta);
            returnStatus = _dataservice.AddFatherForm(this.DiagnosticosConsulta);

            if (returnStatus)
            {
                returnStatus = _dataservice.UpdateFormExpediente(this.DiagnosticosConsulta.Where(x=>x.isNewItem == false).ToList());

                if (returnStatus)
                {
                    var messageDailog = new MessageDailog()
                     {
                         Caption = Resources.MessageResources.DataSavedSuccessfully,
                         DialogButton = DialogButton.Ok,
                         Title = Resources.TitleResources.Information
                     };

                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

                    if (this.CloseWindow != null)
                        this.CloseWindow();
                }
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            }
        }

        private bool CanSaveCustomerChild()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }
        private void OnSaveCustomerChild()
        {
            var returnStatus = false;
            //char
            //returnStatus = !IsInEditMode ? _dataservice.AddFatherForm(this.DiagnosticosConsulta) : _dataservice.UpdateProblemConsultation(this.DiagnosticosConsulta);
            returnStatus = _dataservice.AddFatherForm(this.DiagnosticosConsultaChild);

            if (returnStatus)
            {
                returnStatus = _dataservice.UpdateFormExpediente(this.DiagnosticosConsultaChild.Where(x => x.isNewItem == false).ToList());

                if (returnStatus)
                {
                    var messageDailog = new MessageDailog()
                    {
                        Caption = Resources.MessageResources.DataSavedSuccessfully,
                        DialogButton = DialogButton.Ok,
                        Title = Resources.TitleResources.Information
                    };

                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

                    if (this.CloseWindow != null)
                        this.CloseWindow();
                }
            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            }
        }


        private void OnCancelCustomer()
        {
            var messageDailog = new MessageDailog((result) =>
                {
                    if (result == DialogResult.Ok)
                    {
                        if (this.ParentViewModel != null)
                            this.ParentViewModel.ChildViewModel = null;
                        this.Unload();

                        if (this.CloseWindow != null)
                            this.CloseWindow();
                    }
                }) { Caption = Resources.MessageResources.CancelWindowMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);


        }

        private void OnAddPhone()
        {
            IsInEditMode = false;
           
                if (this.DiagnosticosConsulta == null)
                    this.DiagnosticosConsulta = new List<Form_Expediente>();

                var tempCollection = this.DiagnosticosConsulta;
                //string TipoForm = ("P" +  (tempCollection.Count + 1).ToString());
                tempCollection.Add(new Form_Expediente() { isNewItem = true, Orden = tempCollection.Count + 1, Estado = true, ConObservacion = true, TipoFormulario = "A" });
                this.DiagnosticosConsulta = null;
                this.DiagnosticosConsulta = tempCollection;
                this.RaisePropertyChanged("DiagnosticosConsulta");
                
           
        }

        private void OnChildLook(object _Padre)
        {
            //Especialidades pEspecialidad = _dataservice.GetActiveFormEspecialidadByFather(((Form_Expediente)_Padre).idFormulario).Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            SelectedFather = ((int)_Padre);
            SelectedForm = this.DiagnosticosConsulta.Where(u => u.idFormulario == SelectedFather).SingleOrDefault();
            this.DiagnosticosConsultaChild = _dataservice.GetActiveFormEspecialidadByFather(((int)_Padre));
            if (DiagnosticosConsultaChild.Count == 0)
                this.DiagnosticosConsultaChild = new List<Form_Expediente>();
                
           
        }
        

        private void OnAddPhoneChild()
        {
            IsInEditMode = false;

            if (this.DiagnosticosConsultaChild == null)
                this.DiagnosticosConsultaChild = new List<Form_Expediente>();

            var tempCollection = this.DiagnosticosConsultaChild;
            //string TipoForm = ("P" + (tempCollection.Count + 1).ToString());
            tempCollection.Add(new Form_Expediente() { isNewItem = true, Orden = tempCollection.Count + 1, Estado = true, ConObservacion = true, TipoFormulario = SelectedForm.TipoFormulario, idFormulario_Padre = SelectedFather, MetodoRegistro = 0 });
            this.DiagnosticosConsultaChild = null;
            this.DiagnosticosConsultaChild = tempCollection;
            this.RaisePropertyChanged("DiagnosticosConsultaChild");


        }

        private bool CanDeletePhone()
        {
            return this.Entity != null && this.DiagnosticosConsulta != null &&
                   this.DiagnosticosConsulta.Any(x => x.IsSelected);

        }

        private bool CanDeletePhoneChild()
        {
            return this.Entity != null && this.DiagnosticosConsultaChild != null &&
                   this.DiagnosticosConsultaChild.Any(x => x.IsSelected);

        }

        

        private void OnDeletePhone()
        {

            if (this.DiagnosticosConsulta != null &&
                 this.DiagnosticosConsulta.Any(x => x.IsSelected))
            {
                //char
                var qureyItems = this.DiagnosticosConsulta.Where(x => x.idFormulario > 0 && (x.IsSelected)).ToList();
                if (qureyItems.Any())
                {
                    _dataservice.DeleteForm_Expediente(qureyItems);
                    qureyItems.ForEach(x => x.IsDeleted = true);
                }

                var items = this.DiagnosticosConsulta.Where(x => x.IsSelected && (x.idFormulario == 0)).ToList();

                if (items.Any())
                {
                    foreach (var customerPhone in items)
                    {
                        this.DiagnosticosConsulta.Remove(customerPhone);
                    }
                    var tempCollection = new List<Form_Expediente>(this.DiagnosticosConsulta.ToList());
                    this.DiagnosticosConsulta = null;
                    this.DiagnosticosConsulta = tempCollection;
                    this.RaisePropertyChanged("DiagnosticosConsulta");
                }
            }

            
        }

        private void OnDeletePhoneChild()
        {

            if (this.DiagnosticosConsultaChild != null &&
                 this.DiagnosticosConsultaChild.Any(x => x.IsSelected))
            {
                //char
                var qureyItems = this.DiagnosticosConsultaChild.Where(x => x.idFormulario > 0 && (x.IsSelected)).ToList();
                if (qureyItems.Any())
                {
                    _dataservice.DeleteForm_Expediente(qureyItems);
                    qureyItems.ForEach(x => x.IsDeleted = true);

                }

                var items = this.DiagnosticosConsultaChild.Where(x => x.IsSelected && (x.idFormulario == 0)).ToList();

                if (items.Any())
                {
                    foreach (var customerPhone in items)
                    {
                        this.DiagnosticosConsultaChild.Remove(customerPhone);
                    }
                   
                }

                var tempCollection = new List<Form_Expediente>(this.DiagnosticosConsultaChild.ToList());
                this.DiagnosticosConsultaChild = null;
                this.DiagnosticosConsultaChild = tempCollection;
                this.RaisePropertyChanged("DiagnosticosConsultaChild");
            }


        }



        #endregion



    }
      
   
}