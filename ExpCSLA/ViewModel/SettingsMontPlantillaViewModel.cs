﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ExpWCF;
using ExpCSF.Base;
using System;
using ExpCSF.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace ExpCSF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class SettingsMontPlantillaViewModel : BaseViewModel<List<Motivos>>
    {

        private DataService _dataservice = new DataService();

        private int _SelectedDiagnosticID;
        public int SelectedDiagnosticID
        {
            get { return _SelectedDiagnosticID; }
            set
            {
                _SelectedDiagnosticID = value;
                this.RaisePropertyChanged("SelectedDiagnosticID");
            }
        }


        private List<Motivos> _DiagnosticosConsulta;
        public List<Motivos> DiagnosticosConsulta
        {
            get { return _DiagnosticosConsulta; }
            set
            {
                _DiagnosticosConsulta = value;
                this.RaisePropertyChanged("DiagnosticosConsulta");
            }
        }

        private List<Plantillas> _DiagnosticosConsultaChild;
        public List<Plantillas> DiagnosticosConsultaChild
        {
            get { return _DiagnosticosConsultaChild; }
            set
            {
                _DiagnosticosConsultaChild = value;
                this.RaisePropertyChanged("DiagnosticosConsultaChild");
            }
        }


        private Plantillas _SelectedDiagnostic;
        public Plantillas SelectedDiagnostic
        {
            get { return _SelectedDiagnostic; }
            set
            {
                _SelectedDiagnostic = value;
                this.RaisePropertyChanged("SelectedDiagnostic");
            }
        }

        private int _SelectedFather;
        public int SelectedFather
        {
            get { return _SelectedFather; }
            set
            {
                _SelectedFather = value;
                this.RaisePropertyChanged("SelectedFather");
            }
        }





        #region Properties

        #region Window Properties
        public override string Title
        {
            get
            {
                return !IsInEditMode ? "Formularios" : "Editar Formularios";
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 85;
            }
        }

        public override double DialogStartupCustomHeight
        {
            get
            {
                return 680;
            }
        }

        public override double DialogStartupCustomWidth
        {
            get
            {
                return 750;
            }
        }

        public override DialogType DialogType
        {
            get
            {
                return DialogType.BySizeInPixel;
            }
        }
        #endregion

        #endregion

        #region Commands
        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return this._saveCommand ?? (this._saveCommand = new RelayCommand(OnSaveCustomer, CanSaveCustomer)); }
        }

        private ICommand _saveCommandChild;
        public ICommand SaveCommandChild
        {
            get { return this._saveCommandChild ?? (this._saveCommandChild = new RelayCommand(OnSaveCustomerChild, CanSaveCustomer)); }
        }


        
        private ICommand _addPhoneCommand;
        private ICommand _deletePhoneCommand;
        private ICommand _addPhoneCommandChild;
        private ICommand _deletePhoneCommandChild;
        private ICommand _ChildLookCommand;



        //public ICommand CancelCommand
        //{
        //    get { return this._cancelCommand ?? (this._cancelCommand = new RelayCommand(OnCancelCustomer)); }
        //}

        public ICommand AddPhoneCommand
        {
            get { return this._addPhoneCommand ?? (this._addPhoneCommand = new RelayCommand(OnAddPhone)); }
        }

        public ICommand DeletePhoneCommand
        {
            get { return this._deletePhoneCommand ?? (this._deletePhoneCommand = new RelayCommand(OnDeletePhone, CanDeletePhone)); }
        }

        public ICommand AddPhoneCommandChild
        {
            get { return this._addPhoneCommandChild ?? (this._addPhoneCommandChild = new RelayCommand(OnAddPhoneChild)); }
        }

        public ICommand DeletePhoneCommandChild
        {
            get { return this._deletePhoneCommandChild ?? (this._deletePhoneCommandChild = new RelayCommand(OnDeletePhoneChild, CanDeletePhoneChild)); }
        }

        public ICommand ChildLookCommand
        {
            get { return this._ChildLookCommand ?? (this._ChildLookCommand = new RelayCommand<object>((valor) => OnChildLook(valor))); }
        }




        #endregion

        #region Constructors

        private void ButtonClick(object button) { }

        public SettingsMontPlantillaViewModel(IMessenger messenger)
            : base(messenger)
        {
            Especialidades pEspecialidad;
            if (UserLogin.medicos.Especialidades_Medico.FirstOrDefault() != null)
            {
                pEspecialidad = UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
            }
            else
            {
                pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            }
            //_dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            this.DiagnosticosConsulta = _dataservice.GetActiveMotives().Where(u => u.ConReporte == true).ToList();
            if (DiagnosticosConsulta.Count == 0)
                this.DiagnosticosConsulta = new List<Motivos>();
        }

        public SettingsMontPlantillaViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            Especialidades pEspecialidad;
            if (UserLogin.medicos != null)
            {
                if (UserLogin.medicos.Especialidades_Medico.FirstOrDefault() != null)
                {
                    pEspecialidad = UserLogin.medicos.Especialidades_Medico.FirstOrDefault().Especialidades;
                }
                else
                {
                    pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
                }
            }
            else
            {
                pEspecialidad = _dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            }
            //_dataservice.GetSpecialities().Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            this.DiagnosticosConsulta = _dataservice.GetActiveMotives().Where(u => u.ConReporte == true).ToList();
            if (DiagnosticosConsulta.Count == 0)
                this.DiagnosticosConsulta = new List<Motivos>();

        }

        public SettingsMontPlantillaViewModel(IMessenger messenger, Usuario userLogin, List<Motivos> customer)
            : base(messenger, userLogin)
        {
            IsInEditMode = true;
            this.Entity = customer;
            this.DiagnosticosConsulta = customer;


        }
        #endregion

        #region Override Methods

        public override void Initialize()
        {
            base.Initialize();

            //CargarDiagnosticos();

        }

        #endregion

        #region Command Methods
        private bool CanSaveCustomer()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }
        private void OnSaveCustomer()
        {
            var returnStatus = false;

            var rejectStatus = _dataservice.GetPlantillasMotive(SelectedFather);
            var results = this.DiagnosticosConsultaChild.Where(i => !rejectStatus.Any(e => i.IdPlantilla == e.IdPlantilla && e.IdMotivo == SelectedFather)).ToList().Where(x => x.IsSelected == true).ToList();
            returnStatus = _dataservice.AddPlantillaMotive(results, SelectedFather);

            var resultsDelete = this.DiagnosticosConsultaChild.Where(i => rejectStatus.Any(e => i.IdPlantilla == e.IdPlantilla && e.IdMotivo == SelectedFather)).ToList().Where(x => x.IsSelected == false).ToList();
            returnStatus = _dataservice.DeletePlantillaMotive(resultsDelete, SelectedFather);

            if (returnStatus)
            {
                var messageDailog = new MessageDailog()
                 {
                     Caption = Resources.MessageResources.DataSavedSuccessfully,
                     DialogButton = DialogButton.Ok,
                     Title = Resources.TitleResources.Information
                 };

                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

            }
            else
            {
                var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            }
        }

        private bool CanSaveCustomerChild()
        {
            return this.Entity != null; //&& this.Entity.HasValueInAllRequiredField;
        }
        private void OnSaveCustomerChild()
        {
            
            //char
            //returnStatus = !IsInEditMode ? _dataservice.AddFatherForm(this.DiagnosticosConsulta) : _dataservice.UpdateProblemConsultation(this.DiagnosticosConsulta);
            //returnStatus = _dataservice.AddFatherForm(this.DiagnosticosConsultaChild);

            //if (returnStatus)
            //{
            //    returnStatus = _dataservice.UpdateFormExpediente(this.DiagnosticosConsultaChild.Where(x => x.isNewItem == false).ToList());

            //    if (returnStatus)
            //    {
            //        var messageDailog = new MessageDailog()
            //        {
            //            Caption = Resources.MessageResources.DataSavedSuccessfully,
            //            DialogButton = DialogButton.Ok,
            //            Title = Resources.TitleResources.Information
            //        };

            //        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);

            //        if (this.CloseWindow != null)
            //            this.CloseWindow();
            //    }
            //}
            //else
            //{
            //    var messageDailog = new MessageDailog() { Caption = Resources.MessageResources.DataSavedFailed, DialogButton = DialogButton.Ok, Title = Resources.TitleResources.Error };
            //    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);
            //}
        }


        private void OnCancelCustomer()
        {
            var messageDailog = new MessageDailog((result) =>
                {
                    if (result == DialogResult.Ok)
                    {
                        if (this.ParentViewModel != null)
                            this.ParentViewModel.ChildViewModel = null;
                        this.Unload();

                        if (this.CloseWindow != null)
                            this.CloseWindow();
                    }
                }) { Caption = Resources.MessageResources.CancelWindowMessage, DialogButton = DialogButton.OkCancel, Title = Resources.TitleResources.Warning };
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(messageDailog);


        }

        private void OnAddPhone()
        {
            IsInEditMode = false;

            if (this.DiagnosticosConsulta == null)
                this.DiagnosticosConsulta = new List<Motivos>();

            //var tempCollection = this.DiagnosticosConsulta;
            //string TipoForm = ("P" +  (tempCollection.Count + 1).ToString());
            //tempCollection.Add(new Form_Expediente() { isNewItem = true, Orden = tempCollection.Count + 1, Estado = true, ConObservacion = true, TipoFormulario = TipoForm });
            //this.DiagnosticosConsulta = null;
            //this.DiagnosticosConsulta = tempCollection;
            //this.RaisePropertyChanged("DiagnosticosConsulta");


        }

        private void OnChildLook(object _Padre)
        {
            //Especialidades pEspecialidad = _dataservice.GetActiveFormEspecialidadByFather(((Form_Expediente)_Padre).idFormulario).Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            SelectedFather = ((int)_Padre);
            this.DiagnosticosConsultaChild = _dataservice.GetPlantillas();
            List<PlantillaMotivos> tempo = _dataservice.GetPlantillasMotive((int)_Padre);
            foreach (PlantillaMotivos item in tempo)
            {
                this.DiagnosticosConsultaChild.Where(u => u.IdPlantilla == item.IdPlantilla).FirstOrDefault().IsSelected = true;
            }
            //this.DiagnosticosConsultaChild = _dataservice.GetPlantillas();
            if (DiagnosticosConsultaChild.Count == 0)
                this.DiagnosticosConsultaChild = new List<Plantillas>();


        }

        private void OnCheckCommand(object _Valores)
        {
            var values = (object[])_Valores;
            //Especialidades pEspecialidad = _dataservice.GetActiveFormEspecialidadByFather(((Form_Expediente)_Padre).idFormulario).Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault();
            SelectedFather = ((int)values[0]);
            this.DiagnosticosConsultaChild = _dataservice.GetPlantillas();
            List<PlantillaMotivos> tempo = _dataservice.GetPlantillasMotive((int)values[1]);
            foreach (PlantillaMotivos item in tempo)
            {
                this.DiagnosticosConsultaChild.Where(u => u.IdPlantilla == item.IdPlantilla).FirstOrDefault().IsSelected = true;
            }
            //this.DiagnosticosConsultaChild = _dataservice.GetPlantillas();
            if (DiagnosticosConsultaChild.Count == 0)
                this.DiagnosticosConsultaChild = new List<Plantillas>();


        }


        private void OnAddPhoneChild()
        {
            IsInEditMode = false;

            if (this.DiagnosticosConsultaChild == null)
                this.DiagnosticosConsultaChild = new List<Plantillas>();

            var tempCollection = this.DiagnosticosConsultaChild;
            string TipoForm = ("P" + (tempCollection.Count + 1).ToString());
            //tempCollection.Add(new Plantillas() { isNewItem = true, Orden = tempCollection.Count + 1, Estado = true, ConObservacion = true, TipoFormulario = TipoForm, idFormulario_Padre = SelectedFather, MetodoRegistro = 0 });
            this.DiagnosticosConsultaChild = null;
            this.DiagnosticosConsultaChild = tempCollection;
            this.RaisePropertyChanged("DiagnosticosConsultaChild");


        }

        private bool CanDeletePhone()
        {
            return this.Entity != null && this.DiagnosticosConsulta != null;
            //&&
            //this.DiagnosticosConsulta.Any(x => x.IsSelected);

        }

        private bool CanDeletePhoneChild()
        {
            return this.Entity != null && this.DiagnosticosConsultaChild != null;
            //&&
            //this.DiagnosticosConsultaChild.Any(x => x.IsSelected);

        }



        private void OnDeletePhone()
        {

            //if (this.DiagnosticosConsulta != null &&
            //     this.DiagnosticosConsulta.Any(x => x.IsSelected))
            //{
            //    //char
            //    var qureyItems = this.DiagnosticosConsulta.Where(x => x.idFormulario > 0 && (x.IsSelected)).ToList();
            //    if (qureyItems.Any())
            //    {
            //        _dataservice.DeleteForm_Expediente(qureyItems);
            //        qureyItems.ForEach(x => x.IsDeleted = true);
            //    }

            //    var items = this.DiagnosticosConsulta.Where(x => x.IsSelected && (x.idFormulario == 0)).ToList();

            //    if (items.Any())
            //    {
            //        foreach (var customerPhone in items)
            //        {
            //            this.DiagnosticosConsulta.Remove(customerPhone);
            //        }
            //        var tempCollection = new List<Form_Expediente>(this.DiagnosticosConsulta.ToList());
            //        this.DiagnosticosConsulta = null;
            //        this.DiagnosticosConsulta = tempCollection;
            //        this.RaisePropertyChanged("DiagnosticosConsulta");
            //    }
            //}


        }

        private void OnDeletePhoneChild()
        {

            //if (this.DiagnosticosConsultaChild != null &&
            //     this.DiagnosticosConsultaChild.Any(x => x.IsSelected))
            //{
            //    //char
            //    var qureyItems = this.DiagnosticosConsultaChild.Where(x => x.idFormulario > 0 && (x.IsSelected)).ToList();
            //    if (qureyItems.Any())
            //    {
            //        _dataservice.DeleteForm_Expediente(qureyItems);
            //        qureyItems.ForEach(x => x.IsDeleted = true);

            //    }

            //    var items = this.DiagnosticosConsultaChild.Where(x => x.IsSelected && (x.idFormulario == 0)).ToList();

            //    if (items.Any())
            //    {
            //        foreach (var customerPhone in items)
            //        {
            //            this.DiagnosticosConsultaChild.Remove(customerPhone);
            //        }

            //    }

            //    var tempCollection = new List<Form_Expediente>(this.DiagnosticosConsultaChild.ToList());
            //    this.DiagnosticosConsultaChild = null;
            //    this.DiagnosticosConsultaChild = tempCollection;
            //    this.RaisePropertyChanged("DiagnosticosConsultaChild");
            //}


        }



        #endregion



    }
}