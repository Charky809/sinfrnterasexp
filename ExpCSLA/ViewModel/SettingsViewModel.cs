﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExpCSF.Base;
using ExpWCF;
using GalaSoft.MvvmLight.Messaging;
using ExpCSF.Controls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using GalaSoft.MvvmLight.Command;
using System.Data;
using ExpCSF.Resources;

namespace ExpCSF.ViewModel
{
    class SettingsViewModel: BaseViewModel
    {
        #region Fields
        private Pacientes _selectedCustomer;
        private string _selectedMenuItem;
        private IBaseViewModel _detailSectionViewModel;
        private Consultas _selectedConsultation;
        //private RelayCommand<object> _selectedMenuItem;

        #endregion

        #region Properties
        public Pacientes SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                _selectedCustomer = value;
                this.RaisePropertyChanged("SelectedCustomer");
            }
        }

        public Consultas SelectedConsultation
        {
            get { return _selectedConsultation; }
            set
            {
                _selectedConsultation = value;
                this.RaisePropertyChanged("SelectedConsultation");
            }
        }

        

        public List<string> MenuList
        {
            get
            {
                List<string> itemsMenu = new List<string>();

              
                    itemsMenu = new List<string>()
                    {
                        
                        MenuSettingsResources.Especialidades,
                        MenuSettingsResources.Formularios,
                        MenuSettingsResources.FormulariosSpec,
                        MenuSettingsResources.Plantillas,
                        MenuSettingsResources.MotPlantillas
                      
                    };
               

                return itemsMenu;
            }
        }

        public string SelectedMenuItem
        {
            get
            {
                //if (_selectedMenuItem == null)
                //{
                //    if (this.SelectedCustomer == null)
                //    {
                //        SetAppointmentsList();
                //    }
                //    else
                //    {
                //        //SetSummaryView();
                //        SelectedMenuItem = MenuResources.Detail;
                //    }
                //}
                    
                return _selectedMenuItem;
            }
            set
            {
                _selectedMenuItem = value;
                this.RaisePropertyChanged("SelectedMenuItem");
                OnSelectedMenuItem();
            }
        }



        public SettingsFormViewModel _SettingsFormViewModel { get; set; }
        public AddPlantillaViewModel _AddPlantillaViewModel { get; set; }
        public SettingsMontPlantillaViewModel _SettingsMontPlantillaViewModel { get; set; }
        public SettingsSpecFormsViewModel _SettingsSpecFormsViewModel { get; set; }

       

        public IBaseViewModel DetailSectionViewModel
        {
            get { return _detailSectionViewModel; }
            set
            {
                _detailSectionViewModel = value;
                this.RaisePropertyChanged("DetailSectionViewModel");
            }
        }

        #endregion

        #region Command Properties

        //public RelayCommand<object> SelectedMenuItem
        //{
        //    get { return _selectedMenuItem; }
        //    set
        //    {
        //        _selectedMenuItem = value;
        //        this.RaisePropertyChanged(() => SelectedMenuItem);
        //    }
        //}

        #endregion

        #region Constructors

        public SettingsViewModel(IMessenger messenger)
            : base(messenger)
        {
        }

        public SettingsViewModel(IMessenger messenger, Usuario userLogin, Pacientes selectedCustomer)
            : base(messenger, userLogin)
        {
            try
            {
                SelectedCustomer = selectedCustomer;
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

        public SettingsViewModel(IMessenger messenger, Usuario userLogin, Consultas selectedConsultation)
            : base(messenger, userLogin)
        {
            try
            {
                SelectedConsultation = selectedConsultation;
                SelectedCustomer = selectedConsultation.Pacientes;
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

        public SettingsViewModel(IMessenger messenger, Usuario userLogin)
            : base(messenger, userLogin)
        {
            
        }

        #endregion

        #region Override Methods
        public override void Initialize()
        {
            base.Initialize();
            //SelectedMenuItem = new RelayCommand<object>(OnSelectedMenuItem);
        }

        public override void Unload()
        {
            base.Unload();
            SelectedMenuItem = null;
        }
        #endregion

        #region Command Methods
       
        #endregion

        #region Private Methods
        private void OnSelectedMenuItem()
        {
            if (SelectedMenuItem == MenuSettingsResources.Formularios)
            {
                if (_SettingsFormViewModel == null)
                    _SettingsFormViewModel = new SettingsFormViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };

                DetailSectionViewModel = _SettingsFormViewModel;
                return;
            }
            else if (SelectedMenuItem == MenuSettingsResources.Plantillas)
            {
                //if (_AddPlantillaViewModel == null)
                    _AddPlantillaViewModel = new AddPlantillaViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };

                DetailSectionViewModel = _AddPlantillaViewModel;
                return;
            }
            else if (SelectedMenuItem == MenuSettingsResources.MotPlantillas)
            {
                //if (_AddPlantillaViewModel == null)
                _SettingsMontPlantillaViewModel = new SettingsMontPlantillaViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };

                DetailSectionViewModel = _SettingsMontPlantillaViewModel;
                return;
            }
            else if (SelectedMenuItem == MenuSettingsResources.FormulariosSpec)
            {
                //if (_AddPlantillaViewModel == null)
                _SettingsSpecFormsViewModel = new SettingsSpecFormsViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };

                DetailSectionViewModel = _SettingsSpecFormsViewModel;
                return;
            }

            
        }

        //private void SetSummaryView()
        //{
        //    DetailSectionViewModel = null;
        //    if (CustomerSummaryViewModel == null)
        //        CustomerSummaryViewModel = new PatientSummaryViewModel(this.Messenger, this.UserLogin, this.SelectedCustomer) { ParentViewModel = this };

        //    DetailSectionViewModel = CustomerSummaryViewModel;
        //    //this.SelectedMenuItem = MenuResources.Detail;
        //}

        //private void SetAppointmentsList()
        //{
        //    DetailSectionViewModel = null;

        //    if (CustomerAppointmentViewModel == null)
        //        CustomerAppointmentViewModel = new ListaViewModel(this.Messenger, this.UserLogin) { ParentViewModel = this };

        //    DetailSectionViewModel = CustomerAppointmentViewModel;
            
        //}
        #endregion

    }
}
