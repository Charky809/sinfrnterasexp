﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using ExpCSF.Base;
using ExpCSF.Commands;
using ExpCSF.Controls;
using ExpWCF;

namespace ExpCSF.ViewModel
{

    public class UserLoginViewModel : BaseViewModel<Usuario>
    {
        private RelayCommand _signInCommand;

        #region Fields

        #endregion

        #region Properties
        public override string Title
        {
            get
            {
                return "ACCESO";
            }
        }

        public override double DialogStartupSizePercentage
        {
            get
            {
                return 35;
            }
        }

        public override double WindowHeight
        {
            get
            {
                return 600;
            }
        }

        public override double WindowWidth
        {
            get
            {
                return 600;
            }
        }

        private string _LoginName;
        public string LoginName
        {
            get { return _LoginName; }
            set
            {
                if (_LoginName != value)
                {
                    _LoginName = value;
                    this.RaisePropertyChanged("LoginName");
                }
            }
        }

        private string _LoginPassword;
        public string LoginPassword
        {
            get { return _LoginPassword; }
            set
            {
                if (_LoginPassword != value)
                {
                    _LoginPassword = value;
                    this.RaisePropertyChanged("LoginPassword");

                }
            }
        }
        #endregion

        #region Commands

        public RelayCommand SignInCommand
        {
            get { return _signInCommand; }
            set
            {
                _signInCommand = value;
                this.RaisePropertyChanged("SignInCommand");
            }
        }

        #endregion

        #region Constructors
            public UserLoginViewModel(IMessenger messenger)
                : base(messenger)
            {

            }
            #endregion

            #region Public Methods

            #endregion

            #region Privae Methods

            #endregion

            #region Command Methods
            //public void ExecuteSingInCommand()
            //{
            //    this.OnDataProcess();
            //}
            public bool CanExecuteSingInCommand()
            {
                return (!string.IsNullOrEmpty(this.LoginName) && !string.IsNullOrEmpty(this.LoginPassword));
            }
            #endregion

            #region Virtual Methods
            public override void Initialize()
            {
                base.Initialize();
                this.SignInCommand = new RelayCommand(OnDataProcess, CanExecuteSingInCommand);
            }

            protected override void OnAction(ActionResult<Usuario> result)
            {
                try
                {

                    //var userLoginActions = new UserLoginAction();
                    DataService _dataservice = new DataService();
                    
                    result.Data = _dataservice.LoginCheck(_LoginName, _LoginPassword);
                    
                    
                    if (result.Data != null)
                    {
                        this.ParentViewModel.UpdateUserLogin(result.Data);
                        if (((Usuario)result.Data).UsuarioRol.FirstOrDefault().Roles.Nombre.ToLower().Equals("enfermero"))
                        {
                           this.ParentViewModel.UpdateCitasVisible(false);
                        }else
                            this.ParentViewModel.UpdateCitasVisible(true);
                        
                        result.Result = ActionResultType.DataFetched;
                        this.CloseChild();
                        //Task.Factory.StartNew(() =>
                        //{
                        //    ((MainViewModel)(this.ParentViewModel)).IsAppointmentTabSelected = true;
                        //});
                        
                    }
                    else
                    {
                        StatusMessage = "Autenticación Falló";
                        LoginPassword = string.Empty;
                        result.Result = ActionResultType.DataNotFound;
                    }
                }
                catch (Exception )
                {
                    //NLogLogger.LogError(exception, TitleResources.Error, ExceptionResources.ExceptionOccured,
                    //                    ExceptionResources.ExceptionOccuredLogDetail);
                }
            }
            #endregion
        }

    }
