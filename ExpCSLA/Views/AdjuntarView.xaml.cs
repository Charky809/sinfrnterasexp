﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

namespace ExpCSF.Views
{
    /// <summary>
    /// Interaction logic for UserLoginView.xaml
    /// </summary>
    public partial class AdjuntarView : UserControl
    {
        public AdjuntarView()
        {
            InitializeComponent();
        }

        private void UploadFileToSQL_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".pdf"; // Default file extension
            dlg.Filter = "Text documents (.pdf)|*.pdf"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                UploadFile(filename);
            }
        }

        private void UploadFile(string fullName)
        {
            string Cnx = string.Empty;
            Cnx = ConfigurationManager.ConnectionStrings["ExpCSF.Properties.Settings.ExpCsfConnectionString"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(Cnx))
            {
                cn.Open();
                FileStream fStream = File.OpenRead(fullName);
                byte[] contents = new byte[fStream.Length];
                fStream.Read(contents, 0, (int)fStream.Length);
                fStream.Close();
                using (SqlCommand cmd = new SqlCommand("insert into SavePDFTable " + "(PDFFile)values(@data)", cn))
                {
                    //cmd.Parameters.Add("@data", contents);
                    cmd.Parameters.AddWithValue("@data", contents);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Finished");
                }
            }
        }

        private void LoadFileFromSQL_Click(object sender, RoutedEventArgs e)
        {

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = "MyPDF";
            dlg.DefaultExt = ".pdf"; // Default file extension
            dlg.Filter = "Text documents (.pdf)|*.pdf"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {

               
                // Open document 
                string filename = dlg.FileName;
                LoadFile(filename);
                MessageBox.Show("Finished");
            }
        }

        private void LoadFile(string fullName)
        {
            string Cnx = string.Empty;
            Cnx = ConfigurationManager.ConnectionStrings["ExpCSF.Properties.Settings.ExpCsfConnectionString"].ConnectionString;

            using (SqlConnection cn
                = new SqlConnection(Cnx))
            {
                cn.Open();
                using (SqlCommand cmd
                    = new SqlCommand("select PDFFile from SavePDFTable  where ID='" + "1" + "' ", cn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.Default))
                    {
                        if (dr.Read())
                        {

                            byte[] fileData = (byte[])dr.GetValue(0);
                            using (System.IO.FileStream fs = new System.IO.FileStream(fullName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite))
                            {
                                using (System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs))
                                {
                                    bw.Write(fileData);
                                    bw.Close();
                                }
                            }
                        }

                        dr.Close();
                    }
                }
            }
        }
    }
}
