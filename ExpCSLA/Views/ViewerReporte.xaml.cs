﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using ExpWCF;
using System.IO;
using System.Drawing.Printing;
using System.Drawing.Imaging;

namespace ExpCSF.Views
{
    /// <summary>
    /// Interaction logic for ViewerReporte.xaml
    /// </summary>
    public partial class ViewerReporte : Window
    {
        private DataService _dataservice = new DataService();
                private int m_currentPageIndex = 0;

        public IList<Stream> m_streams { get; set; }

        public ViewerReporte(int  idParametro,bool ImprimirDirecto)
        {
            InitializeComponent();
            
            if(ImprimirDirecto)
            {
            LocalReport _report = new LocalReport();
            //var _titulo = new ReportParameter("Titulo", Titulo, false);
            //var _date2 = new ReportParameter("Fecha2", val_fec2, false);
            //List<ReportParameter> ListReportParameter = new List<ReportParameter>();

            //CSF_BL.SF_Context admin = new CSF_BL.SF_Context();
            //ListReportParameter.Add(_titulo);

            _report.ReportPath = "Reportes\\rptResultadoEstudio.rdlc";
            //_report.SetParameters(ListReportParameter);
            _report.DataSources.Add(new ReportDataSource("Dset_Datos", _dataservice.spTraerPlantilla(idParametro)));
             Export(_report);
            Print();
            Close();
            }else
            {
                DisplayReport(idParametro);
            }
            
        }

        private void DisplayReport(int idParametro)
        {
            try
            {
               
                //var val_fec1 = _Fecha1;
                //var val_fec2 = _Fecha2;
                //var _titulo = new ReportParameter("Titulo", Titulo, false);
                //var _date2 = new ReportParameter("Fecha2", val_fec2, false);
                //List<ReportParameter> ListReportParameter = new List<ReportParameter>();

                //CSF_BL.SF_Context admin = new CSF_BL.SF_Context();
                //ListReportParameter.Add(_titulo);
                //ListReportParameter.Add(_date2);

                this.viewerInstance.LocalReport.ReportPath = "Reportes\\rptResultadoEstudio.rdlc";
                viewerInstance.LocalReport.DataSources.Clear();
                viewerInstance.LocalReport.Refresh();
                //viewerInstance.LocalReport.SetParameters(ListReportParameter);
                viewerInstance.LocalReport.DataSources.Add(new ReportDataSource("Dset_Datos", _dataservice.spTraerPlantilla(idParametro)));
              
                viewerInstance.RefreshReport();

            }
            catch (Exception ex)
            {
               throw new Exception("Ocurrió un error controlado, " + ex.Message.ToString());
            }
        }

        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            ev.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            Metafile pageImage = new Metafile(m_streams[m_currentPageIndex]);

            ev.Graphics.DrawImage(pageImage, ev.PageBounds);

            m_currentPageIndex += 1;

            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);

        }

        private void Print()
        {
            if (m_streams == null || m_streams.Count == 0)
            {
                throw new Exception("Error: no stream to print.");
            }
            PrintDocument printDoc = new PrintDocument();
            PaperSize ps = new PaperSize("Tiquete", 850, 1100);

            printDoc.DefaultPageSettings.PaperSize = ps;

            var _with1 = printDoc.DefaultPageSettings.Margins;

            _with1.Top = 1;

            _with1.Bottom =1;

            _with1.Left =1;

            _with1.Right = 1;


            printDoc.DocumentName = "ReporteGeneral";

            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.PrintPage += PrintPage;
                m_currentPageIndex = 0;
                printDoc.Print();
            }
        }

        

        private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }

        private void Export(LocalReport report)
        {
            string deviceInfo = "<DeviceInfo>" + "<OutputFormat>EMF</OutputFormat>" + "<PageWidth>8.51in</PageWidth>" + "<PageHeight>11in</PageHeight>" + "<MarginTop>0.16416in</MarginTop>" + "<MarginLeft>0.20472in</MarginLeft>" + "<MarginRight>0.20472in</MarginRight>" + "<MarginBottom>0.16416in</MarginBottom>" + "</DeviceInfo>";
            Warning[] warnings = null;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream, out warnings);
            foreach (Stream stream in m_streams)
            {
                stream.Position = 0;
            }
        }
    }
}
