﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpCSF.Widget.Core
{
    public class AppCell
    {
        public int Column;
        public int Row;

        public AppCell(int column, int row)
        {
            Column = column;
            Row = row;
        }
    }
}
