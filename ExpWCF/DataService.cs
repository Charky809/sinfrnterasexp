﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Globalization;
using System.Data.Objects;
using System.Data;

namespace ExpWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class DataService : IDataService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public List<Consultas> GetConsultas()
        {

            CSLADBEntities _entidades = new CSLADBEntities();

            return _entidades.Consultas.Take(20).ToList();

        }

        public List<Consultas> GetConsultasEnfermeria(DateTime _fecha)
        {

            CSLADBEntities _entidades = new CSLADBEntities();

            DateTime _fechaBuscar = _fecha.AddDays(-1);

            return _entidades.Consultas.Where(x => EntityFunctions.TruncateTime(x.Fecha) >= EntityFunctions.TruncateTime(_fechaBuscar)
                    && x.Bitacora_Consultas.Where(z => z.IdTipoRegistroConsulta >= 1).FirstOrDefault() != null).OrderBy(y => y.Fecha).ToList();

        }

        public List<Consultas> GetConsultasMedicos(DateTime _fecha, int _idMedico)
        {

            CSLADBEntities _entidades = new CSLADBEntities();

            DateTime _fechaBuscar = _fecha.AddDays(1);

            return _entidades.Consultas.Where(x => EntityFunctions.TruncateTime(x.Fecha) >= EntityFunctions.TruncateTime(_fecha)
                && EntityFunctions.TruncateTime(x.Fecha) < EntityFunctions.TruncateTime(_fechaBuscar)
                && x.Medico == _idMedico
                && x.Bitacora_Consultas.Where(z => z.IdTipoRegistroConsulta >= 1).FirstOrDefault() != null).OrderBy(y => y.Fecha).ToList();

        }

        public List<Consultas> GetConsultasReportes(DateTime _fecha)
        {

            CSLADBEntities _entidades = new CSLADBEntities();

            DateTime _fechaBuscar = _fecha.AddDays(1);

            return _entidades.Consultas.Where(x => EntityFunctions.TruncateTime(x.Fecha) >= EntityFunctions.TruncateTime(_fecha)
                && EntityFunctions.TruncateTime(x.Fecha) < EntityFunctions.TruncateTime(_fechaBuscar)
                && x.Motivos.ConReporte == true
                && x.Bitacora_Consultas.Where(z => z.IdTipoRegistroConsulta >= 1).FirstOrDefault() != null).OrderBy(y => y.Fecha).ToList();

        }

        public List<Consultas> GetConsultasPatient(Pacientes _patient)
        {
            if (_patient != null)
            {
                CSLADBEntities _entidades = new CSLADBEntities();

                return _entidades.Consultas.Where(x => x.Paciente == _patient.IdPaciente && (x.Form_Exp_Consulta.Count() >= 1
                        || x.SIGNOS_VITALES.Count() >= 1
                        || x.Problemas_Consulta.Count() >= 1
                        || x.Diagnosticos_Cita.Count() >= 1
                        || x.Datos_Consulta.Count() >= 1)).OrderByDescending(y => y.Fecha).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<Consultas> GetConsultasRecepcion(DateTime _fecha)
        {

            CSLADBEntities _entidades = new CSLADBEntities();

            DateTime _fechaBuscar = _fecha.AddDays(-1);

            return _entidades.Consultas.Where(x => EntityFunctions.TruncateTime(x.Fecha) >= EntityFunctions.TruncateTime(_fechaBuscar)).OrderBy(z => z.Fecha).ToList();

        }

        public Dictionary<string, string> GetPatients(string _SearchText)
        {

            CSLADBEntities _entidades = new CSLADBEntities();
            int _idNumber;
            bool result = Int32.TryParse(_SearchText, out _idNumber);
            if (result)
            {
                return _entidades.Pacientes.Where(u => u.Cedula.ToLower().Trim()
                       .Equals(_SearchText.ToLower().Trim()))
                           .ToList()
                           .OrderBy(z => z.NombreCompleto.Trim())
                               .Select(x => new { CustomerID = x.IdPaciente, Name = x.NombreCompleto.Trim() })
                              .ToDictionary(y => y.CustomerID.ToString(CultureInfo.InvariantCulture), y => y.Name); ;
            }
            else
            {
                return _entidades.Pacientes.Where(u => u.NombreCompleto.ToLower()
                            .Contains(_SearchText.ToLower()))
                                .ToList()
                                 .OrderBy(z => z.NombreCompleto.Trim())
                                    .Select(x => new { CustomerID = x.IdPaciente, Name = x.NombreCompleto.Trim() })
                                   .ToDictionary(y => y.CustomerID.ToString(CultureInfo.InvariantCulture), y => y.Name); ;
            }


        }

        public Usuario LoginCheck(string NombreUsuario, string Password)
        {

            try
            {
                CSLADBEntities segdatamodel = new CSLADBEntities();


                var usuario = segdatamodel.Usuario.Where(u => u.NombreUsuario.ToLower() == NombreUsuario.ToLower() && u.Password.ToLower() == Password.ToLower()).FirstOrDefault();

                return usuario;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool PatientInsert(Pacientes paciente)
        {
            bool vReturn = false;
            try
            {
                CSLADBEntities _entidades = new CSLADBEntities();

                _entidades.Pacientes.AddObject(paciente);
                _entidades.SaveChanges();

                vReturn = true;

            }
            catch (Exception)
            {

                throw;
            }

            return vReturn;
        }

        public bool PatientUpdate(Pacientes paciente)
        {
            bool vReturn = false;
            try
            {
                CSLADBEntities _entidades = new CSLADBEntities();
                Pacientes original = _entidades.Pacientes.Where(x => x.IdPaciente == paciente.IdPaciente).Single();

                original.Telefono1 = paciente.Telefono1;
                original.GeographicDistributionId = paciente.GeographicDistributionId;
                original.FechaNacimiento = paciente.FechaNacimiento;
                original.Sexo = paciente.Sexo;
                original.Nombre = paciente.Nombre;
                original.Apellidos = paciente.Apellidos;
                original.NombreCompleto = paciente.NombreCompleto;

                _entidades.SaveChanges();

                vReturn = true;

            }
            catch (Exception)
            {

                throw;
            }

            return vReturn;
        }

        public bool AddAppointment(Consultas appointment)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                Consultas nuevaConsult = new Consultas
                {
                    Paciente = appointment.Paciente,
                    ConsultaTipo = appointment.ConsultaTipo,
                    Tipo = appointment.Tipo,
                    Hora = appointment.Hora,
                    Fecha = appointment.Fecha,
                    idCategoria = appointment.idCategoria,
                    Comentarios = appointment.Comentarios,
                    Duracion = appointment.Duracion,
                    idEspecialidad = appointment.idEspecialidad,
                    Medico = appointment.Medico,
                    Motivo = appointment.Motivo,
                    UsuarioId = appointment.UsuarioId,
                    Confirmado = appointment.Confirmado,
                    Estado = appointment.Estado,
                    IdUsuario = appointment.IdUsuario,

                };


                context.Consultas.AddObject(nuevaConsult);

                int resultado = 0;
                resultado = context.SaveChanges();
                if (resultado != 0)
                {
                    Bitacora_Consultas _bitacora = new Bitacora_Consultas() { FechaRegistro = DateTime.Now, IdConsulta = nuevaConsult.ID, UsuarioRegistro = nuevaConsult.IdUsuario.ToString(), IdTipoRegistroConsulta = context.TipoRegistroConsulta.Where(x => x.Descripcion == "Ingreso").FirstOrDefault().idTipoRegistroConsulta };
                    context.Bitacora_Consultas.AddObject(_bitacora);
                    resultado = context.SaveChanges();

                }
                return resultado > 0;

            }
            catch (Exception )
            {
                return false;
            }
        }

        public bool AddAppointmentData(Datos_Consulta _appointmentData)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                context.Datos_Consulta.AddObject(_appointmentData);
                var result = context.SaveChanges();
                return result > 0;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddVitalSigns(SIGNOS_VITALES _vitalSigns)
        {

            try
            {

                CSLADBEntities context = new CSLADBEntities();
                Consultas appVacuna = new Consultas();


                SIGNOS_VITALES _vacum = new SIGNOS_VITALES()
                {
                    TEMPERATURA = _vitalSigns.TEMPERATURA,
                    CirCef = _vitalSigns.CirCef,
                    ESTIMADO = _vitalSigns.ESTIMADO,
                    FECHA = _vitalSigns.FECHA,
                    FRECUENCIA = _vitalSigns.FRECUENCIA,
                    FRECUENCIA_RESPIRATORIA = _vitalSigns.FRECUENCIA_RESPIRATORIA,
                    IMV = _vitalSigns.IMV,
                    NotaEnfermeria = _vitalSigns.NotaEnfermeria,
                    PAI = _vitalSigns.PAI,
                    PESO = _vitalSigns.PESO,
                    PIC = _vitalSigns.PIC,
                    PRESION_ARTERIAL = _vitalSigns.PRESION_ARTERIAL,
                    PULSO = _vitalSigns.PULSO,
                    PVC = _vitalSigns.PVC,
                    RESULTADO_ASC = _vitalSigns.RESULTADO_ASC,
                    RESULTADO_IMC = _vitalSigns.RESULTADO_IMC,
                    RESULTADO_PAM = _vitalSigns.RESULTADO_PAM,
                    RESULTADO_SATO2 = _vitalSigns.RESULTADO_SATO2,
                    TALLA = _vitalSigns.TALLA,
                    IdConsulta = _vitalSigns.IdConsulta,
                    DIAS_ENFERMEDAD = _vitalSigns.DIAS_ENFERMEDAD
                };

                context.SIGNOS_VITALES.AddObject(_vacum);
                appVacuna = _vacum.Consultas;

                int resultado = 0;
                resultado = context.SaveChanges();
                if (resultado != 0)
                {

                    if (context.Bitacora_Consultas.Where(x => x.IdConsulta == appVacuna.ID && x.IdTipoRegistroConsulta == context.TipoRegistroConsulta.Where(z => z.Descripcion == "Preconsulta").FirstOrDefault().idTipoRegistroConsulta).FirstOrDefault() == null)
                    {
                        Bitacora_Consultas _bitacora = new Bitacora_Consultas() { FechaRegistro = DateTime.Now, IdConsulta = appVacuna.ID, UsuarioRegistro = appVacuna.IdUsuario.ToString(), IdTipoRegistroConsulta = context.TipoRegistroConsulta.Where(x => x.Descripcion == "Preconsulta").FirstOrDefault().idTipoRegistroConsulta };
                        context.Bitacora_Consultas.AddObject(_bitacora);
                        resultado = context.SaveChanges();
                    }
                }
                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }

            //    CSLADBEntities context = new CSLADBEntities();

            //    context.SIGNOS_VITALES.AddObject(_vitalSigns);
            //    var result = context.SaveChanges();
            //    return result > 0;

            //}
            //catch (Exception)
            //{
            //    return false;
            //}
        }

        public bool AddDxConsultation(IEnumerable<Diagnosticos_Cita> _dxsConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                foreach (var customer in _dxsConsultation)
                {

                    Diagnosticos_Cita diagInsertar = new Diagnosticos_Cita()
                    {
                        FechaRegistro = customer.FechaRegistro,
                        idCita = customer.idCita,
                        Estado = customer.Estado,
                        idDiagnostico = customer.Diagnosticos.Id_Diagnostico,
                        IdUsuario = customer.IdUsuario,
                        Observaciones = customer.Observaciones,
                        Orden = customer.Orden,
                        IsDeleted = customer.IsDeleted,
                        IsSelected = customer.IsSelected
                    };

                    context.Diagnosticos_Cita.AddObject(diagInsertar);

                }
                var result = context.SaveChanges();
                return result > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public Diagnosticos AddDx(Diagnosticos _dxs)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();



                Diagnosticos diagInsertar = new Diagnosticos()
                    {
                        Cie10 = _dxs.Cie10,
                        Descripcion = _dxs.Descripcion,
                        Estado = _dxs.Estado,
                    };

                    context.Diagnosticos.AddObject(diagInsertar);
                               
                var result = context.SaveChanges();

                if (result > 0)
                    return diagInsertar;
                else
                    return null;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool AddMedicNoteConsultation(Datos_Consulta _dtConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                Datos_Consulta appDatos = new Datos_Consulta();
                int IdEspec = 0;
                Consultas Ahora = context.Consultas.Where(u => u.ID == _dtConsultation.idConsulta).SingleOrDefault();
                if (context.Consultas.Where(u => u.ID == _dtConsultation.idConsulta).SingleOrDefault().medicos.Especialidades_Medico.Count != 0)
                    IdEspec = context.Consultas.Where(u => u.ID == _dtConsultation.idConsulta).SingleOrDefault().medicos.Especialidades_Medico.FirstOrDefault().idEspecialidad;
                else
                    IdEspec = context.Consultas.Where(u => u.ID == _dtConsultation.idConsulta).SingleOrDefault().Especialidades.ID;


                Datos_Consulta _vacum = new Datos_Consulta()
                    {
                        Evolucion = _dtConsultation.Evolucion,
                        idConsulta = _dtConsultation.idConsulta,
                        idUsuario = _dtConsultation.idUsuario,
                        Indicaciones = _dtConsultation.Indicaciones,
                        Motivo = _dtConsultation.Motivo,
                        Subjetivo = _dtConsultation.Subjetivo,
                        FechaRegistro = DateTime.Now,
                        idEspecialidad = IdEspec,
                        Estado = true,
                    };

                context.Datos_Consulta.AddObject(_vacum);
                //appVacuna = _vacum.Consultas;

                int resultado = 0;
                resultado = context.SaveChanges();
                if (resultado != 0)
                {
                    if (context.Bitacora_Consultas.Where(x => x.IdConsulta == _vacum.idConsulta && x.IdTipoRegistroConsulta == context.TipoRegistroConsulta.Where(z => z.Descripcion == "Atendido").FirstOrDefault().idTipoRegistroConsulta).FirstOrDefault() == null)
                    {
                        Bitacora_Consultas _bitacora = new Bitacora_Consultas() { FechaRegistro = DateTime.Now, IdConsulta = _vacum.idConsulta, UsuarioRegistro = _vacum.idUsuario.ToString(), IdTipoRegistroConsulta = context.TipoRegistroConsulta.Where(x => x.Descripcion == "Atendido").FirstOrDefault().idTipoRegistroConsulta };
                        context.Bitacora_Consultas.AddObject(_bitacora);
                        resultado = context.SaveChanges();
                    }
                }
                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool AddFatherForm(List<Form_Expediente> _formSpec)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                bool Entro = false;


                foreach (var item in _formSpec.Where(x => x.isNewItem))
                {
                    Entro = true;
                    if (item.isFatherItem)
                    {
                        Form_Expediente _vacum = new Form_Expediente()
                              {

                                  ConObservacion = false,
                                  Descripcion = item.Descripcion,
                                  MetodoRegistro = item.MetodoRegistro,
                                  TipoFormulario = item.TipoFormulario,
                                  Orden = item.Orden,
                                  Estado = item.Estado,
                              };
                        context.Form_Expediente.AddObject(_vacum);

                    }
                    else
                    {
                        Form_Expediente _vacum = new Form_Expediente()
                        {

                            ConObservacion = false,
                            Descripcion = item.Descripcion,
                            MetodoRegistro = item.MetodoRegistro,
                            TipoFormulario = item.TipoFormulario,
                            idFormulario_Padre = item.idFormulario_Padre,
                            Orden = item.Orden,
                            Estado = item.Estado,

                        };
                        context.Form_Expediente.AddObject(_vacum);

                    }

                }
                int resultado = 0;
                resultado = context.SaveChanges();
                if (Entro == false)
                    resultado = 1;

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool AddPlantilla(Plantillas _dtPlantilla)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                Plantillas _vacum = new Plantillas()
                {
                    Descripcion = _dtPlantilla.Descripcion,
                    Encabezado = _dtPlantilla.Encabezado,
                    Resultado = _dtPlantilla.Resultado,
                    PiePagina = _dtPlantilla.PiePagina,
                    ImpresionDiag = _dtPlantilla.ImpresionDiag,
                };

                context.Plantillas.AddObject(_vacum);
                //appVacuna = _vacum.Consultas;

                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool AddImage(SavePDFTable _dtPlantilla)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();


                SavePDFTable _vacum = new SavePDFTable()
                {
                    FechaRegistro = _dtPlantilla.FechaRegistro,
                     PDFFile = _dtPlantilla.PDFFile,
                     UsuarioId = _dtPlantilla.UsuarioId,
                    idConsulta = _dtPlantilla.idConsulta,
                    Descripcion = _dtPlantilla.Descripcion,
                    
                };

                context.SavePDFTable.AddObject(_vacum);
                //appVacuna = _vacum.Consultas;

                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool AddPlantillaMotive(List<Plantillas> _plantillas, int Motive)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                bool Entro = false;

                foreach (var item in _plantillas)
                {
                    Entro = true;

                    PlantillaMotivos _vacum = new PlantillaMotivos()
                    {

                        IdMotivo = Motive,
                        IdPlantilla = item.IdPlantilla,
                        CampoEspecial = false,
                        Estado = true,

                    };
                    context.PlantillaMotivos.AddObject(_vacum);

                }
                int resultado = 0;
                resultado = context.SaveChanges();
                if (Entro == false)
                    resultado = 1;

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool AddPlantillaMotivoConsulta(PlantillaMotivoConsulta _plantilla)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();


                PlantillaMotivoConsulta _vacum = new PlantillaMotivoConsulta()
                    {

                        IdMotivo = _plantilla.IdMotivo,
                        IdConsulta = _plantilla.IdConsulta,
                        IdPlantillaOriginal = _plantilla.IdPlantillaOriginal,
                        Resultado = _plantilla.Resultado,
                        Encabezado = _plantilla.Encabezado,
                        PiePagina = _plantilla.PiePagina,
                        FechaRegistro = _plantilla.FechaRegistro,
                        ImpresionDiag = _plantilla.ImpresionDiag,
                        IdMedico = _plantilla.IdMedico,

                    };
                context.PlantillaMotivoConsulta.AddObject(_vacum);


                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool AddFormSpeciality(List<Form_Expediente> _plantillas, int Speciality)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                bool Entro = false;

                foreach (var item in _plantillas)
                {
                    Entro = true;

                    Form_Especialidad _vacum = new Form_Especialidad()
                    {

                        idEspecialidad = Speciality,
                        idForm_Expediente = item.idFormulario,
                        Estado = true,

                    };
                    context.Form_Especialidad.AddObject(_vacum);

                }
                int resultado = 0;
                resultado = context.SaveChanges();
                if (Entro == false)
                    resultado = 1;

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool UpdateMedicNoteConsultation(Datos_Consulta _dtConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                Datos_Consulta appDatos = new Datos_Consulta();

                //int IdEspec = context.Especialidades.Where(x => x.VARIABLE.Equals("M6")).FirstOrDefault().ID;
                int IdEspec;

                if (context.Usuario.Where(u => u.Id == _dtConsultation.idUsuario).Single().medicos != null)
                {
                    if (context.Usuario.Where(u => u.Id == _dtConsultation.idUsuario).Single().medicos.Especialidades_Medico != null)
                        IdEspec = context.Usuario.Where(u => u.Id == _dtConsultation.idUsuario).Single().medicos.Especialidades_Medico.FirstOrDefault().idEspecialidad;
                    else
                        IdEspec = context.Especialidades.Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault().ID;
                }
                else
                    IdEspec = context.Especialidades.Where(z => z.VARIABLE.Equals("M6")).FirstOrDefault().ID;

                appDatos = context.Datos_Consulta.Where(x => x.idDatosConsulta == _dtConsultation.idDatosConsulta).FirstOrDefault();

                appDatos.Evolucion = _dtConsultation.Evolucion;
                appDatos.idConsulta = _dtConsultation.idConsulta;
                appDatos.idUsuario = _dtConsultation.idUsuario;
                appDatos.Indicaciones = _dtConsultation.Indicaciones;
                appDatos.FechaRegistro = _dtConsultation.FechaRegistro;
                appDatos.idEspecialidad = _dtConsultation.idEspecialidad;
                appDatos.Estado = _dtConsultation.Estado;
                appDatos.Motivo = _dtConsultation.Motivo;
                appDatos.Subjetivo = _dtConsultation.Subjetivo;
                

                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool UpdatePlantilla(Plantillas _dtPlantilla)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                Plantillas appDatos = new Plantillas();


                appDatos = context.Plantillas.Where(x => x.IdPlantilla == _dtPlantilla.IdPlantilla).FirstOrDefault();


                appDatos.Encabezado = _dtPlantilla.Encabezado;
                appDatos.Resultado = _dtPlantilla.Resultado;
                appDatos.PiePagina = _dtPlantilla.PiePagina;
                appDatos.ImpresionDiag = _dtPlantilla.ImpresionDiag;
                appDatos.Descripcion = _dtPlantilla.Descripcion;


                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }


        public bool UpdateImage()
        {
            try
            {
            //    CSLADBEntities context = new CSLADBEntities();
            //    Plantillas appDatos = new Plantillas();


            //    appDatos = context.Plantillas.Where(x => x.IdPlantilla == _dtPlantilla.IdPlantilla).FirstOrDefault();


            //    appDatos.Encabezado = _dtPlantilla.Encabezado;
            //    appDatos.Resultado = _dtPlantilla.Resultado;
            //    appDatos.PiePagina = _dtPlantilla.PiePagina;
            //    appDatos.ImpresionDiag = _dtPlantilla.ImpresionDiag;
            //    appDatos.Descripcion = _dtPlantilla.Descripcion;


            //    int resultado = 0;
            //    resultado = context.SaveChanges();

            //    return resultado > 0;
                return true;
            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool UpdatePlantillaMotivoConsulta(PlantillaMotivoConsulta _dtPlantilla)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                PlantillaMotivoConsulta appDatos = new PlantillaMotivoConsulta();

                appDatos = context.PlantillaMotivoConsulta.Where(x => x.IdPlantillaMotivoConsulta == _dtPlantilla.IdPlantillaMotivoConsulta).SingleOrDefault();

                appDatos.Encabezado = _dtPlantilla.Encabezado;
                appDatos.Resultado = _dtPlantilla.Resultado;
                appDatos.PiePagina = _dtPlantilla.PiePagina;
                appDatos.ImpresionDiag = _dtPlantilla.ImpresionDiag;

                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool UpdatePreConsultation(SIGNOS_VITALES _vitalSigns)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                SIGNOS_VITALES appDatos = new SIGNOS_VITALES();

                //int IdEspec = context.Especialidades.Where(x => x.VARIABLE.Equals("M6")).FirstOrDefault().ID;
                //int IdEspec = context.Usuario.Where(u => u.Id == _dtConsultation.idUsuario).FirstOrDefault().medicos.Especialidades_Medico.FirstOrDefault().idEspecialidad;

                appDatos = context.SIGNOS_VITALES.Where(x => x.COD_SIGNOS_VITALES == _vitalSigns.COD_SIGNOS_VITALES).FirstOrDefault();

                appDatos.TEMPERATURA = _vitalSigns.TEMPERATURA;
                appDatos.CirCef = _vitalSigns.CirCef;
                appDatos.ESTIMADO = _vitalSigns.ESTIMADO;
                appDatos.FECHA = _vitalSigns.FECHA;
                appDatos.FRECUENCIA = _vitalSigns.FRECUENCIA;
                appDatos.FRECUENCIA_RESPIRATORIA = _vitalSigns.FRECUENCIA_RESPIRATORIA;
                appDatos.IMV = _vitalSigns.IMV;
                appDatos.NotaEnfermeria = _vitalSigns.NotaEnfermeria;
                appDatos.PAI = _vitalSigns.PAI;
                appDatos.PESO = _vitalSigns.PESO;
                appDatos.PIC = _vitalSigns.PIC;
                appDatos.PRESION_ARTERIAL = _vitalSigns.PRESION_ARTERIAL;
                appDatos.PULSO = _vitalSigns.PULSO;
                appDatos.PVC = _vitalSigns.PVC;
                appDatos.RESULTADO_ASC = _vitalSigns.RESULTADO_ASC;
                appDatos.RESULTADO_IMC = _vitalSigns.RESULTADO_IMC;
                appDatos.RESULTADO_PAM = _vitalSigns.RESULTADO_PAM;
                appDatos.RESULTADO_SATO2 = _vitalSigns.RESULTADO_SATO2;
                appDatos.TALLA = _vitalSigns.TALLA;
                appDatos.IdConsulta = _vitalSigns.IdConsulta;
                appDatos.DIAS_ENFERMEDAD = _vitalSigns.DIAS_ENFERMEDAD;

                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }



        public bool AddVacunaConsultation(IEnumerable<Vacunas_Consulta> _dxsConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                Consultas appVacuna = new Consultas();
                foreach (var customer in _dxsConsultation)
                {

                    Vacunas_Consulta _vacum = new Vacunas_Consulta()
                    {
                        Fecha_Aplicacion = customer.Fecha_Aplicacion,
                        idConsulta = customer.idConsulta,
                        idUsuario = customer.idUsuario,
                        idVacuna = customer.Vacunas.idVacuna,
                        Tipo_Vacuna = customer.Tipo_Vacuna,
                        Secuencia = customer.Secuencia,
                        isDeleted = customer.isDeleted,
                        IsSelected = customer.IsSelected
                    };

                    context.Vacunas_Consulta.AddObject(_vacum);
                    appVacuna = _vacum.Consultas;
                }
                int resultado = 0;
                resultado = context.SaveChanges();
                if (resultado != 0)
                {

                    if (context.Bitacora_Consultas.Where(x => x.IdConsulta == appVacuna.ID && x.IdTipoRegistroConsulta == context.TipoRegistroConsulta.Where(z => z.Descripcion == "Preconsulta").FirstOrDefault().idTipoRegistroConsulta).FirstOrDefault() == null)
                    {
                        Bitacora_Consultas _bitacora = new Bitacora_Consultas() { FechaRegistro = DateTime.Now, IdConsulta = appVacuna.ID, UsuarioRegistro = appVacuna.IdUsuario.ToString(), IdTipoRegistroConsulta = context.TipoRegistroConsulta.Where(x => x.Descripcion == "Preconsulta").FirstOrDefault().idTipoRegistroConsulta };
                        context.Bitacora_Consultas.AddObject(_bitacora);
                        resultado = context.SaveChanges();
                    }
                }
                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool AddProblemConsultation(IEnumerable<Problemas_Consulta> _dxsConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                foreach (var customer in _dxsConsultation)
                {
                    context.Problemas_Consulta.AddObject(customer);
                }
                var result = context.SaveChanges();
                return result > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool UpdateFormExpediente(IEnumerable<Form_Expediente> dxsConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                bool Entro = false;
                foreach (var customer in dxsConsultation.Where(x => x.isNewItem == false))
                {
                    Entro = true;
                    Form_Expediente modificar = context.Form_Expediente.Where(x => x.idFormulario == customer.idFormulario).FirstOrDefault();
                    modificar.Descripcion = customer.Descripcion;
                    modificar.MetodoRegistro = customer.MetodoRegistro;
                    modificar.Orden = customer.Orden;
                    modificar.TipoFormulario = customer.TipoFormulario;

                }

                var result = context.SaveChanges();
                if (!Entro)
                    result = 1;

                return result > 0;
            }
            catch (Exception )
            {

                return false;
            }
        }

        public bool UpdateDxConsultation(IEnumerable<Diagnosticos_Cita> dxsConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                foreach (var customer in dxsConsultation)
                {
                    Diagnosticos_Cita modificar = context.Diagnosticos_Cita.Where(x => x.IdDiagnosticoCita == customer.IdDiagnosticoCita).Single();
                    modificar.Observaciones = customer.Observaciones;

                    //context.ObjectStateManager.ChangeObjectState(customer, System.Data.EntityState.Modified);
                }


                var result = context.SaveChanges();
                return result > 0;
            }
            catch (Exception )
            {

                return false;
            }
        }

        public bool UpdateVacunasConsultation(IEnumerable<Vacunas_Consulta> dxsConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                foreach (var customer in dxsConsultation)
                {
                    context.ObjectStateManager.ChangeObjectState(customer, System.Data.EntityState.Modified);
                }


                var result = context.SaveChanges();
                return result > 0;
            }
            catch (Exception )
            {

                return false;
            }
        }


        public bool UpdateProblemConsultation(IEnumerable<Problemas_Consulta> dxsConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                foreach (var customer in dxsConsultation)
                {
                    context.ObjectStateManager.ChangeObjectState(customer, System.Data.EntityState.Modified);
                }

                var result = context.SaveChanges();
                return result > 0;
            }
            catch (Exception )
            {

                return false;
            }
        }

        public bool DeleteAppointment(Consultas appointment)
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    context.Consultas.Attach(new Consultas() { ID = appointment.ID });
                    context.Consultas.DeleteObject(appointment);
                    var result = context.SaveChanges();
                    return result > 0;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool DeleteAppointments(IEnumerable<Consultas> appointments)
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    foreach (var customer in appointments.Select(customer => new Consultas() { ID = customer.ID }))
                    {
                        context.Consultas.Attach(customer);
                        context.Consultas.DeleteObject(customer);
                    }

                    var result = context.SaveChanges();
                    return result > 0;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool DeleteDXConsultation(IEnumerable<Diagnosticos_Cita> _dxConsultation)
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    foreach (var _dxDelete in _dxConsultation.Select(_dxDelete => new Diagnosticos_Cita() { IdDiagnosticoCita = _dxDelete.IdDiagnosticoCita }))
                    {
                        context.Diagnosticos_Cita.Attach(_dxDelete);
                        context.Diagnosticos_Cita.DeleteObject(_dxDelete);
                    }

                    var result = context.SaveChanges();
                    return result > 0;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool DeletePlantillaMotive(List<Plantillas> _plantillas, int Motive)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                bool Entro = false;

                foreach (var item in _plantillas)
                {
                    Entro = true;

                    var Eliminar = context.PlantillaMotivos.Where(u => u.IdMotivo == Motive && u.IdPlantilla == item.IdPlantilla).FirstOrDefault();

                    context.PlantillaMotivos.DeleteObject(Eliminar);

                }
                int resultado = 0;
                resultado = context.SaveChanges();
                if (Entro == false)
                    resultado = 1;

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool DeletePlantilla(Plantillas _plantilla)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();


                var Eliminar = context.Plantillas.Where(u => u.IdPlantilla == _plantilla.IdPlantilla).FirstOrDefault();

                context.Plantillas.DeleteObject(Eliminar);


                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool DeleteSavedimage(SavePDFTable _plantilla)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();


                var Eliminar = context.SavePDFTable.Where(u => u.ID == _plantilla.ID).FirstOrDefault();

                context.SavePDFTable.DeleteObject(Eliminar);


                int resultado = 0;
                resultado = context.SaveChanges();

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool DeleteFormSpeciality(List<Form_Expediente> _plantillas, int Speciality)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                bool Entro = false;

                foreach (var item in _plantillas)
                {
                    Entro = true;

                    var Eliminar = context.Form_Especialidad.Where(u => u.idEspecialidad == Speciality && u.idForm_Expediente == item.idFormulario).FirstOrDefault();

                    context.Form_Especialidad.DeleteObject(Eliminar);

                }
                int resultado = 0;
                resultado = context.SaveChanges();
                if (Entro == false)
                    resultado = 1;

                return resultado > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }


        public bool DeleteVacunaConsultation(IEnumerable<Vacunas_Consulta> _dxConsultation)
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    foreach (var _dxDelete in _dxConsultation.Select(_dxDelete => new Vacunas_Consulta() { IdVacunaConsulta = _dxDelete.IdVacunaConsulta }))
                    {
                        context.Vacunas_Consulta.Attach(_dxDelete);
                        context.Vacunas_Consulta.DeleteObject(_dxDelete);
                    }

                    var result = context.SaveChanges();
                    return result > 0;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool DeleteProblemConsultation(IEnumerable<Problemas_Consulta> _dxConsultation)
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    foreach (var _dxDelete in _dxConsultation.Select(_dxDelete => new Problemas_Consulta() { idProblemaConsulta = _dxDelete.idProblemaConsulta }))
                    {
                        context.Problemas_Consulta.Attach(_dxDelete);
                        context.Problemas_Consulta.DeleteObject(_dxDelete);
                    }

                    var result = context.SaveChanges();
                    return result > 0;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }


        public bool DeleteForm_Expediente(IEnumerable<Form_Expediente> _dxConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                foreach (var _dxDelete in _dxConsultation.Select(_dxDelete => new Form_Expediente() { idFormulario = _dxDelete.idFormulario }))
                {
                    context.Form_Expediente.Attach(_dxDelete);
                    context.Form_Expediente.DeleteObject(_dxDelete);


                }

                var result = context.SaveChanges();
                return result > 0;

            }
            catch (Exception)
            {

                return false;
            }
        }



        public List<Consultas> GetAppointments(DateTime appointmentDate)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Consultas.Where(x => EntityFunctions.TruncateTime(x.Fecha) == EntityFunctions.TruncateTime(appointmentDate)
                    ).OrderBy(z => z.Fecha);
                return new List<Consultas>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Especialidades> GetSpecialities()
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                List<Especialidades> items = context.Especialidades.Where(x => x.ESTADO == true).OrderBy(z => z.DESCRIPCION).ToList();

                return items;

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Consultas> GetNurseAppointments(DateTime appointmentDate, Especialidades pEspecialiad)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                List<Consultas> items = context.Consultas.Where(x => (EntityFunctions.TruncateTime(x.Fecha) == EntityFunctions.TruncateTime(appointmentDate) || EntityFunctions.TruncateTime(x.Fecha) == EntityFunctions.TruncateTime(appointmentDate.AddDays(-1)))
                    && x.Datos_Consulta.Count > 0).OrderBy(z => z.Fecha).ToList();
                //M6
                List<Consultas> ItemsEspecialidad = items.Where(z => z.Datos_Consulta.FirstOrDefault().Especialidades.VARIABLE == pEspecialiad.VARIABLE).ToList();
                var itemsFinales = ItemsEspecialidad.Where(c => c.Datos_Consulta.FirstOrDefault().Fecha_Egreso == null);
                return new List<Consultas>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Consultas> GetAppointmentsMedic(DateTime appointmentDate, int idMedico)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Consultas.Where(x => EntityFunctions.TruncateTime(x.Fecha) == EntityFunctions.TruncateTime(appointmentDate) && x.Medico == idMedico).OrderBy(z => z.Fecha);
                return new List<Consultas>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<medicos> GetActiveMedics()
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.medicos.Where(x => x.Disponible == true);
                return new List<medicos>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public medicos GetActiveMedicsById(int _IdMedico)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                medicos item = context.medicos.Where(x => x.Disponible == true && x.IDMedico == _IdMedico).FirstOrDefault();
                return item;

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Diagnosticos> GetActiveDX()
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    var items = context.Diagnosticos.Where(x => x.Estado == true);
                    return new List<Diagnosticos>(items.ToList());
                }
            }
            catch (Exception)
            {

                return null;
            }
        }

        public Dictionary<string, string> GetActiveDXFilter(string _SearchText, bool IsCode)
        {

            CSLADBEntities _entidades = new CSLADBEntities();


            if (IsCode)
            {
                return _entidades.Diagnosticos.Where(u => u.Cie10.ToLower().Trim()
                       .Contains(_SearchText.ToLower().Trim()))
                           .ToList()
                           .OrderBy(z => z.Descripcion.Trim())
                               .Select(x => new { CustomerID = x.Id_Diagnostico, Descr = x.Descripcion.Trim() })
                              .ToDictionary(y => y.CustomerID.ToString(CultureInfo.InvariantCulture), y => y.Descr); ;
            }
            else
            {
                return _entidades.Diagnosticos.Where(u => u.Descripcion.ToLower()
                            .Contains(_SearchText.ToLower()))
                                .ToList()
                                 .OrderBy(z => z.Descripcion.Trim())
                                    .Select(x => new { CustomerID = x.Id_Diagnostico, Descr = x.Descripcion.Trim() })
                                   .ToDictionary(y => y.CustomerID.ToString(CultureInfo.InvariantCulture), y => y.Descr); ;
            }


        }

        public Dictionary<string, string> GetActiveVacunasFilter(string _SearchText, bool IsCode)
        {

            CSLADBEntities _entidades = new CSLADBEntities();

            return _entidades.Vacunas.Where(u => u.Descripcion.ToLower()
                        .Contains(_SearchText.ToLower()))
                            .ToList()
                             .OrderBy(z => z.Descripcion.Trim())
                                .Select(x => new { CustomerID = x.idVacuna, Descr = x.Descripcion.Trim() })
                               .ToDictionary(y => y.CustomerID.ToString(CultureInfo.InvariantCulture), y => y.Descr); ;



        }

        public List<Diagnosticos_Cita> GetDXConsultation(Consultas _consulta)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Diagnosticos_Cita.Where(x => x.idCita == _consulta.ID);
                return new List<Diagnosticos_Cita>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Vacunas_Consulta> GetVacunasConsultation(Consultas _consulta)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Vacunas_Consulta.Where(x => x.idConsulta == _consulta.ID);
                return new List<Vacunas_Consulta>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Problemas_Consulta> GetProblemsConsultation(Consultas _consulta)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Problemas_Consulta.Where(x => x.idConsulta == _consulta.ID);

                foreach (var item in items)
                {
                    item.Tipo = item.Tipo.Trim();
                }
                return new List<Problemas_Consulta>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Recordatorios> GetReminders(string connectionString, DateTime? reminderDate)
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    var items = context.Recordatorios.Where(x => EntityFunctions.TruncateTime((DateTime)x.ReminderDate) == EntityFunctions.TruncateTime((DateTime)reminderDate.Value));
                    return new List<Recordatorios>(items.ToList());
                }
            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Recordatorios> GetReminders(string connectionString)
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    return new List<Recordatorios>(context.Recordatorios.ToList());
                }
            }
            catch (Exception)
            {

                return null;
            }
        }

        public bool UpdateAppointment(Consultas appointment)
        {
            try
            {

                CSLADBEntities context = new CSLADBEntities();
                Consultas pConsulta = context.Consultas.Where(u => u.ID == appointment.ID).Single();

                pConsulta.idCategoria = appointment.idCategoria;
                pConsulta.idEspecialidad = appointment.idEspecialidad;
                pConsulta.Estado = appointment.Estado;
                pConsulta.Duracion = appointment.Duracion;
                pConsulta.ConsultaTipo = appointment.ConsultaTipo;
                pConsulta.Comentarios = appointment.Comentarios;
                pConsulta.Medico = appointment.Medico;
                pConsulta.Motivo = appointment.Motivo;

                var result = context.SaveChanges();
                return result > 0;


            }
            catch (Exception )
            {

                return false;
            }
        }

        public List<GeographicDistribution> GetGeographicDistribution(decimal? parent_id)
        {
            CSLADBEntities context = new CSLADBEntities();

            if (parent_id == null)
            {
                return context.GeographicDistribution.Where(i => i.ParentId == null).OrderBy(i => i.Order).ToList<GeographicDistribution>();
            }
            else
            {
                return context.GeographicDistribution.Where(i => i.ParentId == parent_id).OrderBy(i => i.Order).ToList<GeographicDistribution>();
            }
        }

        public List<Motivos> GetActiveMotives()
        {
            try
            {
                using (CSLADBEntities context = new CSLADBEntities())
                {
                    var items = context.Motivos.Where(x => x.Disponible == true).OrderBy(z=> z.Descripcion);
                    return new List<Motivos>(items.ToList());
                }
            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Form_Exp_Consulta> GetFormEspecConsultation(Consultas _consulta, String Tipo)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Form_Exp_Consulta.Where(x => x.idConsulta == _consulta.ID && x.Form_Expediente.TipoFormulario == Tipo);
                return new List<Form_Exp_Consulta>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Form_Exp_Consulta> GetFormEspecPatient(Pacientes _patient, String Tipo)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                List<Consultas> patientConsultas = new List<Consultas>();
                patientConsultas = _patient.Consultas.Where(x => x.Form_Exp_Consulta.Count() != 0).OrderByDescending(c => c.Fecha).ToList();
                List<Form_Exp_Consulta> formPatient = new List<Form_Exp_Consulta>();
                foreach (var itemConsulta in patientConsultas)
                {
                    var items = context.Form_Exp_Consulta.Where(x => x.idConsulta == itemConsulta.ID && x.Form_Expediente.TipoFormulario == Tipo);

                    foreach (var itemForm in items)
                    {
                        formPatient.Add(itemForm);
                    }
                }

                return formPatient;

            }
            catch (Exception)
            {

                return null;
            }
        }


        public List<Diagnosticos_Cita> GetDiagPatient(Pacientes _patient)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                List<Consultas> patientConsultas = new List<Consultas>();
                patientConsultas = _patient.Consultas.Where(x => x.Diagnosticos_Cita.Count() != 0).OrderByDescending(c => c.Fecha).ToList();
                List<Diagnosticos_Cita> formPatient = new List<Diagnosticos_Cita>();
                foreach (var itemConsulta in patientConsultas)
                {
                    var items = context.Diagnosticos_Cita.Where(x => x.idCita == itemConsulta.ID && x.IsDeleted != true).OrderBy(z => z.Orden);

                    foreach (var itemForm in items)
                    {
                        formPatient.Add(itemForm);
                    }
                }

                return formPatient;

            }
            catch (Exception)
            {

                return null;
            }
        }



        public List<Datos_Consulta> GetMedicNotesPatient(Pacientes _patient)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                List<Consultas> patientConsultas = new List<Consultas>();
                patientConsultas = _patient.Consultas.Where(x => x.Datos_Consulta.Count() != 0).OrderByDescending(c => c.Fecha).ToList();
                List<Datos_Consulta> formPatient = new List<Datos_Consulta>();
                foreach (var itemConsulta in patientConsultas)
                {
                    var items = context.Datos_Consulta.Where(x => x.idConsulta == itemConsulta.ID);

                    foreach (var itemForm in items)
                    {
                        formPatient.Add(itemForm);
                    }
                }

                return formPatient;

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<SIGNOS_VITALES> GetPreConsultationsPatient(Pacientes _patient)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                List<Consultas> patientConsultas = new List<Consultas>();
                patientConsultas = _patient.Consultas.Where(x => x.SIGNOS_VITALES.Count() != 0).OrderByDescending(c => c.Fecha).ToList();
                List<SIGNOS_VITALES> formPatient = new List<SIGNOS_VITALES>();
                foreach (var itemConsulta in patientConsultas)
                {
                    var items = context.SIGNOS_VITALES.Where(x => x.IdConsulta == itemConsulta.ID);

                    foreach (var itemForm in items)
                    {
                        formPatient.Add(itemForm);
                    }
                }

                return formPatient;

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Problemas_Consulta> GetProblemsPatient(Pacientes _patient)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                List<Consultas> patientConsultas = new List<Consultas>();
                patientConsultas = _patient.Consultas.Where(x => x.Problemas_Consulta.Count() != 0).OrderByDescending(c => c.Fecha).ToList();
                List<Problemas_Consulta> formPatient = new List<Problemas_Consulta>();
                foreach (var itemConsulta in patientConsultas)
                {
                    var items = context.Problemas_Consulta.Where(x => x.idConsulta == itemConsulta.ID).OrderBy(z => z.Orden);

                    foreach (var itemForm in items)
                    {
                        formPatient.Add(itemForm);
                    }
                }

                return formPatient;

            }
            catch (Exception)
            {

                return null;
            }
        }

        public Datos_Consulta GetMedicNoteConsultation(Consultas _consulta)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Datos_Consulta.Where(x => x.idConsulta == _consulta.ID).FirstOrDefault();

                return items;

            }
            catch (Exception)
            {

                return null;
            }
        }


        public SIGNOS_VITALES GetPreConsultation(Consultas _consulta)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.SIGNOS_VITALES.Where(x => x.IdConsulta == _consulta.ID).FirstOrDefault();

                return items;

            }
            catch (Exception)
            {

                return null;
            }
        }

        public bool AddAntsConsultation(IEnumerable<Form_Exp_Consulta> _antsConsultation)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();
                bool entro = false;
                foreach (var customer in _antsConsultation)
                {
                   
                    //context.Detach(customer.Form_Expediente);
                    //context.Detach(customer.Consultas);
                    if ((customer.ValorTexto != null && customer.ValorTexto != string.Empty)
                            || (customer.ValSeleccionadoSi != null)
                               || (customer.ValSeleccionaodNo != null)
                               || (customer.ValSeleccionadoOtro != null)
                                || (customer.Observacion != null && customer.Observacion != string.Empty)
                                || (customer.Form_Expediente.idFormulario_Padre.ToString() == string.Empty ))
                                    
                    {
                        entro = true;

                        Form_Exp_Consulta nuevo = new Form_Exp_Consulta()
                        {
                            Fecha = customer.Fecha,
                            idConsulta = customer.idConsulta,
                            idFormulario = customer.idFormulario,
                            IdUsuario = customer.IdUsuario,
                            IsSelected = customer.IsSelected,
                            Observacion = customer.Observacion,
                            ValSeleccionadoSi = customer.ValSeleccionadoSi,
                            ValSeleccionaodNo = customer.ValSeleccionaodNo,
                            ValSeleccionadoOtro = customer.ValSeleccionadoOtro,
                            ValorTexto = customer.ValorTexto
                        };

                        context.Form_Exp_Consulta.AddObject(nuevo);
                    }
                }
                var result = context.SaveChanges();

                if (!entro)
                    result = 1;

                return result > 0;

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
                return false;
            }
        }

        public bool UpdateAntsConsultation(IEnumerable<Form_Exp_Consulta> _antsConsultation)
        {
            try
            {
                bool Entro = false;
                CSLADBEntities context = new CSLADBEntities();

                foreach (var customer in _antsConsultation.Where(x=>x.IdForm_Exp_Consulta != 0))
                {
                    Entro = true;
                    var Modificar = context.Form_Exp_Consulta.Where(x => x.IdForm_Exp_Consulta == customer.IdForm_Exp_Consulta).FirstOrDefault();
                    Modificar.ValorTexto = customer.ValorTexto;
                    Modificar.ValSeleccionadoSi = customer.ValSeleccionadoSi;
                    Modificar.ValSeleccionaodNo = customer.ValSeleccionaodNo;
                    Modificar.ValSeleccionadoOtro = customer.ValSeleccionadoOtro;
                    Modificar.Observacion = customer.Observacion;
                }

                var result = context.SaveChanges();

                if (!Entro)
                    result = 1;

                return result > 0;
            }
            catch (Exception )
            {

                return false;
            }
        }

        public List<Form_Especialidad> GetActiveFormEspecialidad(string _type, Especialidades _especialidad)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Form_Especialidad.Where(x => x.idEspecialidad == _especialidad.ID && x.Estado == true && x.Form_Expediente.TipoFormulario == _type).OrderBy(m => m.Form_Expediente.idFormulario_Padre).OrderBy(x => x.Form_Expediente.Orden);
                return new List<Form_Especialidad>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }


        public List<Form_Expediente> GetActiveFormEspecialidad(Especialidades _especialidad)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                //var items = context.Form_Expediente.Where(x => x.Form_Especialidad.Where(v => v.idEspecialidad == _especialidad.ID).Count() != 0).OrderBy(m => m.Descripcion).ToList();
                var items = context.Form_Expediente.Where(x => x.idFormulario_Padre == null).OrderBy(m => m.Descripcion).ToList();

                foreach (var item in items)
                {
                    item.TipoFormulario = item.TipoFormulario.Trim();
                }

                return new List<Form_Expediente>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Form_Expediente> GetActiveFormEspecialidadByFather(int _idPadre)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                var items = context.Form_Expediente.Where(x => x.idFormulario_Padre == _idPadre).OrderBy(x => x.Orden);
                return new List<Form_Expediente>(items.ToList());

            }
            catch (Exception)
            {

                return null;
            }
        }

        public List<Plantillas> GetPlantillas()
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                return context.Plantillas.OrderBy(u => u.Descripcion).ToList();

            }
            catch (Exception)
            {

                return null;
            }

        }

        public List<SavePDFTable> GetSAvedImages(int idConsulta)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                return context.SavePDFTable.Where(x => x.idConsulta == idConsulta).OrderBy(u => u.FechaRegistro).ToList();

            }
            catch (Exception)
            {

                return null;
            }

        }

        public List<PlantillaMotivos> GetPlantillasMotive(int _idMotivo)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                return context.PlantillaMotivos.Where(u => u.IdMotivo == _idMotivo).ToList();

            }
            catch (Exception)
            {

                return null;
            }

        }

        public List<PlantillaMotivoConsulta> GetPlantillasConsPatient(int _idPaciente)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                return context.PlantillaMotivoConsulta.Where(u => u.Consultas.Paciente == _idPaciente).OrderBy(x=>x.Consultas.Fecha).ToList();
               
                
            }
            catch (Exception)
            {

                return null;
            }

        }

        public PlantillaMotivoConsulta GetPlantillaMotivoConsulta(int _idConsulta)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                return context.PlantillaMotivoConsulta.Where(u => u.IdConsulta == _idConsulta).OrderBy(x => x.Consultas.Fecha).FirstOrDefault();


            }
            catch (Exception)
            {

                return null;
            }

        }

        public PlantillaMotivoConsulta GetPlantillaMotivoConsulta(int _idConsulta, int _idMotivo)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                return context.PlantillaMotivoConsulta.Where(u => u.IdConsulta == _idConsulta && u.IdMotivo == _idMotivo).OrderBy(x => x.Consultas.Fecha).FirstOrDefault();


            }
            catch (Exception)
            {

                return null;
            }

        }

        public List<Form_Especialidad> GetFormSpeciality(int _idEspecialidad)
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                return context.Form_Especialidad.Where(u => u.idEspecialidad == _idEspecialidad).ToList();

            }
            catch (Exception)
            {

                return null;
            }

        }

        public List<Form_Expediente> GetFatherForms()
        {
            try
            {
                CSLADBEntities context = new CSLADBEntities();

                return context.Form_Expediente.Where(u => u.idFormulario_Padre == null).OrderBy(u => u.Descripcion).ToList();

            }
            catch (Exception)
            {

                return null;
            }

        }



        public ObjectResult<EXP_TRAER_PLANTILLA_Result> spTraerPlantilla(int _idPlantilla)
        {
            try
            {

                CSLADBEntities context = new CSLADBEntities();

                return context.CALL_EXP_TRAER_PLANTILLA(_idPlantilla);

            }
            catch (Exception )
            {

                return null;
            }

        }
    }
}
