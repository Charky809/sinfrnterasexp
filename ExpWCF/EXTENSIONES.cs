﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;

namespace ExpWCF
{
    public partial class Recordatorios : EntityObject
    {


        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        public global::System.String FormattedReminder
        {
            get
            {
                return string.Format("{0} : {1} : {2}", this.ReminderName, this.ReminderType, ((DateTime)(this.ReminderDate)).Date.ToLongDateString()) + Environment.NewLine + this.ReminderMessage;
            }
            set
            {
                OnFormattedReminderChanging(value);
                ReportPropertyChanging("FormattedReminder");
                _FormattedReminder = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("FormattedReminder");
                OnFormattedReminderChanged();
            }
        }
        private global::System.String _FormattedReminder;
        partial void OnFormattedReminderChanging(global::System.String value);
        partial void OnFormattedReminderChanged();


    }


    public partial class Consultas : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _EsDigitador;
        public bool EsDigitador
        {
            get
            {
                return _EsDigitador;

            }
            set
            {
                _EsDigitador = value;
            }
        }

        private string _SelectedCat;
        public string SelectedCat
        {
            get
            {
                return
                    _SelectedCat;
            }
            set
            {
                //Clasificacion de la categoria segun Enum CategoryApt en Helpers
                if (value == "Roja")
                    idCategoria = 1;
                else if (value == "Amarilla")
                    idCategoria = 2;
                else if (value == "Verde")
                    idCategoria = 3;
                else
                    idCategoria = 0;

                _SelectedCat = value;


            }
        }
    }

    public partial class Diagnosticos : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }


        public string diagCodDesc
        {
            get { return Cie10 + "-" + Descripcion; }

        }



    }

    public partial class Pacientes : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private string _Edad;
        public string Edad
        {
            get
            {
                if (this.FechaNacimiento != null)
                {
                    TimeSpan age = DateTime.Now - FechaNacimiento.Value;
                    //string message = new String("Your age: {0} years and {1} days", (int)(age.Days / 365.25), age.Days % 365.25);
                    string message = Convert.ToString( (int)(age.Days / 365.25)) + " años y " + Convert.ToString(age.Days % 365.25) +  " dias";
    

                    //int Days = (DateTime.Now.Year * 365 + DateTime.Now.DayOfYear) - (FechaNacimiento.Value.Year * 365 + FechaNacimiento.Value.DayOfYear);
                    //int Years = Days / 365;
                    //string message = (Days >= 365) ? "Su Edad: " + Years + " años" : "Your Edad: " + Days + " days";


                    return message;
                }
                else
                {
                    return "No calculada.";
                }
                
            }
            set
            {

                _Edad = value;
            }
        }




    }


    public partial class Plantillas : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        


    }

    public partial class PlantillaMotivoConsulta : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }





    }

    public partial class Diagnosticos_Cita : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isNewItem;
        public bool isNewItem
        {
            get { return _isNewItem; }
            set { _isNewItem = value; }
        }

        private bool _isFirst;
        public bool isFirst
        {
            get { return _isFirst; }
            set { _isFirst = value; }
        }


    }

    public partial class Problemas_Consulta : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isNewItem;
        public bool isNewItem
        {
            get { return _isNewItem; }
            set { _isNewItem = value; }
        }

        private bool _isFirst;
        public bool isFirst
        {
            get { return _isFirst; }
            set { _isFirst = value; }
        }


    }

    public partial class Form_Exp_Consulta : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isNewItem;
        public bool isNewItem
        {
            get { return _isNewItem; }
            set { _isNewItem = value; }
        }

        private bool _isFirst;
        public bool isFirst
        {
            get { return _isFirst; }
            set { _isFirst = value; }
        }

        private string _TextoRadioSi;
        public string TextoRadioSi {
            get {
                if (this.Form_Expediente.Descripcion == "Especuloscopía")
                {
                    _TextoRadioSi = "Normal";
                }
                else
                {
                    _TextoRadioSi = "Sí";
                }
                return _TextoRadioSi;
            }
            set {

                _TextoRadioSi = value;
            }
        }

        private string _TextoRadioNo;
        public string TextoRadioNo {

            get
            {
                if (this.Form_Expediente.Descripcion == "Especuloscopía")
                {
                    _TextoRadioNo = "Alterada";
                }
                else
                {
                    _TextoRadioNo = "No";
                }
                return _TextoRadioNo;
            }
            set
            {

                _TextoRadioNo = value;
            }
        }

        private string _TextoRadioOtro;
        public string TextoRadioOtro {
            get
            {
                if (this.Form_Expediente.Descripcion == "Especuloscopía")
                {
                    _TextoRadioOtro = "No Examinada";
                }
                else
                {
                    _TextoRadioOtro = "Otro";
                }
                return TextoRadioOtro;
            }
            set
            {

                _TextoRadioOtro = value;
            }
        }

    }

    public partial class Vacunas_Consulta : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isFirst;
        public bool isFirst
        {
            get { return _isFirst; }
            set { _isFirst = value; }
        }


    }

    public partial class SIGNOS_VITALES : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isFirst;
        public bool isFirst
        {
            get { return _isFirst; }
            set { _isFirst = value; }
        }


    }

    public partial class Datos_Consulta : EntityObject
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isFirst;
        public bool isFirst
        {
            get { return _isFirst; }
            set { _isFirst = value; }
        }

        private bool _isDeleted;
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }


    }

    public partial class Form_Expediente : EntityObject
    {
        
        public bool isFatherItem
        {
            get
            {
                bool result;
                if (idFormulario_Padre != null && idFormulario_Padre != 0)
                {
                    result = false;
                }
                else
                    result = true;

                return result;
            }

        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isNewItem;
        public bool isNewItem
        {
            get { return _isNewItem; }
            set { _isNewItem = value; }
        }

        private bool _isDeleted;
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        private bool _VisibleOpcionTexto;
        public bool VisibleOpcionTexto
        {
            get
            {
                if ((MetodoRegistro == 0 || MetodoRegistro == 2) && !isFatherItem)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set { _VisibleOpcionTexto = value; }
        }

        private bool _VisibleOpcionRadio;
        public bool VisibleOpcionRadio
        {
            get
            {
                if ((MetodoRegistro == 1 || MetodoRegistro ==2) && !isFatherItem)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set { _VisibleOpcionRadio = value; }
        }

        private string _NombreGrupo;
        public string NombreGrupo
        {
            get
            {

                return Descripcion + this.idFormulario.ToString();
               
            }
            set { _NombreGrupo = value; }
        }

    }
}